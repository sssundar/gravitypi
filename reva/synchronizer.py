from time import time
from _optimizations import ffi as ffi
from _optimizations import lib as lib
import numpy as np

class Synchronizer():
	def __init__(self, logger, scheduler, state_estimator):
		self.name = "sync"		
		self.logger = logger
		self.scheduler = scheduler
		self.state_estimator = state_estimator

		self.sync = ffi.new("struct sync_s *")
		self.sync_cfg = ffi.new("struct sync_cfg_s *")
		self.sync_cfg.delay_s = 0.11
		self.sync_cfg.systick_s = 0.05
		self.sync_cfg.n_ticks_to_skip = 2;
		self.sync_cfg.sampling_period_s = 0.01;		
		lib.sync_init(	self.sync, self.sync_cfg, 
						self.scheduler.accelerometer.vector_queue,
						self.scheduler.gyro.vector_queue,
						self.scheduler.compass.vector_queue)
		return
	
	def deinit(self):
		return		

	def get_input_queue(self):
		return lib.sync_get_inq(self.sync)

	def get_output_queue(self):
		return lib.sync_get_outq(self.sync)

	# Handled as close to the exact time of the last 20Hz system tick as possible.
	def handle_task(self, flagged_at):		
		self.logger.trace(self, flagged_at)
		lib.sync_process_systick(self.sync, flagged_at)
		self.state_estimator.process_imu(self.get_output_queue())
		pass
