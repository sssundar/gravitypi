from board import COMPASS_INTERRUPT_PIN
from helpers import hexify
from time import time, sleep
from _optimizations import ffi as ffi
from _optimizations import lib as lib
import numpy as np

def configure_compass_vq(queue_cfg, nominal_sampling_period_s, fifo_wtm_lvl):
									# Rotation to common sensor frame
										# Swap Y,Z axes because sensor registers are ordered XZY.
										# No other rotation required because Adafruit 1604 IMU 
										# already aligns sensors to a common sensor frame.
	queue_cfg.quadx_sensor_idx = 0 	# X to X
	queue_cfg.quady_sensor_idx = 2 	# Y to Z
	queue_cfg.quadz_sensor_idx = 1 	# Z to Y
	queue_cfg.x_scale = 230.	 					# units per gauss
	queue_cfg.y_scale = 230.
	queue_cfg.z_scale = 205.
	queue_cfg.x_offset = 0.
	queue_cfg.y_offset = 0.
	queue_cfg.z_offset = 0.
	queue_cfg.nominal_sampling_period_s = nominal_sampling_period_s
	queue_cfg.fifo_wtm_lvl = fifo_wtm_lvl
	return

# LSM303DLHC Compass. 400kHz I2C max.
class Compass():
	def __init__(self, logger):
		self.name = "compass"
		self.logger = logger

		# i2c constants
		self.address = 0x1E

		# device configuration
		# compass must be configured after accelerometer, which reboots the entire chip		
		self.configuration = [
			# 	Reg  	Value 	Delay 	Verify 	Description
			[	0x02,	0x03, 	0,		False, 	"MR: sleep mode"],
			[	0x00,	0x14, 	0, 		True, 	"CRA: 30Hz compass, no temperature"],
			[	0x01,	0xE0, 	0, 		True, 	"CRB: +/-8.1 Gauss scale"],
			[	0x02,	0x00, 	0, 		True, 	"MR: continuous conversion"],
		]
		self.nominal_sampling_period_s = 0.033		

		# error flags and data address
		self.data_addr = 0x03
		self.sr = 0x09
		self.sr_drdy_flag = 0x01
		self.sample_bytes   = 6    # x(msb)x(lsb) z(msb)z(lsb) y(msb)y(lsb)

		# preallocated i2c buffers
		self.src = np.zeros(self.sample_bytes, dtype='uint8')
		self.src_ptr = ffi.cast("uint8_t *", self.src.ctypes.data)
		self.dst = self.src
		self.dst_ptr = self.src_ptr

		# sample batches, in radians per second, with second timestamps extrapolated between watermarks
		# for downstream synchronization with other sensors
		self.vector_queue = ffi.new("struct vector_queue_s *")
		self.queue_cfg = ffi.new("struct sensor_configuration_s *")
		configure_compass_vq(self.queue_cfg, self.nominal_sampling_period_s, 0)
		lib.vq_init(self.vector_queue, self.queue_cfg)	

		# configure the Pi to receive interrupts	
		import wiringpi # to allow access to configure_*_vq for offline tests on devices (like a laptop) without wiringpi	
		wiringpi.pinMode(COMPASS_INTERRUPT_PIN, wiringpi.GPIO.INPUT)
		wiringpi.pullUpDnControl(COMPASS_INTERRUPT_PIN, wiringpi.GPIO.PUD_DOWN)
		from callbacks import compass_callback # to avoid circular dependency on scheduler.py in import
		wiringpi.wiringPiISR(COMPASS_INTERRUPT_PIN, wiringpi.GPIO.INT_EDGE_RISING, compass_callback)
		
		# configure the compass
		self.configure()
		compass_callback() 	# Compass needs to be read to clear DRDY (e.g. if we crash with it on)
							# Weird chip. Sometimes gets into a bad state where this is the only way.
		return

	def configure(self):
		for addr, value, delay, _, _ in self.configuration:
			self.src[0] = value
			lib.i2cbus_explicit_write(self.address, addr, self.src_ptr, 1)						

			if delay > 0:
				sleep(delay)
		
		passed = True
		for addr, expected, _, check, _ in self.configuration:
			if check:				
				lib.i2cbus_explicit_read(self.address, addr, self.dst_ptr, 1)
				byte = self.dst[0]
				passed = passed and (byte == expected)		

		if not passed:
			self.logger.error(self, "misconfigured")		

	def deinit(self):
		import wiringpi # to allow access to configure_*_vq for offline tests on devices (like a laptop) without wiringpi
		from callbacks import null_callback # to avoid circular dependency on scheduler.py in import
		wiringpi.wiringPiISR(COMPASS_INTERRUPT_PIN, wiringpi.GPIO.INT_EDGE_RISING, null_callback)
		return		

	# sample the compass
	# we will only call this function on a drdy interrupt
	def handle_task(self, flagged_at):
		# grab the status of the fifo
		lib.i2cbus_explicit_read(self.address, self.sr, self.dst_ptr, 1)
		status = self.dst[0]			
		ready = (status & self.sr_drdy_flag) != 0

		if not ready:
			self.logger.info(self, "was not ready")
			return			

		# One-by-one		
		lib.i2cbus_explicit_read(self.address, self.data_addr, self.dst_ptr, self.sample_bytes);
		lib.vq_unpack_be_words(self.vector_queue, flagged_at, self.dst_ptr, 1)
		self.logger.trace(self, (flagged_at, 1, self.dst))
		return