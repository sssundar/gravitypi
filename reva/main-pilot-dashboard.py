# The dashboard for this quad. Runs on the laptop,
# as its own process, communicating with the pilot-comms
# via file IO for quad, gamepad state. Updated at ~10Hz.
# This script is required to be a separate event loop
# as blitting doesn't work well (flickers) when we 
# try to make this script non-blocking.

# References
# [1] https://learn.sparkfun.com/tutorials/graph-sensor-data-with-python-and-matplotlib/update-a-graph-in-real-time
# [2] https://learn.sparkfun.com/tutorials/graph-sensor-data-with-python-and-matplotlib/speeding-up-the-plot-animation
# [3] http://www.galaxysofts.com/new/python-creating-a-real-time-3d-plot/
# [4] http://nicolasfauchereau.github.io/climatecode/posts/drawing-a-gauge-with-matplotlib/
# [5] https://matplotlib.org/users/text_intro.html
# [6] https://stackoverflow.com/questions/28269157/plotting-in-a-non-blocking-way-with-matplotlib
# [7] https://matplotlib.org/3.1.1/gallery/mplot3d/mixed_subplots.html
# [8] https://stackoverflow.com/a/48924820/1623303

import sys
import datetime as dt
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.gridspec as gridspec
import numpy as np
from quaternions import vector_norm, quaternion_rotation
from helpers import rad_to_deg, read
from board import *

# Ensure our inbox exists.
inbox = open(INBOX_DASHBOARD, "wb")
inbox.close()
inbox = open(INBOX_DASHBOARD, "rb")
pilot_heartbeat = -1
DELAY_CNTDWN = 30
countdown = DELAY_CNTDWN

dt_s = 0.1 # ~10 fps
		
fig = plt.figure(figsize=(20,10))
gs = gridspec.GridSpec(5, 5)
flags = fig.add_subplot(gs[0, 0])
omega = fig.add_subplot(gs[0, 1:4])
chassis = fig.add_subplot(gs[1:, 0:4], projection='3d')
fuel = fig.add_subplot(gs[:, 4])

# omega time series
n_w = 200
t_s = list((np.array(range(n_w)) - n_w) * dt_s)
w_eff = [0.] * n_w		
omega.set_ylim([0., rad_to_deg(2.)])
omega_line = omega.plot(t_s, w_eff)[0]		
omega.set_title('||w|| [dps]')		
omega.set_xticklabels([])

# fuel time series
n_v = 5
v_bat = [0.] * n_v
v_mean = np.mean(v_bat)	
y_bat = [0., v_mean]
def get_fuel_gauge_color(v_battery):
	if v_battery < LOWEST_RECOMMENDED_BATTERY_VOLTAGE:
		result = "r"
	elif v_battery < (LOWEST_RECOMMENDED_BATTERY_VOLTAGE + 0.1):
		result = "y"
	else:
		result = "g"
	return result
gauge_color = get_fuel_gauge_color(v_mean)
fuel.set_ylim([6.4, 8.4])
fuel.set_xlim([-1,1])
fuel_lines = []	
spacing = 0.03
linewidth = 3
fuel_lines.append(fuel.plot([-spacing, -spacing], y_bat, gauge_color, linewidth=linewidth)[0])
fuel_lines.append(fuel.plot([0., 0.], y_bat, gauge_color, linewidth=linewidth)[0])
fuel_lines.append(fuel.plot([spacing, spacing], y_bat, gauge_color, linewidth=linewidth)[0])
fuel.set_title('Fuel Gauge [V]')
fuel.set_xticklabels([])

# indicator flags
have_gamepad = False
sent_comms_req = True
got_comms_ack = False
is_comms_dead = False
sent_shutdown_req = False
got_shutdown_ack = False
shutdown_req_timed_out = False
def get_gamepad_color():
	global have_gamepad
	if have_gamepad:
		return "g"
	else:
		return "r"

def get_comms_color():
	global sent_comms_req, got_comms_ack, is_comms_dead
	if is_comms_dead:
		return "r"
	elif got_comms_ack:
		return "g"
	elif sent_comms_req:
		return "y"
	else:
		return "w"

def get_shutdown_color():
	global sent_shutdown_req, got_shutdown_ack, shutdown_req_timed_out
	if shutdown_req_timed_out:
		return "r"
	elif got_shutdown_ack:
		return "g"
	elif sent_shutdown_req:
		return "y"
	else:
		return "w"

gamepad_color = get_gamepad_color()
comms_color = get_comms_color()
shutdown_color = get_shutdown_color()
flags.set_ylim([0, 1])
flags.set_xlim([0, 1])
flags_lines = []
flags_lines.append(flags.plot([0.25, 0.25], [0.49, 0.5], gamepad_color, linewidth=15)[0])
flags_lines.append(flags.plot([0.5, 0.5], [0.49, 0.5], comms_color, linewidth=15)[0])
flags_lines.append(flags.plot([0.75, 0.75], [0.49, 0.5], shutdown_color, linewidth=15)[0])
flags.text(0.1, 0.2, 'Gamepad', fontsize=8)
flags.text(0.4, 0.2, 'Comms', fontsize=8)
flags.text(0.65, 0.2, 'Shutdown', fontsize=8)
flags.set_title('Indicators')
flags.set_xticklabels([])
flags.set_yticklabels([])

# chassis
q_ref = [1., np.array([0., 0., 0.])]
q_est = [1., np.array([0., 0., 0.])]
motor_duty_cycles = [0.] * 4

# define the base points on the chassis
# then vectors can be defined as rotated bases plus the differences between them.
pwm1 = [0., np.asarray([1., -1., 0.])]
pwm2 = [0., np.asarray([-1., -1., 0.])]
pwm3 = [0., np.asarray([-1., 1., 0.])]
pwm4 = [0., np.asarray([1., 1., 0.])]
body_z = [0., np.asarray([0., 0., 1.])]

def compute_frame_segments(q, m=None):
	global pwm1, pwm2, pwm3, pwm4, body_z
	v1 = quaternion_rotation(pwm1, q)[1]
	v2 = quaternion_rotation(pwm2, q)[1]
	v3 = quaternion_rotation(pwm3, q)[1]
	v4 = quaternion_rotation(pwm4, q)[1]
	motors = [v1, v2, v3, v4]
	v5 = quaternion_rotation(body_z, q)[1]
	
	# See board.py for quad orientation relative to motors. 	
	chassis_segments = [] 	# Start		End
	chassis_segments.append([list(v1), list(v2)]) # -x 
	chassis_segments.append([list(v2), list(v3)]) # +y 
	chassis_segments.append([list(v3), list(v4)]) # +x 
	chassis_segments.append([list(v4), list(v1)]) # -y

	if m is not None:
		assert(len(m) == len(motors))
		thrust_segments = []
		for k in range(len(m)):
			thrust_segments.append([list(motors[k]), list(motors[k] + v5*m[k])])
		return (chassis_segments, thrust_segments)
	else:	
		return chassis_segments

def compute_segments():
	global q_ref, q_est, motor_duty_cycles

	ref = compute_frame_segments(q_ref)
	est, thrust = compute_frame_segments(q_est, motor_duty_cycles)

	# q_ref chassis. green frame with yellow nose
	# q_est chassis. black frame with red nose.
	# q_est thrust vectors. blue if in [0,1], red otherwise
	ref_colors = ['g', 'g', 'y', 'g']
	est_colors = ['k', 'k', 'r', 'k']
	thrust_colors = ['b' if ((motor_duty_cycles[k] >= 0.) and (motor_duty_cycles[k] <= 1.)) else 'r' for k in range(len(motor_duty_cycles))]
	return ref, ref_colors, est, est_colors, thrust, thrust_colors

# For some reason, axes3d.quiver takes (x_start, y_start, z_start, x_delta, y_delta, z_delta) as input
# but axes3d.quiver.set_segments takes (x_start, ..., x_end) as input.
# Since we're using .set_segments routinely but quiver only once... let's just reshape for initialization.
def reshape_segments(segments):
	x = [s[0][0] for s in segments]
	y = [s[0][1] for s in segments]
	z = [s[0][2] for s in segments]
	u = [s[1][0]-s[0][0] for s in segments]
	v = [s[1][1]-s[0][1] for s in segments]
	w = [s[1][2]-s[0][2] for s in segments]
	return (x,y,z,u,v,w)

chassis_ref, chassis_ref_colors, chassis_est, chassis_est_colors, thrust, thrust_colors = compute_segments()
ref_chassis_vectors = chassis.quiver(*reshape_segments(chassis_ref), colors=chassis_ref_colors, arrow_length_ratio=0., normalize=False)
est_chassis_vectors = chassis.quiver(*reshape_segments(chassis_est), colors=chassis_est_colors, arrow_length_ratio=0., normalize=False)
thrust_vectors = chassis.quiver(*reshape_segments(thrust), colors=thrust_colors, arrow_length_ratio=0.2, normalize=False)

chassis.set_xlim([-3,3])
chassis.set_ylim([-3,3])
chassis.set_zlim([-3,3])
chassis.set_xlabel("Earth x")
chassis.set_ylabel("Earth y")
chassis.set_zlabel("Earth z")
chassis.set_xticklabels([])
chassis.set_yticklabels([])
chassis.set_zticklabels([])

def die():
	global inbox	
	inbox.close()
	sys.exit(1)

def get_state():	
	global inbox, pilot_heartbeat, countdown, DELAY_CNTDWN
	global w_eff
	global v_bat, n_v, v_mean, gauge_color
	global q_ref, q_est, motor_duty_cycles
	global have_gamepad, sent_comms_req, got_comms_ack, is_comms_dead, sent_shutdown_req, got_shutdown_ack, shutdown_req_timed_out
	global gamepad_color, comms_color, shutdown_color
	global chassis_ref, chassis_ref_colors, chassis_est, chassis_est_colors, thrust, thrust_colors

	result = read(inbox, DASHBOARD_STATE, unpack_dashboard, None)

	# We will kill this process if pilot_heartbeat is the same five times in a row	
	if (result is None) or ((result is not None) and (result[1][0] == pilot_heartbeat)):
		countdown -= 1
		if countdown == 0:			
			die()
		return
	else:
		countdown = DELAY_CNTDWN

	pilot_heartbeat, we, vb, q_ref, q_est, motor_duty_cycles, have_gamepad, sent_comms_req, got_comms_ack, is_comms_dead, sent_shutdown_req, got_shutdown_ack, shutdown_req_timed_out = result[1]

	w_eff.append(rad_to_deg(vector_norm(we)))
	
	v_bat.append(vb)
	v_bat = v_bat[-n_v:]
	v_mean = np.mean(v_bat)
	gauge_color = get_fuel_gauge_color(v_mean)

	gamepad_color = get_gamepad_color()
	comms_color = get_comms_color()
	shutdown_color = get_shutdown_color()

	chassis_ref, _, chassis_est, _, thrust, thrust_colors = compute_segments()
		
def animate(i):			   
	global w_eff, n_w, omega_line
	global y_bat, v_mean, fuel_lines, gauge_color
	global flags_lines
	global ref_chassis_vectors, est_chassis_vectors, thrust_vectors 
	global chassis_ref, chassis_est, thrust, thrust_colors

	get_state()

	w_eff = w_eff[-n_w:]
	omega_line.set_ydata(w_eff)

	y_bat[1] = v_mean
	for l in fuel_lines:
		l.set_ydata(y_bat)
		l.set_color(gauge_color)
	
	flags_lines[0].set_color(gamepad_color)
	flags_lines[1].set_color(comms_color)
	flags_lines[2].set_color(shutdown_color)
		
	ref_chassis_vectors.set_segments(chassis_ref)
	est_chassis_vectors.set_segments(chassis_est)
	thrust_vectors.set_segments(thrust)
	thrust_vectors.set_color(thrust_colors)	

	modified = [ref_chassis_vectors, est_chassis_vectors, thrust_vectors, omega_line] + fuel_lines + flags_lines
	return modified

# Blitting works with 3d plots alone or 2d plots alone but fails with mixed plots. Try it. The 3d plot will not animate.
ani = animation.FuncAnimation(fig, animate, interval=50, blit=False)

# For use in post-flight simulation. May need to specify the number of frames.
# ani.save('/home/sushant/Documents/gravitypi/data/flight.gif', writer='imagemagick')

plt.show()