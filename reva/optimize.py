from cffi import FFI
import sys

# smbus2 (pure python wrapping /dev/i2c-* ioctl calls) 
# has a rw access rate of ~8bytes/ms for long burst reads, 
# and ~6bytes/ms for short burst reads, from Python

# pigpio (C wrappers for /dev/i2c-* ioctl calls)
# has a rw access rate of ~24bytes/ms for short burst reads in C.
# It's Python wrapper is incredibly slow for some reason (3bytes/ms).

# i2cbus (C wrappers for /dev/i2c-* ioctl calls) 
# also has a rw access rate of ~24bytes/ms for short burst reads in C,
# and it uses CFFI to speed up the Python wrappers and retain this rate
# even in Python.

if __name__ == "__main__":
	if (len(sys.argv) == 2) and sys.argv[1] == 'local':
		ignore_i2c = True
	else:
		ignore_i2c = False

	ffibuilder = FFI()

	if not ignore_i2c:
		ffibuilder.cdef(
			"""
			void i2cbus_open(void);
			void i2cbus_close(void);
			void i2cbus_implicit_read(uint8_t addr, uint8_t *dst, int n_bytes);
			void i2cbus_explicit_read(uint8_t addr, uint8_t reg, uint8_t *dst, int n_bytes);
			void i2cbus_implicit_write(uint8_t addr, uint8_t *src, int n_bytes);
			void i2cbus_explicit_write(uint8_t addr, uint8_t reg, uint8_t *src, int n_bytes);
			""")

	ffibuilder.cdef(
		"""
		/* Keep this a positive power of 2 at least 2x greater than our FIFO depths (32) */
		#define N_SAMPLES 128

		/* A vector queue with overwrite protection which interpolates timestamps between watermark samples. */

		struct vector_s {
			float t_s; /* Watermarks must be >= 0. Timestamps in need of interpolation must be negative. */
			float x;
			float y;
			float z;
		};

		struct sensor_configuration_s {
			int quadx_sensor_idx; 		/* 	Assuming an [x,y,z] = [0,1,2] input vector from the sensor frame,
											how should the axes be rotated to match the quadcopter frame? */
			int quady_sensor_idx;
			int quadz_sensor_idx;
			float x_scale; 				/* 	Scale from sensor readings to SI units. If these values are negative the axis will be reversed. */
			float y_scale; 			
			float z_scale;
			float x_offset; 			/* Zero-level offset */
			float y_offset;
			float z_offset;
			float nominal_sampling_period_s; 
			int fifo_wtm_lvl; 			/* Which index to watermark in a batch, when unpacking */
		};

		struct vector_queue_s {
			struct sensor_configuration_s cfg;
			struct vector_s circular_buffer[N_SAMPLES];
			int length;
			int wr_idx;
			int rd_idx;
			int wtm_idx;
			bool have_seen_wtm;
		};

		void vq_init(struct vector_queue_s *q, struct sensor_configuration_s *cfg);
		void vq_enqueue(struct vector_queue_s *q, struct vector_s *src);
		void vq_dequeue(struct vector_queue_s *q, struct vector_s *dst);
		void vq_unpack_le_words(struct vector_queue_s *q, float wtm_s, uint8_t *burst_xyz_le_interleaved_bytes, int n_samples);
		void vq_unpack_be_words(struct vector_queue_s *q, float wtm_s, uint8_t *burst_xyz_be_interleaved_bytes, int n_samples);
		""")

	ffibuilder.cdef(
		"""
		/* An IMU vector queue with overwrite protection, for time-synchronized vectors */

		enum sensor_e {
			ACCEL = 0,
			GYRO,
			COMPASS,
			
			/* Do not modify past this point manually */
			NUM_SENSORS,
		};

		struct imu_s {
			struct vector_s synchronized_sample[NUM_SENSORS];	
		};

		struct imu_queue_s {	
			struct imu_s circular_buffer[N_SAMPLES];
			int length;
			int wr_idx;
			int rd_idx;		
		};

		bool imuq_can_dequeue(struct imu_queue_s *q);
		void imuq_dequeue(struct imu_queue_s *q, struct imu_s *dst);

		/* A debugging queue for all kinds of vectors, without any overwrite protection. */

		struct debugging_queue_s {		
			struct vector_s circular_buffer[N_SAMPLES];
			int datatype[N_SAMPLES];
			int length;
			int wr_idx;
			int rd_idx;			
		};

		bool dbq_can_dequeue(struct debugging_queue_s *q);
		void dbq_dequeue(struct debugging_queue_s *q, struct vector_s *dst, enum sensor_e *datatype);

		/* 	The sync runs off 20Hz system ticks.
			20Hz system ticks are the last thing we initialize. 
			All sensors are running before they start.
			For sensors sampling between 30-100Hz in batches of 10-30Hz, 
			We can safely say that for any 2 system ticks we'll be sure
			of a batch from every single sensor.

			This means if we intend to delay the sync by, say, 3 systicks, 
			we just need to skip the first 6 ticks or so, to be sure
			of finding samples both earlier and later than the delayed time
			window we're looking for. This is exactly what we need
			for a linearly interpolating synchronizer. */
		struct sync_cfg_s {
			float delay_s;
			float systick_s;
			int n_ticks_to_skip;
			float sampling_period_s;
		};

		struct sensor_window_s {
			struct vector_s lower;
			struct vector_s upper;
		};

		struct sync_s {
			struct sync_cfg_s configuration;
			int n_skipped_ticks;

			bool has_started;
			float t_sync_s;

			struct debugging_queue_s inputq;
			struct imu_queue_s outputq;

			struct vector_queue_s *sensor_qs[NUM_SENSORS];
			
			bool initialized_sensor_windows;
			struct sensor_window_s windows[NUM_SENSORS];
		};

		void sync_init(	struct sync_s *s,
						struct sync_cfg_s *configuration,
						struct vector_queue_s *accelq, 
						struct vector_queue_s *gyroq, 
						struct vector_queue_s *compassq);
		struct debugging_queue_s *sync_get_inq(struct sync_s *s);
		struct imu_queue_s *sync_get_outq(struct sync_s *s);
		void sync_process_systick(	struct sync_s *s, 
									float systick_s);	
		""")

	ffibuilder.cdef(
		"""
		struct vect_s {
			float x;
			float y;
			float z;
		};

		struct quat_s {
			float r;
			struct vect_s v;
		};

		struct mcf_cfg_s {
			float sampling_period_s;
			int n_zero_lvl_samples;
			struct quat_s q_imu_to_quad; 
			float soft_iron[9];	
			struct vect_s hard_iron;
			float beta;
			float zeta;
		};

		struct mcf_s {
			struct mcf_cfg_s cfg;
			int n_zero_lvl_samples;
			struct vect_s w_zero_lvl;
			struct vect_s g_level;
			struct vect_s m_level;
			struct vect_s int_we;
			struct quat_s w_est;
			struct quat_s q_est;
			struct quat_s ddt_q;
		};

		float l2_vect_norm(struct vect_s *v);
		void scale_vect(struct vect_s *v, float s, struct vect_s *u);

		void quat_product(struct quat_s *p, struct quat_s *q, struct quat_s *pq, bool normalize);
		void quat_inverse(struct quat_s *q, struct quat_s *u);
		void quat_rotation(struct vect_s *v, struct quat_s *qr, struct vect_s *u);

		void mcf_gradient(struct mcf_s *self, struct quat_s *q, struct vect_s *a, struct vect_s *b, struct vect_s *m, struct quat_s *u);
		void mcf_init(struct mcf_s *self, struct mcf_cfg_s *cfg);
		void process_imu(struct mcf_s *self, struct imu_queue_s *synced, struct quat_s *q_est, struct vect_s *w_eff);
		""")

	if not ignore_i2c:
		ffibuilder.set_source("_optimizations",
		"""
			#include "i2cbus.h"
			#include "vector_queue.h"
			#include "sync.h"
			#include "state_estimation.h"
		""",
		sources=["i2cbus.c", "vector_queue.c", "sync.c", "state_estimation.c"])
	else:
		ffibuilder.set_source("_optimizations",
		"""			
			#include "vector_queue.h"
			#include "sync.h"
			#include "state_estimation.h"
		""",
		sources=["vector_queue.c", "sync.c", "state_estimation.c"])

	ffibuilder.compile(verbose=True)