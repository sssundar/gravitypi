from logger import Logger

from _optimizations import ffi as ffi
from _optimizations import lib as lib
import wiringpi
from time import time

from synchronizer import Synchronizer
from compass import Compass
from accelerometer import Accelerometer
from gyro import Gyro
from state_estimator import MadgwickComplementaryFilter
from pilot_interface import PilotInterface
from controller import PIDController
from battery import ADC
from pwm import PWMExtender

from systick import generate_periodic_events

# A cooperative scheduler managing I2C bus access 
# and numerical computation for Drone Rev A. Only
# one instance of this object exists: drone_ctx.
class Scheduler():
	def __init__(self):	
		return

	# we separate setup from __init__ (object creation)
	# to avoid circular dependency on this file, which
	# exposes a global Scheduler named drone_ctx
	def setup(self, log_level='info'):
		self.name = "scheduler"
		self.start_time_s = time()
		self.logger = Logger(self, log_level)	

		wiringpi.wiringPiSetup()
		lib.i2cbus_open()

		self.tasks = { 		# 	Scheduled 	Request Timestamp (s)
			"run_sync": 		[False, 	None],
			"get_heading": 		[False, 	None],
			"get_acceleration": [False, 	None],
			"get_rotation": 	[False, 	None],			
			"get_pilot": 		[False, 	None],
			"get_battery": 		[False, 	None],			
		}
		self.can_slip = [			
			"get_pilot",
			"get_battery",
		]
						
		self.pilot = PilotInterface(self.logger)  											# 10Hz (driven by system ticks)
		self.battery = ADC(self.logger, self.pilot)											# 1Hz  (driven by system ticks)
		self.pwm = PWMExtender(self.logger, self.battery) 									# 20Hz (driven by Controller)
		self.controller = PIDController(self.logger, self.pilot, self.pwm) 					# 20Hz (driven by MCF)
		self.state_estimator = MadgwickComplementaryFilter(self.logger, self.controller) 	# 100Hz (20Hz batched) (driven by Synchronizer)

		# sensors generate interrupts, and must be rebooted, which introduces delay
		# sensors are initialized by event generation rate (slowest to fastest) to minimize 
		# the chance of a dropped event on boot
		self.accelerometer = Accelerometer(self.logger)			 						# 10Hz
		self.gyro = Gyro(self.logger) 													# 10Hz		
		self.compass = Compass(self.logger) 											# 30Hz

		self.sync = Synchronizer(self.logger, self, self.state_estimator)				# 20Hz (driven by system ticks)

		# drives system ticks
		generate_periodic_events() 														# 20Hz, 10Hz, 1Hz

		self.task_priority = [
			["run_sync", self.sync],
			["get_heading", self.compass],
			["get_acceleration", self.accelerometer],
			["get_rotation", self.gyro],
			["get_pilot", self.pilot],
			["get_battery",	self.battery],
		]

		self.components = [
			self.sync,
			self.compass,
			self.accelerometer,
			self.gyro,
			self.state_estimator,			
			self.controller,
			self.battery,
			self.pwm,
			self.logger, 			# We do this as late as we can, to capture failures in deinit as well.
			self.pilot, 			# Req-ack with the Pilot on the ground requires that the PilotInterface deinitializes last
		]
		return		

	def deinit(self):		
		for component in self.components:
			component.deinit()		
		lib.i2cbus_close()

	def schedule_task(self, task):
		was_flagged, flagged_at = self.tasks[task]
		self.tasks[task] = [True, time()-self.start_time_s]
		if was_flagged and (task not in self.can_slip):
			self.logger.error(self, "dropped " + task + " requested at %0.3f" % flagged_at)

	def spin(self):
		try:
			while not self.pilot.shutdown:
				for task, handler in self.task_priority:
					was_flagged, flagged_at = self.tasks[task]
					if was_flagged:
						handler.handle_task(flagged_at)
						self.tasks[task] = [False, None]
						break
		except KeyboardInterrupt:
			pass
		self.deinit()			

def init():
	global drone_ctx
	drone_ctx = Scheduler()

