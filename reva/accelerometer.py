from board import ACCEL_INTERRUPT_PIN
from helpers import hexify
from time import time, sleep
from _optimizations import ffi as ffi
from _optimizations import lib as lib
import numpy as np

def configure_accelerometer_vq(queue_cfg, nominal_sampling_period_s, fifo_wtm_lvl):
									# A rotation to a common sensor frame
										# No rotation required because Adafruit 1604 IMU 
										# already aligns sensors to a common sensor frame.
	queue_cfg.quadx_sensor_idx = 0 	# X to X
	queue_cfg.quady_sensor_idx = 1 	# Y to Y
	queue_cfg.quadz_sensor_idx = 2 	# Z to Z
	queue_cfg.x_scale = 16133.33/12 				# units per gravity # TODO Calibrate this properly. 
	queue_cfg.y_scale = 16562.67/12
	queue_cfg.z_scale = 16781.33/12
	queue_cfg.x_offset = -0.027  					# gravities, subtracted
	queue_cfg.y_offset = -0.003
	queue_cfg.z_offset = 0.027
	queue_cfg.nominal_sampling_period_s = nominal_sampling_period_s
	queue_cfg.fifo_wtm_lvl = fifo_wtm_lvl
	return

# LSM303DLHC Accelerometer. 1MHz I2C max.
class Accelerometer():
	def __init__(self, logger):
		self.name = "accel"
		self.logger = logger

		# i2c constants
		self.address = 0x19
		self.register_address_autoincrement = 0x80
		
		# device configuration
		self.configuration = [
			# 	Reg  	Value 	Delay 	Verify 	Description
			[	0x24, 	0x80, 	0,		False, 	"CTRL_REG5_A: Request reboot of memory content, so all sensors (incl. compass) disabled."],	
			[	0x20, 	0x00, 	0.02,	False, 	"CTRL_REG1_A: Actually reboot."], # Undocumented. Learned about this from L3GD20H datasheet.
			[	0x22, 	0x04, 	0, 		True, 	"CTRL_REG3_A: FIFO watermark interrupt enabled on LINT1."],
			[	0x23, 	0xB0, 	0, 		True, 	"CTRL_REG4_A: Block update in little endian (LSB first) with +/-16g resolution"],
			[	0x24, 	0x40, 	0, 		True, 	"CTRL_REG5_A: FIFO enabled without latching of interrupt requests."],
			[	0x2E, 	0x00, 	0, 		False,	"FIFO_CTRL_REG_A: Clear mode bits to wipe away any error state."],
			[	0x2E, 	0x88, 	0, 		True, 	"FIFO_CTRL_REG_A: FIFO streaming. Discards older samples if full. FIFO threshold at 8/32 samples."],
			[	0x20, 	0x57, 	0, 		True, 	"CTRL_REG1_A: Start 100Hz sampling of x, y, z."],
		]
		self.nominal_sampling_period_s = 0.01
		self.fifo_wtm_lvl = 8		
		
		# fifo error flags and data address
		self.fifo_src_reg_a = 0x2F 
		self.fifo_wtm_flag  = 0x80 
		self.fifo_ovr_flag  = 0x40
		self.fifo_fill_lvl  = 0x1F
		self.fifo_data_addr = 0x28 # one 32-element FIFO per acceleration axis
		self.max_fifo_samples = 32
		self.sample_bytes   = 6    # x(lsb)x(msb) y(lsb)y(msb) z(lsb)z(msb)

		# preallocated i2c buffers
		self.src = np.zeros(self.max_fifo_samples * self.sample_bytes, dtype='uint8')
		self.src_ptr = ffi.cast("uint8_t *", self.src.ctypes.data)
		self.dst = self.src
		self.dst_ptr = self.src_ptr		

		# sample batches, in gravities, with second timestamps extrapolated between watermarks
		# for downstream synchronization with other sensors
		self.vector_queue = ffi.new("struct vector_queue_s *")
		self.queue_cfg = ffi.new("struct sensor_configuration_s *")
		configure_accelerometer_vq(self.queue_cfg, self.nominal_sampling_period_s, self.fifo_wtm_lvl)
		lib.vq_init(self.vector_queue, self.queue_cfg)		

		# configure the Pi to receive interrupts		
		import wiringpi # to allow access to configure_*_vq for offline tests on devices (like a laptop) without wiringpi
		wiringpi.pinMode(ACCEL_INTERRUPT_PIN, wiringpi.GPIO.INPUT)
		wiringpi.pullUpDnControl(ACCEL_INTERRUPT_PIN, wiringpi.GPIO.PUD_DOWN)
		from callbacks import accel_callback # to avoid circular dependency on scheduler.py in import
		wiringpi.wiringPiISR(ACCEL_INTERRUPT_PIN, wiringpi.GPIO.INT_EDGE_RISING, accel_callback)
		
		# configure the accelerometer
		self.configure()
		return

	def configure(self):
		for addr, value, delay, _, _ in self.configuration:
			self.src[0] = value
			lib.i2cbus_explicit_write(self.address, addr, self.src_ptr, 1)
			if delay > 0:
				sleep(delay)

		passed = True
		for addr, expected, _, check, _ in self.configuration:
			if check:
				lib.i2cbus_explicit_read(self.address, addr, self.dst_ptr, 1)
				byte = self.dst[0]
				passed = passed and (byte == expected)		
				
		if not passed:
			self.logger.error(self, "misconfigured")

	def deinit(self):
		import wiringpi # to allow access to configure_*_vq for offline tests on devices (like a laptop) without wiringpi
		from callbacks import null_callback # to avoid circular dependency on scheduler.py in import
		wiringpi.wiringPiISR(ACCEL_INTERRUPT_PIN, wiringpi.GPIO.INT_EDGE_RISING, null_callback)
		return		

	# empty the accelerometer FIFO
	# we will only call this function on a watermark interrupt
	def handle_task(self, flagged_at):
		# grab the status of the fifo
		lib.i2cbus_explicit_read(self.address, self.fifo_src_reg_a, self.dst_ptr, 1)
		status = self.dst[0]		
		ready = (status & self.fifo_wtm_flag) != 0
		overrun = (status & self.fifo_ovr_flag) != 0
		n_samples = status & self.fifo_fill_lvl

		if overrun:
			self.logger.error(self, "overrun")

		if not ready:
			return			

		if n_samples < (self.fifo_wtm_lvl+1):
			self.logger.error(self, "watermark timing assumption broken")
		
		# Burst read
		lib.i2cbus_explicit_read(self.address, self.register_address_autoincrement | self.fifo_data_addr, self.dst_ptr, self.sample_bytes * n_samples);
		lib.vq_unpack_le_words(self.vector_queue, flagged_at, self.dst_ptr, n_samples)
		self.logger.trace(self, (flagged_at, n_samples, self.dst))
		return