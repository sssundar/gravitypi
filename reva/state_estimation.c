/*  A C implementation of the MCF and related quaternion operations.
	Ought to be a drop-in replacement for reva/PyMCF and reva/quaternions.py.
	For the original derivations, see the MCF paper and GitHub/sssundar/Drone/simulator/estimation.py.	

	Numerical precision is likely to be dwarfed by sensor noise so
	the goal at this time is not the exact reproduction of numpy outputs 
	(e.g. sorting floating point additions, etc.) but rather a qualitative 
	similarity in behavior in offline trace simulation. */

/* No idea why CFFI adds this. Wasn't seeing how to remove it in docs & didn't feel like blocking on it. */
#ifdef NDEBUG
	#undef NDEBUG
#endif
#include <assert.h>

#include <string.h>
#include <math.h>
#include "state_estimation.h"

/* Returns u = <v, w> */
static float dot(struct vect_s *v, struct vect_s *w) {
	return (v->x * w->x) + (v->y * w->y) + (v->z * w->z);
}

/* Returns u = v x w */
static void cross(struct vect_s *v, struct vect_s *w, struct vect_s *u) {
	u->x = v->y*w->z - v->z*w->y;
	u->y = v->z*w->x - v->x*w->z;
	u->z = v->x*w->y - v->y*w->x;
}

/* Returns the L2 norm of w */
float l2_vect_norm(struct vect_s *v) {
	return sqrtf(dot(v, v));
}

/* Returns the L2 norm of q */
static float l2_quat_norm(struct quat_s *q) {
	return sqrtf(q->r*q->r + dot(&q->v, &q->v));
}

/* Returns u = s*v */
void scale_vect(struct vect_s *v, float s, struct vect_s *u) {
	u->x = s * v->x;
	u->y = s * v->y;
	u->z = s * v->z;
}

/* Returns u = u + v */
static void accumulate_vect(struct vect_s *u, struct vect_s *v) {
	u->x = u->x + v->x;
	u->y = u->y + v->y;
	u->z = u->z + v->z;
}

/* Sets u to the zero vector */
static void zero_vect(struct vect_s *u) {
	u->x = 0.f;
	u->y = 0.f;
	u->z = 0.f;
}

/* Returns u = s*q */
static void scale_quat(struct quat_s *q, float s, struct quat_s *u) {
	u->r = s * q->r;
	scale_vect(&q->v, s, &u->v);
}

/* Returns pq, possibly normalized by its L2 norm */
void quat_product(struct quat_s *p, struct quat_s *q, struct quat_s *pq, bool normalize) {
	pq->r = p->r*q->r - dot(&p->v,&q->v);
	
	/* p->r*q->v + q->r*p->v + cross(p->v,q->v) */
	zero_vect(&pq->v);
	struct vect_s u;
	scale_vect(&q->v, p->r, &u);
	accumulate_vect(&pq->v, &u);
	scale_vect(&p->v, q->r, &u);
	accumulate_vect(&pq->v, &u);
	cross(&p->v, &q->v, &u);
	accumulate_vect(&pq->v, &u);

	if (normalize) {		
		scale_quat(pq, 1.f/l2_quat_norm(pq), pq);
	}
}

/* Returns u = q* */
static void quat_conjugate(struct quat_s *q, struct quat_s *u) {
	u->r = q->r;
	scale_vect(&q->v, -1.f, &u->v);
}

/* Return u = q^-1 */ 
void quat_inverse(struct quat_s *q, struct quat_s *u) {
	quat_conjugate(q, u);
	scale_quat(u, 1.f/(q->r*q->r + dot(&q->v, &q->v)), u);
}

/**
 * 	Takes v 
 *	Takes qr A rotation quaternion (unit L2 norm)
 *	Returns the quaternion product qr qv qr^-1, a 'pure vector' of the form [0, u] up to numerical error, as u
 */
void quat_rotation(struct vect_s *v, struct quat_s *qr, struct vect_s *u) {
	struct quat_s qr_inv;
	quat_inverse(qr, &qr_inv);

	struct quat_s qv;
	qv.r = 0.f;
	qv.v.x = v->x;
	qv.v.y = v->y;
	qv.v.z = v->z;

	struct quat_s accumulator;
	struct quat_s pure_vector;
	quat_product(qr, &qv, &accumulator, false);
	quat_product(&accumulator, &qr_inv, &pure_vector, false);

	u->x = pure_vector.v.x;
	u->y = pure_vector.v.y;
	u->z = pure_vector.v.z;
}

#define USE_DISTORTION_COMPENSATION 0

#if USE_DISTORTION_COMPENSATION
	/*	MCF eq. 31, 6-vector (2x 3-vectors stacked) representing alignment error 
		between the 2 estimated field vectors and the 2 'known' field vectors. 

		q is represents our last estimate of the rotation quaternion from the quad to Earth frame. 
		a is a normalized measurement of the acceleration in the quad frame
		b is a normalized distortion-compensated 'truth' for the local magnetic field in the earth frame
		m is a normalized measurement of the magnetic field in the quad frame */
	static void f_gb(struct quat_s *q, struct vect_s *a, struct vect_s *b, struct vect_s *m, float dst[6]) {
		dst[0] = 2*(q->v.x*q->v.z - q->r*q->v.y) - a->x;
		dst[1] = 2*(q->r*q->v.x + q->v.y*q->v.z) - a->y;
		dst[2] = 2*(0.5f - q->v.x*q->v.x - q->v.y*q->v.y) - a->z;
		dst[3] = 2*b->x*(0.5f - q->v.y*q->v.y - q->v.z*q->v.z) + 2*b->z*(q->v.x*q->v.z - q->r*q->v.y) - m->x;
		dst[4] = 2*b->x*(q->v.x*q->v.y - q->r*q->v.z) + 2*b->z*(q->r*q->v.x + q->v.y*q->v.z) - m->y;
		dst[5] = 2*b->x*(q->r*q->v.y + q->v.x*q->v.z) + 2*b->z*(0.5f - q->v.x*q->v.x - q->v.y*q->v.y) - m->z;
	}

	/* MCF eq. 32, Jacobian of our objective function (squared L2 norm of eq. 31). */
	static void j_gb(struct quat_s *q, struct vect_s *b, float dst[6][4]) {
		dst[0][0] = -2*q->v.y; 	dst[0][1] = 2*q->v.z; 	dst[0][2] = -2*q->r; 	dst[0][3] = 2*q->v.x;
		dst[1][0] = 2*q->v.x; 	dst[1][1] = 2*q->r; 	dst[1][2] = 2*q->v.z; 	dst[1][3] = 2*q->v.y;
		dst[2][0] = 0.f;		dst[2][1] = -4*q->v.x;	dst[2][2] = -4*q->v.y; 	dst[2][3] = 0.f;

		dst[3][0] = -2*b->z*q->v.y; 				dst[3][1] = 2*b->z*q->v.z; 					dst[3][2] = -4*b->x*q->v.y - 2*b->z*q->r;	dst[3][3] = -4*b->x*q->v.z + 2*b->z*q->v.x;
		dst[4][0] = -2*b->x*q->v.z + 2*b->z*q->v.x; dst[4][1] = 2*b->x*q->v.y + 2*b->z*q->r; 	dst[4][2] = 2*b->x*q->v.x + 2*b->z*q->v.z; 	dst[4][3] = -2*b->x*q->r + 2*b->z*q->v.y;
		dst[5][0] = 2*b->x*q->v.y; 					dst[5][1] = 2*b->x*q->v.z - 4*b->z*q->v.x; 	dst[5][2] = 2*b->x*q->r - 4*b->z*q->v.y; 	dst[5][3] = 2*b->x*q->v.x;
	}
#else 
	/*	MCF eq. 21, 6-vector (2x 3-vectors stacked) representing alignment error 
		between the 2 measured field vectors and 2 'measured when horizontal' field vectors. 
		
		self contains the calibrated field vectors
		q is represents our last estimate of the rotation quaternion from the quad to Earth frame. 
		a is a normalized measurement of the acceleration in the quad frame
		m is a normalized measurement of the local magnetic field in the quad frame */
	static void f_gb(struct mcf_s *self, struct quat_s *q, struct vect_s *a, struct vect_s *m, float dst[6]) {
		struct vect_s *d, *s;

		float q1 = q->r;
		float q2 = q->v.x;
		float q3 = q->v.y;
		float q4 = q->v.z;

		d = &self->g_level;
		s = a;

		dst[0] = 2*d->x*(0.5f - q3*q3 - q4*q4) + 2*d->y*(q1*q4 + q2*q3) + 2*d->z*(q2*q4 - q1*q3) - s->x;
		dst[1] = 2*d->x*(q2*q3 - q1*q4) + 2*d->y*(0.5f - q2*q2 - q4*q4) + 2*d->z*(q1*q2 + q3*q4) - s->y;
		dst[2] = 2*d->x*(q1*q3 + q2*q4) + 2*d->y*(q3*q4 - q1*q2) + 2*d->z*(0.5f - q2*q2 - q3*q3) - s->z;

		d = &self->m_level;
		s = m;

		dst[3] = 2*d->x*(0.5f - q3*q3 - q4*q4) + 2*d->y*(q1*q4 + q2*q3) + 2*d->z*(q2*q4 - q1*q3) - s->x;
		dst[4] = 2*d->x*(q2*q3 - q1*q4) + 2*d->y*(0.5f - q2*q2 - q4*q4) + 2*d->z*(q1*q2 + q3*q4) - s->y;
		dst[5] = 2*d->x*(q1*q3 + q2*q4) + 2*d->y*(q3*q4 - q1*q2) + 2*d->z*(0.5f - q2*q2 - q3*q3) - s->z;
	}

	/* MCF eq. 22, Jacobian of our objective function (squared L2 norm of eq. 31). */
	static void j_gb(struct mcf_s *self, struct quat_s *q, struct vect_s *a, struct vect_s *m, float dst[6][4]) {
		struct vect_s *d;

		float q1 = q->r;
		float q2 = q->v.x;
		float q3 = q->v.y;
		float q4 = q->v.z;

		d = &self->g_level;

		dst[0][0] = 2*d->y*q4 - 2*d->z*q3; 	dst[0][1] = 2*d->y*q3 + 2*d->z*q4;				dst[0][2] = -4*d->x*q3 + 2*d->y*q2 - 2*d->z*q1; dst[0][3] = -4*d->x*q4 + 2*d->y*q1 + 2*d->z*q2;
		dst[1][0] = -2*d->x*q4 + 2*d->z*q2; dst[1][1] = 2*d->x*q3 - 4*d->y*q2 + 2*d->z*q1;	dst[1][2] = 2*d->x*q2 + 2*d->z*q4; 				dst[1][3] = -2*d->x*q1 - 4*d->y*q4 + 2*d->z*q3;
		dst[2][0] = 2*d->x*q3 - 2*d->y*q2;	dst[2][1] = 2*d->x*q4 - 2*d->y*q1 - 4*d->z*q2;	dst[2][2] = 2*d->x*q1 + 2*d->y*q4 - 4*d->z*q3; 	dst[2][3] = 2*d->x*q2 + 2*d->y*q3;

		d = &self->m_level;

		dst[3][0] = 2*d->y*q4 - 2*d->z*q3; 	dst[3][1] = 2*d->y*q3 + 2*d->z*q4;				dst[3][2] = -4*d->x*q3 + 2*d->y*q2 - 2*d->z*q1; dst[3][3] = -4*d->x*q4 + 2*d->y*q1 + 2*d->z*q2;
		dst[4][0] = -2*d->x*q4 + 2*d->z*q2; dst[4][1] = 2*d->x*q3 - 4*d->y*q2 + 2*d->z*q1;	dst[4][2] = 2*d->x*q2 + 2*d->z*q4; 				dst[4][3] = -2*d->x*q1 - 4*d->y*q4 + 2*d->z*q3;
		dst[5][0] = 2*d->x*q3 - 2*d->y*q2;	dst[5][1] = 2*d->x*q4 - 2*d->y*q1 - 4*d->z*q2;	dst[5][2] = 2*d->x*q1 + 2*d->y*q4 - 4*d->z*q3; 	dst[5][3] = 2*d->x*q2 + 2*d->y*q3;
	}
#endif

/* Gradient u of ||f_gb||^2 with respect to q. Magnetic distortion compensated. */
void mcf_gradient(struct mcf_s *self, struct quat_s *q, struct vect_s *a, struct vect_s *b, struct vect_s *m, struct quat_s *u) {
	float f[6];
	float j[6][4];

	#if USE_DISTORTION_COMPENSATION
		(void) self;
		f_gb(q, a, b, m, f);
		j_gb(q, b, j);
	#else
		(void) b;
		f_gb(self, q, a, m, f);
		j_gb(self, q, a, m, j);
	#endif

	/* u = J^T f */
	u->r = 0.f;
	zero_vect(&u->v);
	for (int r = 0; r < 6; r++) {
		u->r += j[r][0]*f[r];
		u->v.x += j[r][1]*f[r];
		u->v.y += j[r][2]*f[r];
		u->v.z += j[r][3]*f[r];
	}
}

void mcf_init(struct mcf_s *self, struct mcf_cfg_s *cfg) {
	memset(self, 0, sizeof(*self));
	memcpy(&self->cfg, cfg, sizeof(self->cfg));

	/* Zero out the gyro *initial* fast zero-level offset integrator */
	zero_vect(&self->w_zero_lvl);
	zero_vect(&self->g_level);
	zero_vect(&self->m_level);
	self->n_zero_lvl_samples = 0;

	/* Zero out the gyro *in flight* slow zero-level offset integrator */
	zero_vect(&self->int_we);	

	/* Initialize to zero reported angular velocity */
	self->w_est.r = 0.f;
	zero_vect(&self->w_est.v);

	/* Initialize to the unit quaternion */
	self->q_est.r = 1.f;
	zero_vect(&self->q_est.v);

	/* Initialize to zero instantaneous estimated angular velocity */
	self->ddt_q.r = 0.f;
	zero_vect(&self->ddt_q.v);
}

/**
 *	Takes as input a raw magnetometer measurement, m, full scale.
 *  Modifies it in-place, removing the hard iron offset. Then, removes
 *  any soft-iron stretching. The resulting magnetic field measurement 
 *  ~lies on a unit sphere but should be normalized afterward.
 * 
 *  Let the soft-iron matrix be W^-1, and let the hard iron vector be h
 *  We are computing m_corrected = W^-1 (m - h), here.
 */
static void calibrate_compass(struct mcf_cfg_s *cfg, struct vect_s *m) {
	struct vect_s zero_level, accumulator;
	scale_vect(&cfg->hard_iron, -1.f, &zero_level);	
	accumulate_vect(m, &zero_level);
	accumulator.x = cfg->soft_iron[0] * m->x + cfg->soft_iron[1] * m->y + cfg->soft_iron[2] * m->z;
	accumulator.y = cfg->soft_iron[3] * m->x + cfg->soft_iron[4] * m->y + cfg->soft_iron[5] * m->z;	
	accumulator.z = cfg->soft_iron[6] * m->x + cfg->soft_iron[7] * m->y + cfg->soft_iron[8] * m->z;
	memcpy(m, &accumulator, sizeof(*m));
}

/**
 *	Driven at 20Hz by the Sychronizer. 
 *	Takes as input a queue of synchronized 100Hz 9-axis IMU-frame IMU data (accel, gyro, compass) 
 *	in units of gravities, radians per second, and gauss. Uses all data in the queue to estimate:
 *		the rotation quaternion from the quad frame to the Earth frame
 *		the quad body's angular velocity in the quad frame 
 */
void process_imu(struct mcf_s *self, struct imu_queue_s *synced, struct quat_s *q_est, struct vect_s *w_eff) {
	struct imu_s synced_sample;	
	struct vect_s imu_accel, imu_gyro, imu_compass;
	struct vect_s quad_accel, quad_gyro, quad_compass;
	struct vect_s distortion_compensated_compass;
	struct quat_s gradient, qe_dot, q_est_inv, q_we, q_dot;
	struct vect_s w_e, gyro_offset;
	float scale;

	struct quat_s qr; /* Rotation quaternion to transform from IMU to Quad frame */
	quat_inverse(&self->cfg.q_imu_to_quad, &qr);

	float dt = self->cfg.sampling_period_s;

	while (imuq_can_dequeue(synced)) {
		imuq_dequeue(synced, &synced_sample);

		/* Unpack sample. Normalize Accel, Compass readings. */
		imu_accel.x = synced_sample.synchronized_sample[ACCEL].x;
		imu_accel.y = synced_sample.synchronized_sample[ACCEL].y;
		imu_accel.z = synced_sample.synchronized_sample[ACCEL].z;
		scale = l2_vect_norm(&imu_accel);
		if (scale > 1E-9) {
			scale_vect(&imu_accel, 1.f/scale, &imu_accel);
		} else {
			imu_accel.x = 0.f;
			imu_accel.y = 0.f;
			imu_accel.z = 1.f;
		}

		imu_gyro.x = synced_sample.synchronized_sample[GYRO].x;
		imu_gyro.y = synced_sample.synchronized_sample[GYRO].y;
		imu_gyro.z = synced_sample.synchronized_sample[GYRO].z;		
		
		imu_compass.x = synced_sample.synchronized_sample[COMPASS].x;
		imu_compass.y = synced_sample.synchronized_sample[COMPASS].y;
		imu_compass.z = synced_sample.synchronized_sample[COMPASS].z;
		/* Remove hard/soft iron errors as best as we can from the compass */
		calibrate_compass(&self->cfg, &imu_compass);
		scale = l2_vect_norm(&imu_compass);
		scale_vect(&imu_compass, 1.f/scale, &imu_compass);

		/* Rotate IMU frame measurement to Quad frame. */
		quat_rotation(&imu_accel, &qr, &quad_accel);
		quat_rotation(&imu_gyro, &qr, &quad_gyro);
		quat_rotation(&imu_compass, &qr, &quad_compass);

		/* Magnetic Distortion Compensation */		
		quat_rotation(&quad_compass, &self->q_est, &distortion_compensated_compass);
		distortion_compensated_compass.x = sqrtf(distortion_compensated_compass.x*distortion_compensated_compass.x + distortion_compensated_compass.y*distortion_compensated_compass.y);
		distortion_compensated_compass.y = 0.f;

		/* 	For the first self->n_zero_lvl_samples just average the sensors assuming the quad is still & horizontal.
			This lets us get a quick estimate of int_we and keep zeta and beta small, less responsive to noise.
			It also lets us estimate gravity and the local magnetic field when we're horizontal, as field references. */
		if (self->n_zero_lvl_samples < self->cfg.n_zero_lvl_samples) {
			accumulate_vect(&self->w_zero_lvl, &quad_gyro);
			accumulate_vect(&self->g_level, &quad_accel);
			accumulate_vect(&self->m_level, &quad_compass);
			self->n_zero_lvl_samples += 1;
			continue;	
		} else if (self->n_zero_lvl_samples == self->cfg.n_zero_lvl_samples) {
			scale_vect(&self->w_zero_lvl, 1./self->cfg.n_zero_lvl_samples, &self->w_zero_lvl);
			
			scale_vect(&self->g_level, 1./self->cfg.n_zero_lvl_samples, &self->g_level);
			scale = l2_vect_norm(&self->g_level);
			scale_vect(&self->g_level, 1.f/scale, &self->g_level);

			scale_vect(&self->m_level, 1./self->cfg.n_zero_lvl_samples, &self->m_level);
			scale = l2_vect_norm(&self->m_level);
			scale_vect(&self->m_level, 1.f/scale, &self->m_level);

			self->n_zero_lvl_samples += 1;
		}

		/* Compute the gradient w.r.t. self->q_est of an objective function measuring reconstruction error of field vectors, then normalize it. */
		mcf_gradient(self, &self->q_est, &quad_accel, &distortion_compensated_compass, &quad_compass, &gradient);		
		scale = l2_quat_norm(&gradient);
		if (scale > 1E-9) {
			scale_quat(&gradient, 1./scale, &qe_dot);			
		} else {
			qe_dot.r = 0.f;
			zero_vect(&qe_dot.v);			
		}

		/* We use the normalized gradient to slightly adjust our q_est, so it is an effective w_error */
		quat_inverse(&self->q_est, &q_est_inv);
		quat_product(&q_est_inv, &qe_dot, &q_we, false);
		scale_quat(&q_we, 2., &q_we);
		w_e.x = q_we.v.x;
		w_e.y = q_we.v.y;
		w_e.z = q_we.v.z;

		/* Integrating (low pass filtering) this effective w_error yields a value proportional to the zero-level gyro offset! */
		scale_vect(&w_e, dt, &w_e);
		accumulate_vect(&self->int_we, &w_e);

		/* Compute the quaternion which represents the action of our measured angular velocity, sans our estimated zero-level gyro offset */
		scale_vect(&self->int_we, self->cfg.zeta, &gyro_offset);		
		self->w_est.r = 0.f;
		self->w_est.v.x = quad_gyro.x - gyro_offset.x - self->w_zero_lvl.x;
		self->w_est.v.y = quad_gyro.y - gyro_offset.y - self->w_zero_lvl.y;
		self->w_est.v.z = quad_gyro.z - gyro_offset.z - self->w_zero_lvl.z;

		quat_product(&self->q_est, &self->w_est, &q_dot, false);
		scale_quat(&q_dot, 0.5f, &q_dot);

		/* Apply our corrected angular velocity alongside a step of stochastic gradient descent! */		
		self->ddt_q.r = q_dot.r - self->cfg.beta * qe_dot.r;
		self->ddt_q.v.x = q_dot.v.x - self->cfg.beta * qe_dot.v.x;
		self->ddt_q.v.y = q_dot.v.y - self->cfg.beta * qe_dot.v.y;
		self->ddt_q.v.z = q_dot.v.z - self->cfg.beta * qe_dot.v.z;

		self->q_est.r += self->ddt_q.r * dt;
		self->q_est.v.x += self->ddt_q.v.x * dt;
		self->q_est.v.y += self->ddt_q.v.y * dt;
		self->q_est.v.z += self->ddt_q.v.z * dt;
		
		/* A quaternion representing rotation must be of unit norm */
		scale = l2_quat_norm(&self->q_est);
		scale_quat(&self->q_est, 1./scale, &self->q_est);
	}

	/* 	Return our final state estimates

		Technically we could convert self->ddt_q to w_eff but
		stochastic gradient descent is really noisy. Let our 
		controller see the low-pass filtered w_est.	*/
	w_eff->x = self->w_est.v.x;
	w_eff->y = self->w_est.v.y;
	w_eff->z = self->w_est.v.z;

	q_est->r = self->q_est.r;
	q_est->v.x = self->q_est.v.x;
	q_est->v.y = self->q_est.v.y;
	q_est->v.z = self->q_est.v.z;
}
