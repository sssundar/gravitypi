import sys, os

from _optimizations import ffi as ffi
from _optimizations import lib as lib
import numpy as np

from time import time, sleep
from board import *
from helpers import mail
from accelerometer import configure_accelerometer_vq
from gyro import configure_gyro_vq
from compass import configure_compass_vq
from synchronizer import Synchronizer
from state_estimator import MadgwickComplementaryFilter, PyMCF
from battery import get_volts_per_lsb, get_voltage
from controller import PIDController

try:
	from frames import q_imu_to_quad
except:
	print("\nFATAL ERROR: Could not find IMU-to-Quad frame transformation. Please run reva/calibration.py offline.\n")
	raise

from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.animation as animation
from animate import *
import ipdb

# A sink for logs, unnecessary during simulation of a trace.
class DummyLogger():
	def __init__(self):
		return

	def deinit(self):
		return 

	def error(self, *argv):
		return		
	
	def warning(self, *argv):
		return		

	def info(self, *argv):
		return		

	def trace(self, *argv):
		return	

# A 'source' of controller inputs from the pilot interface.
# A 'sink' for battery, controller outputs.
# A 'source' for the dashboard in simulation as it has a full picture of the quad inputs, outputs.
class DummyPilotInterface():
	def __init__(self):				
		self.q_ref = [1.0, np.array([0., 0., 0.])]		
		self.throttle = 0.

		self.shutdown = False

		self.v_battery = 0.	
		self.q_est = [1.0, np.array([0., 0., 0.])]
		self.w_eff = np.array([0., 0., 0.])
		self.motor_duty_cycles = [0., 0., 0., 0.]

		self.outbox = None
		self.start_s = None
		self.cnt = 0
		return

	def deinit(self):
		if self.outbox is not None:
			self.outbox.close()
		return 

	def enable_dashboard(self, start_s=0.):
		self.start_s = start_s
		self.outbox = open(INBOX_DASHBOARD, "wb")		
		# This will kill itself if it doesn't see state updates regularly.		
		os.system(DASHBOARD_MAIN)

	def unpack(self, flagged_at, byte_list):		
		assert(len(byte_list) == PILOT_REQUESTS.size)			
		shutdown, q_ref, throttle = unpack_pilot(bytes(byte_list))
		self.shutdown = self.shutdown or shutdown		 
		self.q_ref = q_ref
		self.throttle = throttle

		if self.outbox is not None:
			mail(	self.outbox, 
					pack_dashboard(
						pilot_heartbeat=self.cnt, 
						w_eff=self.w_eff, 
						v_battery=self.v_battery, 
						q_ref=self.q_ref, 
						q_est=self.q_est, 
						motor_duty_cycles=self.motor_duty_cycles, 
						have_gamepad=True,
						sent_comms_req=True,
						got_comms_ack=True,
						is_comms_dead=False, 
						sent_shutdown_req=self.shutdown,
						got_shutdown_ack=self.shutdown, 
						shutdown_req_timed_out=False))
			self.cnt += 1	

			if flagged_at >= self.start_s:
				# User wanted visuals from self.start_s.
				# Start mimicing the pilot comms update frequency, for realistic dashboard simulations.
				sleep(0.1) 

	def update_fuel_gauge(self, v_bat):
		self.v_battery = v_bat

	def update_state(self, q_est, w_eff, motor_duty_cycles):
		self.q_est = q_est
		self.w_eff = w_eff
		self.motor_duty_cycles = motor_duty_cycles

	def get_throttle(self):
		return self.throttle

	def get_reference(self):
		return self.q_ref

# A sink for battery measurements
class DummyBattery():
	def __init__(self, pilot):
		self.pilot = pilot
		self.volts_per_lsb = get_volts_per_lsb()		

	def deinit(self):
		return 

	def unpack(self, flagged_at, byte_list, n_samples):
		# Convert byte list into a numpy array of type uint8
		raw_bytes = np.zeros(len(byte_list), dtype='uint8')
		np.copyto(raw_bytes, byte_list, casting='unsafe')		
		self.pilot.update_fuel_gauge(get_voltage(raw_bytes, self.volts_per_lsb))

# A sink for controller outputs generated using the current code
class DummyPWM():
	def __init__(self):
		pass

	def deinit(self):
		return 

	def update(self, motor_duty_cycles):
		pass

# A 'source' holding a vector_queue just as Accelerometer, Gyro, Compass do.
# Anything downstream of the synchronizer can treat the Simulator as the Scheduler
# because it holds correctly named references to these DummySensors & their queues. 
class DummySensor():
	def __init__(self, name):
		assert(name in ["accel", "gyro", "compass"])
		self.name = name

		self.vector_queue = ffi.new("struct vector_queue_s *")
		self.queue_cfg = ffi.new("struct sensor_configuration_s *")
		if name == "accel":
			configure_accelerometer_vq(self.queue_cfg, 0.01, 8)
			self.unpacker = lib.vq_unpack_le_words
		elif name == "gyro":
			configure_gyro_vq(self.queue_cfg, 0.01, 8)
			self.unpacker = lib.vq_unpack_le_words
		elif name == "compass":
			configure_compass_vq(self.queue_cfg, 0.033, 0)
			self.unpacker = lib.vq_unpack_be_words
		lib.vq_init(self.vector_queue, self.queue_cfg)

	def deinit(self):
		return 

	def unpack(self, flagged_at, byte_list, n_samples):	
		# Convert byte list into a numpy array of type uint8
		raw_bytes = np.zeros(len(byte_list), dtype='uint8')
		np.copyto(raw_bytes, byte_list, casting='unsafe')
		dst_ptr = ffi.cast("uint8_t *", raw_bytes.ctypes.data)
		self.unpacker(self.vector_queue, flagged_at, dst_ptr, n_samples)

# Exposes a process_imu() to 'sink' all Synchronizer output.
# Saves all original controller outputs parsed from the trace.
# 
# For physical modeling, these are the only relevant inputs, outputs. 
class IOSink():
	def __init__(self, simulator):
		self.simulator = simulator

		self.raw_sample = ffi.new("struct vector_s *")		
		self.datatype = ffi.new("enum sensor_e *")

		self.synced_sample = ffi.new("struct imu_s *")

		self.raw = {
			lib.ACCEL: {"t": [], "x": [], "y": [], "z": []},
			lib.GYRO: {"t": [], "x": [], "y": [], "z": []},
			lib.COMPASS: {"t": [], "x": [], "y": [], "z": []},
			}

		self.synced = {
			lib.ACCEL: {"t": [], "x": [], "y": [], "z": []},
			lib.GYRO: {"t": [], "x": [], "y": [], "z": []},
			lib.COMPASS: {"t": [], "x": [], "y": [], "z": []},
			}

		# See board.py/MOTOR_ORIENTATION for physical layout.
		self.pwm = {
			"t" : [],
			"duty_cycle_1" : [],
			"duty_cycle_2" : [],
			"duty_cycle_3" : [],
			"duty_cycle_4" : [],
		}
		return

	def deinit(self):
		return 

	def plot_synchronization(self):
		fig, ax = plt.subplots(3,1, sharex=True, sharey=False)
		for sensor in range(lib.NUM_SENSORS):
			ax[sensor].plot(self.raw[sensor]["t"], self.raw[sensor]["x"], 'r-', label="x")
			ax[sensor].plot(self.synced[sensor]["t"], self.synced[sensor]["x"], 'r.')		
			ax[sensor].plot(self.raw[sensor]["t"], self.raw[sensor]["y"], 'g-', label="y")
			ax[sensor].plot(self.synced[sensor]["t"], self.synced[sensor]["y"], 'g.')
			ax[sensor].plot(self.raw[sensor]["t"], self.raw[sensor]["z"], 'b-', label="z")
			ax[sensor].plot(self.synced[sensor]["t"], self.synced[sensor]["z"], 'b.')
		
		ax[lib.ACCEL].set_ylabel("[g]")
		ax[lib.GYRO].set_ylabel("[rad/s]")
		ax[lib.COMPASS].set_ylabel("[gauss]")
		ax[lib.COMPASS].legend()

		ax[0].set_title("Linearly Interpolating Time Synchronizer")
		ax[2].set_xlabel("time [s]")
		plt.show()

	def plot_magnetic_field(self, u=None, v=None, w=None, ax=None, static=False):
		n = len(self.raw[lib.COMPASS]["x"])
		x = np.array([0.]*n)
		y = np.array([0.]*n)
		z = np.array([0.]*n)
		u = u if (u is not None) else np.array(self.raw[lib.COMPASS]["x"])
		v = v if (v is not None) else np.array(self.raw[lib.COMPASS]["y"])
		w = w if (w is not None) else np.array(self.raw[lib.COMPASS]["z"])
		
		solo = ax is None
		if solo:
			fig = plt.figure()
			ax = fig.add_subplot(111, projection='3d')

		if static:
			ax.plot(u,v,w)
		else:
			segments = []		
			for idx in range(n):
				segments.append([[x[idx], y[idx], z[idx]], [u[idx], v[idx], w[idx]]])
			field = ax.quiver([0], [0], [0], [0], [0], [0], normalize=False)

		L = 1.5
		ax.set_xlim([-L,L])
		ax.set_ylim([-L,L])		
		ax.set_zlim([-L,L])		
		ax.set_xlabel("IMU x")
		ax.set_ylabel("IMU y")
		ax.set_zlabel("IMU z")

		def magnimate(i):
			field.set_segments([segments[i]])
			return field

		if not static:
			ani = animation.FuncAnimation(fig, magnimate, frames=n, interval=33, blit=False)
		if solo:
			plt.show()

	# Driven at 20Hz by the Sychronizer. 
	def process_imu(self, q_sync):
		q_raw = self.simulator.sync.get_input_queue()
		while lib.dbq_can_dequeue(q_raw):
			lib.dbq_dequeue(q_raw, self.raw_sample, self.datatype)
			sensor = self.datatype[0]
			data = self.raw_sample[0]
			self.raw[sensor]["t"].append(data.t_s)
			self.raw[sensor]["x"].append(data.x)
			self.raw[sensor]["y"].append(data.y)
			self.raw[sensor]["z"].append(data.z)

		while lib.imuq_can_dequeue(q_sync):
			lib.imuq_dequeue(q_sync, self.synced_sample)
			for sensor in range(lib.NUM_SENSORS):
				data = self.synced_sample[0].synchronized_sample[sensor]
				self.synced[sensor]["t"].append(data.t_s)
				self.synced[sensor]["x"].append(data.x)
				self.synced[sensor]["y"].append(data.y)
				self.synced[sensor]["z"].append(data.z)

	def save_pwm(self, output_s, motor_duty_cycles):
		self.pwm["t"].append(output_s)
		self.pwm["duty_cycle_1"].append(motor_duty_cycles[0])
		self.pwm["duty_cycle_2"].append(motor_duty_cycles[1])
		self.pwm["duty_cycle_3"].append(motor_duty_cycles[2])
		self.pwm["duty_cycle_4"].append(motor_duty_cycles[3])
		return

class DummyController():
	def __init__(self, pilot):
		self.pilot = pilot
		self.q = []
		self.w = []
		return

	def deinit(self):
		return 

	def process_state(self, q, w):
		self.q.append(q)
		self.w.append(w)
		self.pilot.update_state(q, w, [0.5, 0.5, 0.5, 0.5])
		return

# A mock of the Scheduler which just wires up sources, DUTs, and sinks.
class Simulator():
	def __init__(self, dut, trace, dashboard=False, dashboard_start_s=0.):
		self.trace = trace
		self.name = "simulator"
		self.logger = DummyLogger()
				
		self.pilot = DummyPilotInterface()
		self.battery = DummyBattery(self.pilot)
		self.pwm = DummyPWM()
		self.controller = DummyController(self.pilot)
		self.iosink = None

		if dut == "iosink":
			self.state_estimator = IOSink(self)
			self.iosink = self.state_estimator
		elif dut == "py_mcf":			
			self.state_estimator = PyMCF(self.logger, self.controller)
		elif dut == "c_mcf":			
			self.state_estimator = MadgwickComplementaryFilter(self.logger, self.controller)
		elif dut == "pid":
			self.controller = PIDController(self.logger, self.pilot, self.pwm)
			self.state_estimator = MadgwickComplementaryFilter(self.logger, self.controller)			
		else:
			print("DUT %s not yet supported." % dut)
			assert(False)
		
		if dashboard:
			self.pilot.enable_dashboard(start_s=dashboard_start_s)

		self.accelerometer = DummySensor("accel")
		self.gyro = DummySensor("gyro")
		self.compass = DummySensor("compass")

		self.sync = Synchronizer(self.logger, self, self.state_estimator)

		self.components = [
			self.logger,
			self.pilot,
			self.battery,
			self.pwm,
			self.controller,
			self.state_estimator,			
			self.accelerometer,
			self.gyro,
			self.compass,
			self.sync,
		]

	def deinit(self):
		for o in self.components:
			o.deinit()
		return

	# Simulates the trace in the order it was logged.
	def run(self):
		# Parse the trace into complete events
		# We are trying to parse logs such as:
		# 0.169, trace, compass, 0.164, 1, [  0 141 255 231 255 160]
		# 0.173, trace, accel, 0.139, 12, [128 250   0   2  64  66  64 250   0   2   0  65  64 250 128   2  64  66
		#   64 250  64   2  64  65  64 250 128   2  64  66  64 250   0   2 192  65
		#  128 250  64   2  64  66   0 250   0   2  64  66  64 250   0   2   0  66
		#    0 250   0   2   0  66 192 250 192   1 192  65 128 250 128   2 128  66]
		# 0.205, trace, compass, 0.204, 1, [  0 141 255 231 255 160]
		# 0.223, trace, sync, 0.223
		events = []
		current_event = None		
		with open(self.trace, "r") as trace:
			for line in trace:
				line = line.strip("\n")
				tokens = line.split(",")				
				if len(tokens) == 1:
					current_event += line
					continue
				else:
					if current_event is not None:
						events.append(current_event)						
					current_event = line
			if current_event is not None:
				events.append(current_event)

		# For each event:
		# Check if it's a trace log
		# Check the name of the source of the event
		# Find the right handler for this event
		# Feed the handler the remainder of the event for further parsing		
		for e in events:
			log_s, log_type, log_source, *data = [x.strip() for x in e.split(",")]
			log_s = float(log_s)			

			# Indicate which event we're on. If we assert partway through, this speeds up debugging!
			# Print a bunch of spaces after as some lines are longer than others and we're overwriting the
			# same line again and again.
			sys.stdout.write('\rSimulating %s-%s event logged at %0.3f.               ' % (log_type, log_source, log_s))
			sys.stdout.flush()
			if log_type == "trace":
				if log_source == "sync":
					flagged_at = float(data[0])
					self.sync.handle_task(flagged_at)
				elif log_source in ["accel", "gyro", "compass", "battery"]:
					flagged_at = float(data[0])
					byte_list = [int(x) for x in data[2][1:-1].strip().split()]
					n_samples = int(data[1])
					if log_source == "accel":
						self.accelerometer.unpack(flagged_at, byte_list, n_samples)
					elif log_source == "gyro":
						self.gyro.unpack(flagged_at, byte_list, n_samples)
					elif log_source == "compass":
						self.compass.unpack(flagged_at, byte_list, n_samples)
					elif log_source == "battery":
						self.battery.unpack(flagged_at, byte_list, n_samples)
				elif log_source == "pilot":
					flagged_at = float(data[0])
					byte_list = [int(x) for x in data[1][1:-1].strip().split()]
					self.pilot.unpack(flagged_at, byte_list)
				elif log_source == "pwm":
					output_at = float(data[0])
					clipped_motor_duty_cycles = [float(x) for x in data[1][1:-1].strip().split()]
					if self.iosink is not None:
						self.iosink.save_pwm(output_at, clipped_motor_duty_cycles)
				else:
					assert(False)
		sys.stdout.write("\n")
		sys.stdout.flush()
		self.deinit()

if __name__ == "__main__":
	# Parse simulation arguments
	n_args = len(sys.argv) - 1
	if n_args not in [1,2,3]:
		print("Usage: ")
		print("conda activate laptop")
		print("python simulator.py [sync|compass|mcf_regression] [relative/path/to/trace]")
		print("python simulator.py [py_mcf|c_mcf|pid] [relative/path/to/trace] [dashboard_start_s]")
		sys.exit(1)

	dut = sys.argv[1]	
	if (n_args >= 2):
		trace = sys.argv[2] 
	else:
		trace = "../logs/historical/trace_dashboard"

	if (n_args >= 3):
		dashboard_start_s = float(sys.argv[3])
	else:
		dashboard_start_s = 0.

	if dut == "sync":
		s = Simulator("iosink", trace)
		s.run()		
		# Visualize the output of the synchronizer (a good way to sanity check raw sensor data, too)
		s.iosink.plot_synchronization()
		s.iosink.plot_magnetic_field()
	elif dut == "compass":
		# Visualize the 3D spread of the compass in a trace to
		# validate that assumptions are met for soft-iron/hard-iron calibration.
		s = Simulator("iosink", trace)
		s.run()
		s.iosink.plot_magnetic_field(static=True)
	elif dut == "mcf_regression":
		spy = Simulator("py_mcf", trace)
		spy.run()
		sc = Simulator("c_mcf", trace)
		sc.run()

		n_vectors = len(spy.controller.q)
		assert(n_vectors == len(sc.controller.q))
		decimator = 1

		q_e0, q_e1, q_e2 = generate_body_frames(spy.controller.q)		
		r_e0, r_e1, r_e2 = generate_body_frames(sc.controller.q)

		compare(n_vectors, r_e0, r_e1, r_e2, q_e0, q_e1, q_e2, decimator)
	else:
		s = Simulator(dut, trace, dashboard=True, dashboard_start_s=dashboard_start_s)
		s.run()