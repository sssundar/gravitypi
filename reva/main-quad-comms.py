# References
# [1] http://blog.kevindoran.co/bluetooth-programming-with-python-3/

# TODO
# 	Comms both on the MainPilot, PilotInterface, and QuadComms side should
# 		be a finite state machine so it's easier to understand.

import sys
from board import *
from helpers import mail, read
import struct, bluetooth, os
from time import sleep, time
import numpy as np

# Defaults
default_inbox = pack_quad(shutdown=False, q_est=[1., np.array([0., 0., 0.])], w_eff=[0., 0., 0.], motor_duty_cycles=[0., 0., 0., 0.], v_battery=0.)
final_outbox = pack_pilot(shutdown=True, q=[1.0, np.array([0., 0., 0.])], throttle=0.)

# Clear my inbox, also making sure it exists.
inbox = open(INBOX_COMMS, "wb")
inbox.close()
inbox = open(INBOX_COMMS, "rb")

# Wipe any existing outbox.
outbox = open(OUTBOX_COMMS, "wb")

sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
sock.bind((RPI_MAC_ADDRESS, RPI_PORT))
sock.listen(1)

# You need to modify this call manually if you want deeper logging.
if (len(sys.argv) == 2) and (sys.argv[1] == 'trace'):
	os.system(QUADCTRL_TRACE)
else:
	os.system(QUADCTRL_MAIN)

try:
	pilot = None
	pilot, _ = sock.accept()
	while 1:
		data = pilot.recv(PILOT_REQUESTS.size)
		if data:
			mail(outbox, data)
		
		data = read(inbox, QUAD_STATE, None, default_inbox)
		pilot.send(data)
except: 
	if pilot is not None:
		pilot.close()	
	sock.close()
	
	# Give the QuadCtrl a chance to respond to all Pilot commands.
	sleep(1)	

	# If the inbox has a shutdown flag, the Pilot asked us to shut down. Turn off the RPi.
	# Otherwise, the Pilot ctrl-C'd out, so assume we're in development and just 
	# send the QuadCtrl a shutdown flag. Do not turn off the Pi.
	shutdown = read(inbox, QUAD_STATE, unpack_quad, default_inbox)[1][0]	 
	inbox.close()

	mail(outbox, final_outbox)
	outbox.close()

	if shutdown:
		os.system('sudo shutdown -h now')