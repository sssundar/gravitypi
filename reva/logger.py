from time import time
from board import SYSLOG

class Logger():
	def __init__(self, scheduler, log_level):
		self.name = "logger"		
		self.start_time_s = scheduler.start_time_s
		self.logfile = SYSLOG
		self.save_errors = log_level in ["error", "warning", "info", "trace"]
		self.save_warnings = log_level in ["warning", "info", "trace"]
		self.save_info = log_level in ["info", "trace"]
		self.save_trace = log_level in ["trace"]

		self.length = 2**15 # Approximately 6 minutes of deep flight tracing
		self.mod = self.length - 1
		self.wdx = 0		
		self.circular_buffer = [None] * self.length
		
		self.identity = lambda x: x + "\n"
		self.join = lambda x: x[0] + str(x[1]) + "\n"
		self.formatted_join = lambda x: x[0] + "[" + ' '.join(map(str, [y for y in x[1]])) + "]\n"
		return

	def log(self, message):
		self.circular_buffer[self.wdx] = message
		self.wdx = (self.wdx + 1) & self.mod

	def error(self, reporter, message):
		if self.save_errors:
			self.log([self.identity, "%0.3f, error, %s, %s" % (time()-self.start_time_s, reporter.name, message)])
	
	def warning(self, reporter, message):
		if self.save_warnings:
			self.log([self.identity, "%0.3f, warning, %s, %s" % (time()-self.start_time_s, reporter.name, message)])

	def info(self, reporter, message):
		if self.save_info:
			self.log([self.identity, "%0.3f, info, %s, %s" % (time()-self.start_time_s, reporter.name, message)])

	def trace(self, reporter, data):
		if self.save_trace:
			##
			# Inputs
			## 
			if reporter.name in ["accel", "gyro", "compass"]:				
				wtm_s = data[0]
				n_samples = data[1]				
				raw_bytes = data[2][0:n_samples*6].copy() # hardcoding 6 bytes per sample for these three sensors
				self.log([self.join, ["%0.3f, trace, %s, %0.3f, %d, " % (time()-self.start_time_s, reporter.name, wtm_s, n_samples), raw_bytes]])
			elif reporter.name == "sync":
				t_20Hz_tick_s = data
				self.log([self.identity, "%0.3f, trace, %s, %0.3f" % (time()-self.start_time_s, reporter.name, t_20Hz_tick_s)])
			elif reporter.name == "pilot":
				t_10Hz_tick_s = data[0]
				raw_bytes = data[1] # no need to copy, this is the output of a file.read()
				self.log([self.formatted_join, ["%0.3f, trace, %s, %0.3f, " % (time()-self.start_time_s, reporter.name, t_10Hz_tick_s), raw_bytes]])
			elif reporter.name == "battery":
				wtm_s = data[0]
				n_samples = data[1]				
				raw_bytes = data[2][0:n_samples*2].copy() # hardcoding 2 bytes per sample for the adc
				self.log([self.join, ["%0.3f, trace, %s, %0.3f, %d, " % (time()-self.start_time_s, reporter.name, wtm_s, n_samples), raw_bytes]])
			
			##
			# Outputs
			## 
			elif reporter.name == "pwm":
				clipped_motor_duty_cycles = data # no need to copy, the array we are passed is re-allocated
				t_pwm_s = time()-self.start_time_s
				self.log([self.formatted_join, ["%0.3f, trace, %s, %0.3f, " % (t_pwm_s, reporter.name, t_pwm_s), clipped_motor_duty_cycles]])

			else:
				assert(False)

	def deinit(self):
		print("Dumping logs...")		
		with open(self.logfile, "w") as logfile:
			idx = 0
			rdx = self.wdx
			while idx < self.length:				
				if self.circular_buffer[rdx] is not None:
					operator, data = self.circular_buffer[rdx]					
					logfile.write(operator(data))
				rdx = (rdx + 1) & self.mod
				idx += 1