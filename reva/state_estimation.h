#pragma once

#include "sync.h"

struct vect_s {
	float x;
	float y;
	float z;
};

struct quat_s {
	float r;
	struct vect_s v;
};

struct mcf_cfg_s {
	float sampling_period_s;
	int n_zero_lvl_samples;
	struct quat_s q_imu_to_quad; 
	float soft_iron[9]; 		/* See reva/calibration.py, W_inv */
	struct vect_s hard_iron; 	/* See reva/calibration.py, h */
	float beta;
	float zeta;
};

struct mcf_s {
	struct mcf_cfg_s cfg;
	int n_zero_lvl_samples;
	struct vect_s w_zero_lvl;
	struct vect_s g_level;
	struct vect_s m_level;
	struct vect_s int_we;
	struct quat_s w_est;
	struct quat_s q_est;
	struct quat_s ddt_q;
};

float l2_vect_norm(struct vect_s *v);
void scale_vect(struct vect_s *v, float s, struct vect_s *u);

void quat_product(struct quat_s *p, struct quat_s *q, struct quat_s *pq, bool normalize);
void quat_inverse(struct quat_s *q, struct quat_s *u);
void quat_rotation(struct vect_s *v, struct quat_s *qr, struct vect_s *u);

void mcf_gradient(struct mcf_s *self, struct quat_s *q, struct vect_s *a, struct vect_s *b, struct vect_s *m, struct quat_s *u);
void mcf_init(struct mcf_s *self, struct mcf_cfg_s *cfg);
void process_imu(struct mcf_s *self, struct imu_queue_s *synced, struct quat_s *q_est, struct vect_s *w_eff);
