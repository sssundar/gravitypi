#pragma once

#include <stdbool.h>
#include <stdint.h>

/* Keep this a positive power of 2 at least 2x greater than our FIFO depths (32) */
#define N_SAMPLES 128

/* A vector queue with overwrite protection which interpolates timestamps between watermark samples. */

struct vector_s {
	float t_s; /* Watermarks must be >= 0. Timestamps in need of interpolation must be negative. */
	float x;
	float y;
	float z;
};

struct sensor_configuration_s {
	int quadx_sensor_idx; 		/* 	Assuming an [x,y,z] = [0,1,2] input vector from the sensor frame,
									how should the axes be rotated to match the quadcopter frame? */
	int quady_sensor_idx;
	int quadz_sensor_idx;
	float x_scale; 				/* 	Scale from sensor readings to SI units. If these values are negative the axis will be reversed. */
	float y_scale; 			
	float z_scale;
	float x_offset; 			/* Zero-level offset */
	float y_offset;
	float z_offset;
	float nominal_sampling_period_s; 
	int fifo_wtm_lvl; 			/* Which index to watermark in a batch, when unpacking */
};

struct vector_queue_s {
	struct sensor_configuration_s cfg;
	struct vector_s circular_buffer[N_SAMPLES];
	int length;
	int wr_idx;
	int rd_idx;
	int wtm_idx;
	bool have_seen_wtm;
};

void vq_init(struct vector_queue_s *q, struct sensor_configuration_s *cfg);
void vq_enqueue(struct vector_queue_s *q, struct vector_s *src);
void vq_dequeue(struct vector_queue_s *q, struct vector_s *dst);
void vq_unpack_le_words(struct vector_queue_s *q, float wtm_s, uint8_t *burst_xyz_le_interleaved_bytes, int n_samples);
void vq_unpack_be_words(struct vector_queue_s *q, float wtm_s, uint8_t *burst_xyz_be_interleaved_bytes, int n_samples);
