from _optimizations import ffi as ffi
from _optimizations import lib as lib
import numpy as np
from board import MCF_ZETA, MCF_BETA
from quaternions import *

try:
	from frames import W_inv, h, q_imu_to_quad
except:
	print("\nFATAL ERROR: Could not find IMU-to-Quad frame transformation. Please run reva/calibration.py offline.\n")
	raise

# A wrapper for a C implementation of the MCF
class MadgwickComplementaryFilter():
	def __init__(self, logger, controller):
		self.name = "estimator"			
		self.logger = logger
		self.controller = controller

		self.cfg = ffi.new("struct mcf_cfg_s *")	
		self.cfg.sampling_period_s = 0.01;
		self.cfg.n_zero_lvl_samples = 100;
		self.cfg.q_imu_to_quad.r = q_imu_to_quad[0]
		self.cfg.q_imu_to_quad.v.x = q_imu_to_quad[1][0]
		self.cfg.q_imu_to_quad.v.y = q_imu_to_quad[1][1]
		self.cfg.q_imu_to_quad.v.z = q_imu_to_quad[1][2]
		self.cfg.soft_iron[0] = W_inv[0,0]
		self.cfg.soft_iron[1] = W_inv[0,1]
		self.cfg.soft_iron[2] = W_inv[0,2]
		self.cfg.soft_iron[3] = W_inv[1,0]
		self.cfg.soft_iron[4] = W_inv[1,1]
		self.cfg.soft_iron[5] = W_inv[1,2]
		self.cfg.soft_iron[6] = W_inv[2,0]
		self.cfg.soft_iron[7] = W_inv[2,1]
		self.cfg.soft_iron[8] = W_inv[2,2]
		self.cfg.hard_iron.x = h[0]
		self.cfg.hard_iron.y = h[1]
		self.cfg.hard_iron.z = h[2]
		self.cfg.beta = MCF_BETA
		self.cfg.zeta = MCF_ZETA
		self.mcf = ffi.new("struct mcf_s *")
		lib.mcf_init(self.mcf, self.cfg);

		# Placeholders for C state returned by lib.process_imu.
		self.q_est = ffi.new("struct quat_s *")
		self.w_eff = ffi.new("struct vect_s *")
		return
	
	def deinit(self):
		return		
	
	# Driven at 20Hz by the Sychronizer. 
	# Takes as input a queue of synchronized 100Hz 9-axis IMU-frame IMU data (accel, gyro, compass) 
	# in units of gravities, radians per second, and gauss. Uses all data in the queue to estimate 
	# the orientation of the quad. Notifies the PID controller.
	def process_imu(self, q_sync):
		lib.process_imu(self.mcf, q_sync, self.q_est, self.w_eff);
		q_est = [self.q_est.r, np.array([self.q_est.v.x, self.q_est.v.y, self.q_est.v.z])]
		w_eff = np.array([self.w_eff.x, self.w_eff.y, self.w_eff.z])
		self.controller.process_state(q_est, w_eff)
		return		

# A Python implementation of the MCF
# 	This is copied from my Github repo sssundar/Drone/simulator/estimator.py 
# 	It follows the MCF paper exactly. It was tested thoroughly in closed-loop 
#   using simulated quadcopter plant dynamics.
def f_g(q, a):
	r, v = q
	q = [r] + list(v)
	q1, q2, q3, q4 = q
	ax, ay, az = list(a)
	line1 = 2*(q2*q4 - q1*q3) - ax
	line2 = 2*(q1*q2 + q3*q4) - ay
	line3 = 2*(0.5 - q2**2 - q3**2) - az
	return np.array([line1, line2, line3])

def J_g(q):
	r, v = q
	q = [r] + list(v)
	q1, q2, q3, q4 = q
	line1 = [-2*q3, 2*q4, -2*q1, 2*q2]
	line2 = [2*q2, 2*q1, 2*q4, 2*q3]
	line3 = [0, -4*q2, -4*q3, 0]
	return np.matrix([line1, line2, line3])

def f_b(q, b, m):
	r, v = q
	q = [r] + list(v)
	q1, q2, q3, q4 = q
	bx, _, bz = list(b)
	mx, my, mz = list(m)
	line1 = 2*bx*(0.5 - q3**2 - q4**2) + 2*bz*(q2*q4 - q1*q3) - mx
	line2 = 2*bx*(q2*q3 - q1*q4) + 2*bz*(q1*q2 + q3*q4) - my
	line3 = 2*bx*(q1*q3 + q2*q4) + 2*bz*(0.5 - q2**2 - q3**2) - mz
	return np.array([line1, line2, line3])

def J_b(q, b):
	r, v = q
	q = [r] + list(v)
	q1, q2, q3, q4 = q
	bx, _, bz = b

	line1 = [-2*bz*q3, 2*bz*q4, -4*bx*q3 - 2*bz*q1, -4*bx*q4 + 2*bz*q2]
	line2 = [-2*bx*q4 + 2*bz*q2, 2*bx*q3 + 2*bz*q1, 2*bx*q2 + 2*bz*q4, -2*bx*q1 + 2*bz*q3]
	line3 = [2*bx*q3 , 2*bx*q4 - 4*bz*q2, 2*bx*q1 - 4*bz*q3, 2*bx*q2]

	return np.matrix([line1, line2, line3])

def f_gb(q, a, b, m):
	return np.array(list(f_g(q, a)) + list(f_b(q, b, m)))

def J_gb(q, b):
	return np.concatenate((J_g(q), J_b(q, b)), axis=0)

def gradient_f(q, a, b, m):
	return np.array(np.dot(np.transpose(J_gb(q, b)), f_gb(q, a, b, m)))[0]

class PyMCF():
	def __init__(self, logger, controller):
		self.name = "estimator"			
		self.logger = logger
		self.controller = controller

		self.calibrate_compass = lambda v: np.matmul(W_inv, v - h)
		self.to_quad_frame = lambda v: quaternion_rotation(qv=[0,v], qr=quaternion_inverse(q_imu_to_quad))[1]
		self.sampling_period_s = 0.01

		self.synced_sample = ffi.new("struct imu_s *")

		# Measured Gyro Noise Scales
		# 	Should match that of the observed sampling noise, slowly-varying fixed offset
		# 		Note zeta cannot compensate for beta & cannot be larger than beta.
		# 		Note beta can compensate for bias if it is large enough, but then you'll jitter. 
		# 			You don't want to make beta unnecessarily large, because acceleration
		# 			is very noisy with motors & we want to low-pass filter it.
		self.beta = MCF_BETA # ~3 dps error in all axes. The idea is to trust the gyro in the near-term and trust the proper acceleration vector (on average, gravity, if we're mostly horizontal, so tied to our controller) and compass heading (independent of our controller) in the long term.
		self.zeta = MCF_ZETA # ~3 dps bias error iid in all axes. If this is zero we don't use gyro bias compensation.

		# Bias Accumulator
		self.int_we = np.asarray([0.0,0.0,0.0])
		self.w_c = np.array([0,0,0])

		# Our estimate of the rotation of the body frame relative to the inertial frame.
		self.q = [1, np.asarray([0,0,0])]
		self.ddt_q = [0, np.asarray([0,0,0])]
		return

	def deinit(self):
		return	

	# Driven at 20Hz by the Sychronizer. 
	# Takes as input a queue of synchronized 100Hz 9-axis IMU-frame IMU data (accel, gyro, compass) 
	# in units of gravities, radians per second, and gauss. Uses all data in the queue to estimate 
	# the orientation of the quad. Notifies the PID controller. 
	def process_imu(self, q_sync):
		while lib.imuq_can_dequeue(q_sync):
			lib.imuq_dequeue(q_sync, self.synced_sample)

			# Accel, Gyro, Compass: [Normalize, Data]
			measurements = {
				lib.ACCEL : [True, None],
				lib.GYRO : [False, None],
				lib.COMPASS : [True, None],
				}

			for sensor in range(lib.NUM_SENSORS):
				sample = self.synced_sample[0].synchronized_sample[sensor]				
				must_normalize, _ = measurements[sensor]

				v_imu = np.array([sample.x, sample.y, sample.z])

				# Magnetic hard/soft iron calibration
				if sensor == lib.COMPASS:
					v_imu = self.calibrate_compass(v_imu)

				if must_normalize:
					v_norm = vector_norm(v_imu)
					if (sensor != lib.ACCEL) or (v_norm > 1E-9):
						v = v_imu / v_norm
					else:
						v = np.array([0.0, 0.0, 1.0])
				else:
					v = v_imu

				# Rotate IMU frame measurement to Quad frame.				
				v_quad = self.to_quad_frame(v)
				measurements[sensor][1] = v_quad
			
			# Estimate our orientation and rate of change of orientation
			dt = self.sampling_period_s			
			a_hat = measurements[lib.ACCEL][1]
			w_b = measurements[lib.GYRO][1]
			m_b = measurements[lib.COMPASS][1]

			# Magnetic Distortion Compensation
			m = m_b
			b = quaternion_rotation(qv=[0,m], qr=self.q)[1]
			b = np.asarray([np.sqrt(b[0]**2 + b[1]**2), 0, b[2]])

			grad_f = gradient_f(self.q, a_hat, b, m)

			if vector_norm(grad_f) > 1E-9:
				qe_dot = grad_f / vector_norm(grad_f)
				new_part = self.beta * qe_dot
			else:
				qe_dot = np.asarray([0,0,0,0])
				new_part = [0,0,0,0]
			qe_dot = [qe_dot[0], np.asarray([qe_dot[1], qe_dot[2], qe_dot[3]])]

			w_e = quaternion_times_scalar(scalar=2, quaternion=quaternion_product(quaternion_inverse(self.q), qe_dot, False))[1]
			self.int_we += w_e*dt
			self.w_c = w_b - self.zeta*self.int_we

			q_dot = quaternion_times_scalar(scalar=.5, quaternion=quaternion_product(self.q, [0, self.w_c], False))

			dq = [new_part[0], np.array([new_part[1], new_part[2], new_part[3]])]
			dq = [q_dot[0] - dq[0], q_dot[1] - dq[1]]
			dq = [dq[0] * dt, dq[1] * dt]

			q = [self.q[0] + dq[0], self.q[1] + dq[1]]
			self.q = [e / quaternion_norm(q)  for e in q]

		# Tell the controller our final updated state estimate.
		# Technically we could treat dq (pre dt-scaling) as w_eff but
		# stochastic gradient descent is noisy. We want our controller
		# to see the LPF filtered version.				
		self.controller.process_state(self.q, self.w_c)
