from board import *
from helpers import mail, read
import struct
from time import time
import numpy as np

# Communicates with the Comms thread via file IO. 
# Receives shutdown request, gamepad thrust, reference orientation.
# Sends shutdown state, battery level, estimated orientation, and motor duty cycles.
class PilotInterface():
	def __init__(self, logger):
		self.name = "pilot"
		self.logger = logger

		self.shutdown = False
		self.v_battery = 0.
		self.q_est = [1., np.array([0., 0., 0.])]
		self.w_eff = np.array([0., 0., 0.])
		self.motor_duty_cycles = [0., 0., 0., 0.]
		
		self.q_ref = [1., np.array([0., 0., 0.])]
		self.throttle = 0.

		self.default_pilot = pack_pilot(shutdown=False, q=[1.0, np.array([0., 0., 0.])], throttle=0.)
		
		# Clear my inbox, also making sure it exists.
		self.inbox = open(INBOX_CTRL, "wb")
		self.inbox.close()	
		self.inbox = open(INBOX_CTRL, "rb")

		# Wipe any existing outbox.
		self.outbox = open(OUTBOX_CTRL, "wb")
		return
	
	def deinit(self):
		mail(self.outbox, pack_quad(shutdown=self.shutdown, q_est=[1., np.array([0., 0., 0.])], w_eff=[0., 0., 0.], motor_duty_cycles=[0., 0., 0., 0.], v_battery=0.))
		self.inbox.close()
		self.outbox.close()
		return		

	def handle_task(self, flagged_at):
		data, (shutdown, q_ref, throttle) = read(self.inbox, PILOT_REQUESTS, unpack_pilot, self.default_pilot)

		self.shutdown = self.shutdown or shutdown
		self.q_ref = q_ref
		self.throttle = throttle

		# We do NOT feed through a shutdown request here. The req-ack for a shutdown request is as follows...
		#  Pilot sends shutdown
		#  We parse it here, notifying our scheduler.
		#  Scheduler deinits everyone else, dumping logs (which may take a while).
		#  Scheduler deinits this PilotInterface. At this time, we write the shutdown flag to file.
		#  Comms sends this over, Pilot sees it as an ACK of the shutdown, Pilot disconnects from comms.
		#  Comms checks its inbox and realizes we responded to a shutdown request.
		#  Comms asks RPi to shut down.
		#
		# Note if the Pilot did not request a shutdown (e.g. during development, just Ctrl-C'd out)
		# then Comms would see we did not respond to shutdown, send us a shutdown request, but NOT
		# as the RPi to shut down.
		mail(self.outbox, pack_quad(False, self.q_est, self.w_eff, self.motor_duty_cycles, self.v_battery))

		self.logger.trace(self, (flagged_at, data))

	def update_fuel_gauge(self, v_bat):
		self.v_battery = v_bat

	def update_state(self, q_est, w_eff, motor_duty_cycles):
		self.q_est = q_est
		self.w_eff = w_eff
		self.motor_duty_cycles = motor_duty_cycles

	def get_throttle(self):
		return self.throttle

	def get_reference(self):
		return self.q_ref