/* No idea why CFFI adds this. Wasn't seeing how to remove it in docs & didn't feel like blocking on it. */
#ifdef NDEBUG
   #undef NDEBUG
#endif
#include <assert.h>

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <asm/ioctl.h>

#include <linux/i2c-dev.h>
#include "board.h"

static int i2c_fd = -1;
void i2cbus_open(void) {      
   i2c_fd = open(I2C_BUS, O_RDWR);   
}

void i2cbus_close(void) {
   close(i2c_fd);
}

void i2cbus_implicit_read(uint8_t addr, uint8_t *dst, int n_bytes) {
   struct i2c_msg msg = {
      .addr = addr,
      .flags = I2C_M_RD, 
      .len = n_bytes,
      .buf = dst,
   };

   struct i2c_rdwr_ioctl_data transaction = {
      .msgs = &msg,
      .nmsgs = 1,
   };

   ioctl(i2c_fd, I2C_RDWR, &transaction);
}

void i2cbus_explicit_read(uint8_t addr, uint8_t reg, uint8_t *dst, int n_bytes) {
   /* SMBUS is limited to 32 byte burst reads; I2C is not. 
      There's probably a 256 byte limit in the kernel driver, 
      but we're never even close to that. */

   struct i2c_msg msgs[2] = {
      {
         .addr = addr,
         .flags = 0,
         .len = 1, 
         .buf = &reg,
      },
      {
         .addr = addr,
         .flags = I2C_M_RD, 
         .len = n_bytes,
         .buf = dst,
      },
   };

   struct i2c_rdwr_ioctl_data transaction = {
      .msgs = &msgs[0],
      .nmsgs = 2,
   };

   ioctl(i2c_fd, I2C_RDWR, &transaction);
}

void i2cbus_implicit_write(uint8_t addr, uint8_t *src, int n_bytes) {
   struct i2c_msg msg = {
      .addr = addr,
      .flags = 0, 
      .len = n_bytes,
      .buf = src,
   };

   struct i2c_rdwr_ioctl_data transaction = {
      .msgs = &msg,
      .nmsgs = 1,
   };

   ioctl(i2c_fd, I2C_RDWR, &transaction);
}

void i2cbus_explicit_write(uint8_t addr, uint8_t reg, uint8_t *src, int n_bytes) {
   /* This is not an I2C restriction, I just felt like avoiding a malloc & I don't need more than 20 bytes. */
   assert(n_bytes <= 20);

   uint8_t data[21];
   
   data[0] = reg;
   memcpy(&data[1], src, n_bytes);

   struct i2c_msg msg = {
      .addr = addr,
      .flags = 0, 
      .len = n_bytes+1,
      .buf = &data[0],
   };

   struct i2c_rdwr_ioctl_data transaction = {
      .msgs = &msg,
      .nmsgs = 1,
   };

   ioctl(i2c_fd, I2C_RDWR, &transaction);   
}

