import struct
import numpy as np

##
# Logging File
## 
SYSLOG = "/home/pi/gravitypi/logs/syslog"

##
# Default Calibration Traces
# Calibration has to be run offline from gravitypi/
# on your Pi (for flight) & Laptop (for simulation)
## 

# Chassis Iteration 0.
# DEFAULT_CALIBRATION_TRACES = {
# 	"compass sphere" : "logs/calibration/trace_compass_sphere_methodical_syslog",
# 	"axes" : "logs/calibration/trace_axes_syslog",
# 	"axes annotations" : "logs/calibration/axes_annotations", 
# }

# Chassis Iteration 1. Replaced Motor 3, remounted RPi and battery. 
# DEFAULT_CALIBRATION_TRACES = {
# 	"compass sphere" : "logs/calibration/trace_compass_sphere_methodical_iter1_syslog",
# 	"axes" : "logs/calibration/trace_axes_iter1_syslog",
# 	"axes annotations" : "logs/calibration/axes_annotations_iter1",
# }

# Chassis Iteration 2. Replaced Motor 4. Board/IMU untouched.
DEFAULT_CALIBRATION_TRACES = {
	"compass sphere" : "logs/calibration/trace_compass_sphere_methodical_iter2_syslog",
	"axes" : "logs/calibration/trace_axes_iter1_syslog",
	"axes annotations" : "logs/calibration/axes_annotations_iter1",
}

## 
# Configuration allowing dynamics-based parameter estimation.
# Should we ask the quad to prepare for large angular velocities?
# Which motor would you like to pulse, and how strongly?
CAPTURING_DYNAMICS = False
PULSE_MOTOR_DUTY_CYCLE = [0, 0, 0, 0] # See MOTOR_ORIENTATION. 1, 2, 3, 4.
PULSE_MOTOR_DURATION_S = 3

##
# Lowest Recommended Battery Voltage
# for your LiPo (mine is 3.5V per cell, 2S).
##
LOWEST_RECOMMENDED_BATTERY_VOLTAGE = 7.0

##
# LSM303DLHC accelerometer 
#  interrupt line in WiringPi pin numbering
#  relative to the RPI Zero W. 
##
ACCEL_INTERRUPT_PIN = 2

##
# LSM303DLHC compass 
#  interrupt line in WiringPi pin numbering
#  relative to the RPI Zero W. 
##
COMPASS_INTERRUPT_PIN = 0

##
# L3GD20H gyrometer 
#  interrupt line in WiringPi pin numbering
#  relative to the RPI Zero W. 
##
GYRO_INTERRUPT_PIN = 3

##
# Motor Orientation in Quad Frame
#
# Orientation Key
# 	M = minus. P = plus.
# 	[123] = [e1,e2,e3] = [x,y,z]
# 	P3 => CCW (white, black motor wires).
# 	M3 => CW (red, blue motor wires).
#
# You will have to update this based on
# the axes you define for your quad body frame
# and how you wire up your quad chassis.
# My quad was built as follows:
#
# 	  PWM#3 			   PWM#4
# CCW M1P2P3---------------P1P2M3 CW	  e2,y
# 			  IMU || 					  |
# 			 |----||-| 	 			e3,z .|---e1,x
# 			 |    || | PiHat
# 			 |LDO-||-|
# CW M1M2M3---------------P1M2P3 CCW
#    PWM#2 			      PWM#1
#
# 
## 
MOTOR_ORIENTATION = {
	# PWM 		[ x,  y,  z]
	1 		: 	[ 1, -1,  1],
	2 		: 	[-1, -1, -1],
	3 		: 	[-1,  1,  1],
	4 		: 	[ 1,  1, -1],
}


##
# Madgwick Complementary Filter Gains
##
MCF_BETA = np.pi/30
MCF_ZETA = 0.

##
# *My* RPi's Bluetooth MAC address
# and a port I chose for communication.
## 
RPI_MAC_ADDRESS = 'B8:27:EB:B7:10:5F'
RPI_PORT = 3

##
# File IO and binary formats for 
# quad-comms/ctrl communication.
## 

QUADCTRL_MAIN = "python3 /home/pi/gravitypi/reva/main-quad-ctrl.py &"
QUADCTRL_TRACE = "python3 /home/pi/gravitypi/reva/main-quad-ctrl.py trace &"

OUTBOX_COMMS = INBOX_CTRL = "/home/pi/gravitypi/logs/quadctrl-in"
INBOX_COMMS = OUTBOX_CTRL = "/home/pi/gravitypi/logs/quadctrl-out"

# shutdown, q_ref = [r, vx, vy, vz], f_thrust
PILOT_REQUESTS 	= struct.Struct("<?fffff")
# shutdown, q_est = [r, vx, vy, vz], w_eff = [wx, wy, wz], dc = [pwm_1, pwm_2, pwm_3, pwm_4], v_bat
QUAD_STATE 		= struct.Struct("<?ffffffffffff")

def pack_pilot(shutdown, q, throttle):
	return PILOT_REQUESTS.pack(
		shutdown, 
		q[0], q[1][0], q[1][1], q[1][2], 
		throttle)

def unpack_pilot(inbox_bytes):	
	result = PILOT_REQUESTS.unpack(inbox_bytes)
	shutdown = result[0]
	q_ref = [result[1], np.array([result[2], result[3], result[4]])]
	throttle = result[5]
	return (shutdown, q_ref, throttle)

def pack_quad(shutdown, q_est, w_eff, motor_duty_cycles, v_battery):
	return QUAD_STATE.pack(
		shutdown, 
		q_est[0], q_est[1][0], q_est[1][1], q_est[1][2], 
		w_eff[0], w_eff[1], w_eff[2],
		motor_duty_cycles[0], motor_duty_cycles[1], motor_duty_cycles[2], motor_duty_cycles[3],
		v_battery)

def unpack_quad(inbox_bytes):	
	result = QUAD_STATE.unpack(inbox_bytes)
	shutdown = result[0]
	q_est = [result[1], np.array([result[2], result[3], result[4]])]
	w_eff = np.array([result[5], result[6], result[7]])
	motor_duty_cycles = [result[8], result[9], result[10], result[11]]
	v_battery = result[12]
	return (shutdown, q_est, w_eff, motor_duty_cycles, v_battery)

## 
# File IO and binary formats for 
# pilot-comms/dashboard/gamepad communication.
## 

DASHBOARD_MAIN = "/home/ssundaresh/miniconda3/envs/laptop/bin/python /home/ssundaresh/Documents/gravitypi/reva/main-pilot-dashboard.py &"
INBOX_DASHBOARD = "/home/ssundaresh/Documents/gravitypi/logs/dashboard-in"

# pilot_heartbeat (i) 
# w_eff (fff)
# v_battery (f)
# q_ref (ffff)
# q_est (ffff)
# motor_duty_cycles (ffff)
# have_gamepad (?)
# sent_comms_req (?)
# got_comms_ack (?)
# is_comms_dead (?)
# sent_shutdown_req (?)
# got_shutdown_ack (?)
# shutdown_req_timed_out (?)
DASHBOARD_STATE = struct.Struct("<" + "i" + "fff" + "f" + "ffff" + "ffff" + "ffff" + "?" * 7)

def pack_dashboard(pilot_heartbeat, w_eff, v_battery, q_ref, q_est, motor_duty_cycles, have_gamepad, sent_comms_req, got_comms_ack, is_comms_dead, sent_shutdown_req, got_shutdown_ack, shutdown_req_timed_out):
	return DASHBOARD_STATE.pack(
		pilot_heartbeat, w_eff[0], w_eff[1], w_eff[2], v_battery,
		q_ref[0], q_ref[1][0], q_ref[1][1], q_ref[1][2], 
		q_est[0], q_est[1][0], q_est[1][1], q_est[1][2], 
		motor_duty_cycles[0], motor_duty_cycles[1], motor_duty_cycles[2], motor_duty_cycles[3],
		have_gamepad,
		sent_comms_req, got_comms_ack, is_comms_dead,
		sent_shutdown_req, got_shutdown_ack, shutdown_req_timed_out)

def unpack_dashboard(inbox_bytes):
	result = DASHBOARD_STATE.unpack(inbox_bytes)
	pilot_heartbeat = result[0]
	w_eff = np.array([result[1], result[2], result[3]])
	v_battery = result[4]
	q_ref = [result[5], np.array([result[6], result[7], result[8]])]
	q_est = [result[9], np.array([result[10], result[11], result[12]])]
	motor_duty_cycles = [result[13], result[14], result[15], result[16]]
	have_gamepad = result[17]
	sent_comms_req = result[18]
	got_comms_ack = result[19]
	is_comms_dead = result[20]
	sent_shutdown_req = result[21]
	got_shutdown_ack = result[22]
	shutdown_req_timed_out = result[23]
	return (pilot_heartbeat, w_eff, v_battery, q_ref, q_est, motor_duty_cycles, have_gamepad, sent_comms_req, got_comms_ack, is_comms_dead, sent_shutdown_req, got_shutdown_ack, shutdown_req_timed_out)

GAMEPAD_SCRIPT = "main-pilot-gamepad.py"
GAMEPAD_MAIN = "/home/ssundaresh/miniconda3/envs/laptop/bin/python /home/ssundaresh/Documents/gravitypi/reva/%s &" % GAMEPAD_SCRIPT
OUTBOX_GAMEPAD = "/home/ssundaresh/Documents/gravitypi/logs/gamepad-out"

# shutdown, f_thrust, theta_roll, theta_pitch, w_yaw
GAMEPAD_STATE 	= struct.Struct("<?ffff")

def pack_gamepad(shutdown, throttle, theta_roll, theta_pitch, w_yaw):
	return GAMEPAD_STATE.pack(shutdown, throttle, theta_roll, theta_pitch, w_yaw)		

def unpack_gamepad(inbox_bytes):	
	result = GAMEPAD_STATE.unpack(inbox_bytes)
	shutdown = result[0]
	throttle = result[1]
	theta_roll = result[2]
	theta_pitch = result[3]
	w_yaw = result[4]
	return (shutdown, throttle, theta_roll, theta_pitch, w_yaw)
