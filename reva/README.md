RPi Setup Requirements

sudo apt-get install bluetooth libbluetooth-dev
sudo usermod -aG bluetooth pi
sudo reboot
hciconfig
	replace the address in reva/board.py with the address shown for your Pi
pair your Pi and your laptop/VM which will be connected to the gamepad. See Prototype Rev A for instructions.

Setup WiringPi locally. See Gravity Prototype Push on GDrive for instructions.

sudo apt-get install python3-dev python3-pip
python3 -m pip install smbus2 wiringpi pybluez cffi
sudo apt-get install python3-numpy

enable I2C from raspi-config's interface options, then follow Prototype Rev A on GDrive to set the I2C clock speed to 400kHz.

build i2cbus CFFI wrapper locally on the RPi using cd reva; python3 optimize.py

Run calibration on your Pi to generate frames.py transformations. See calibration.py for instructions.

Over Comms:
(laptop) python gravitypi/reva/main-pilot.py
python3 /home/pi/gravitypi/reva/main-quad-comms.py

Bypassing Comms:
python3 /home/pi/gravitypi/reva/main-quad-ctrl.py [log level]


Dashboard/Simulation Laptop Requirements
- VirtualBox. Ubuntu 18.04 LTS. Shared clipboard.
- sudo apt-get update, upgrade
- Guest additions, reboot for shared clipboard.
- sudo apt-get install git, gcc, bluetooth libbluetooth-dev
- ssh-keygen setup, add to Bitbucket
- git clone git@bitbucket.org:sssundar/gravitypi.git
- install miniconda3
- https://www.sublimetext.com/docs/3/linux_repositories.html
- sudo usermod -aG bluetooth ssundaresh
- sudo reboot
- conda env update -f laptop.yml
- conda activate laptop
- cd reva
- python optimize.py local