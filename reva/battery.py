from time import time, sleep
from board import LOWEST_RECOMMENDED_BATTERY_VOLTAGE
from _optimizations import ffi as ffi
from _optimizations import lib as lib
import numpy as np

def get_volts_per_lsb():
	return 1./23.739

def get_voltage(raw_bytes, v_per_lsb):
	msb = raw_bytes[0]
	lsb = raw_bytes[1]
	v = (((msb & 0x0F) << 4) | ((lsb & 0xF0) >> 4)) * v_per_lsb
	return v

# ADC081C027: Battery Voltage. 400kHz I2C.
class ADC():
	def __init__(self, logger, pilot):
		self.name = "battery"
		self.logger = logger
		self.pilot = pilot

		# i2c constants
		self.address = 0x51 # i2c address, with ADR0 grounded
		self.sample_bytes = 2 # v(msb)v(lsb)
		self.dst = np.zeros(self.sample_bytes, dtype='uint8')
		self.dst_ptr = ffi.cast("uint8_t *", self.dst.ctypes.data)

		# Scaling
		self.volts_per_lsb = get_volts_per_lsb()
	
		# We do not need to track the timestamp. Log timestamps suffice.
		self.battery_voltage_v = None
		
		# For Tattu
		self.warning_voltage_v = LOWEST_RECOMMENDED_BATTERY_VOLTAGE + 0.1
		self.alert = False

		# When the battery voltage is safe, we show a dim LED continuously.
		self.safe_pwm_duty_cycle = 0.1

		# When the battery voltage is unsafe, we flash a bright LED at the 1/decimator the PWM update frequency (20Hz).
		self.unsafe_pwm_duty_cycle = 0.7		
		self.poll_cnt = 0
		self.decimator = 5
		self.is_shining = False

		# configure the adc
		self.configure()
		return

	def read_voltage(self):		
		# the device's internal register address pointer starts out at 0x00 (conversion register) so here, 
		# since we never modify the device configuration from its power-on state,
		# we can just read 2 bytes directly.		
		lib.i2cbus_implicit_read(self.address, self.dst_ptr, self.sample_bytes);
		v = get_voltage(self.dst, self.volts_per_lsb)
		return v

	def configure(self):
		# in the 'normal' power on-state (which we never leave) the device only triggers
		# an ADC read after an I2C read of its conversion register. this read primes us
		# so the next one is meaningful.
		self.read_voltage()

	def deinit(self):
		return		

	# This function will be called at ~1Hz, polled.
	def handle_task(self, flagged_at):	
		self.battery_voltage_v = self.read_voltage()	
		self.alert = self.battery_voltage_v <= self.warning_voltage_v
		self.pilot.update_fuel_gauge(self.battery_voltage_v)
		self.logger.trace(self, (flagged_at, 1, self.dst))
		return

	def get_warning_led_pwm_duty_cycle(self):
		if self.alert:
			self.poll_cnt += 1
			if self.poll_cnt % self.decimator == 0:
				self.is_shining = not self.is_shining
				self.poll_cnt = 0							
			if self.is_shining:
				return self.unsafe_pwm_duty_cycle
			else:				
				return 0.				
		else:
			self.poll_cnt = 0
			self.is_shining = False
			return self.safe_pwm_duty_cycle
