#pragma once

#include <stdbool.h>
#include <stdint.h>
#include "vector_queue.h"

/* An IMU vector queue with overwrite protection, for time-synchronized vectors */

enum sensor_e {
	ACCEL = 0,
	GYRO,
	COMPASS,
	
	/* Do not modify past this point manually */
	NUM_SENSORS,
};

struct imu_s {
	struct vector_s synchronized_sample[NUM_SENSORS];	
};

struct imu_queue_s {	
	struct imu_s circular_buffer[N_SAMPLES];
	int length;
	int wr_idx;
	int rd_idx;		
};

bool imuq_can_dequeue(struct imu_queue_s *q);
void imuq_dequeue(struct imu_queue_s *q, struct imu_s *dst);

/* A debugging queue for all kinds of vectors, without any overwrite protection. */

struct debugging_queue_s {		
	struct vector_s circular_buffer[N_SAMPLES];
	int datatype[N_SAMPLES];
	int length;
	int wr_idx;
	int rd_idx;			
};

bool dbq_can_dequeue(struct debugging_queue_s *q);
void dbq_dequeue(struct debugging_queue_s *q, struct vector_s *dst, enum sensor_e *datatype);

/* 	The sync runs off 20Hz system ticks.
	20Hz system ticks are the last thing we initialize. 
	All sensors are running before they start.
	For sensors sampling between 30-100Hz in batches of 10-30Hz, 
	We can safely say that for any 2 system ticks we'll be sure
	of a batch from every single sensor.

	This means if we intend to delay the sync by, say, 3 systicks, 
	we just need to skip the first 6 ticks or so, to be sure
	of finding samples both earlier and later than the delayed time
	window we're looking for. This is exactly what we need
	for a linearly interpolating synchronizer. */
struct sync_cfg_s {
	float delay_s;
	float systick_s;
	int n_ticks_to_skip;
	float sampling_period_s;
};

struct sensor_window_s {
	struct vector_s lower;
	struct vector_s upper;
};

struct sync_s {
	struct sync_cfg_s configuration;
	int n_skipped_ticks;

	bool has_started;
	float t_sync_s;

	struct debugging_queue_s inputq;
	struct imu_queue_s outputq;

	struct vector_queue_s *sensor_qs[NUM_SENSORS];
	
	bool initialized_sensor_windows;
	struct sensor_window_s windows[NUM_SENSORS];
};

void sync_init(	struct sync_s *s,
				struct sync_cfg_s *configuration,
				struct vector_queue_s *accelq, 
				struct vector_queue_s *gyroq, 
				struct vector_queue_s *compassq);
struct debugging_queue_s *sync_get_inq(struct sync_s *s);
struct imu_queue_s *sync_get_outq(struct sync_s *s);
void sync_process_systick(	struct sync_s *s, 
							float systick_s);
