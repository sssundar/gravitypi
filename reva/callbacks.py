from scheduler import drone_ctx

def accel_callback():
	drone_ctx.schedule_task("get_acceleration")

def gyro_callback():
	drone_ctx.schedule_task("get_rotation")

def compass_callback():
	drone_ctx.schedule_task("get_heading")

def systick_20Hz_callback():
	drone_ctx.schedule_task("run_sync")

def systick_10Hz_callback():
	drone_ctx.schedule_task("get_pilot")

def systick_1Hz_callback():
	drone_ctx.schedule_task("get_battery")

def null_callback():
	pass