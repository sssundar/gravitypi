from time import time, sleep
from board import MOTOR_ORIENTATION, CAPTURING_DYNAMICS, PULSE_MOTOR_DUTY_CYCLE, PULSE_MOTOR_DURATION_S
import numpy as np
from quaternions import *

# Developed in Github/sssundar/Drone/simulator/kiss_pid.py originally. 
# See notes from 12/11-12/2019.
class PIDController():
	def __init__(self, logger, pilot, pwm):
		self.name = "controller"
		self.logger = logger
		self.pilot = pilot
		self.pwm = pwm

		self.q_ref = [1, np.asarray([0.0,0.0,0.0])]	

		# Configuration		
		self.dt_s = 0.05
		self.beta = 3.87e+02
		self.kp = 70.
		self.kd = 30.
		self.ki = 40.
		self.MAX_THRUST = 4.0
		self.zeta = 0.
		self.clip_int = self.beta/(6*self.ki)
		self.clip_total = self.beta/4

		# Capture Dynamics Free-Tumbling, Motor Pulsing
		self.n_ticks = 0
		self.is_pulsing = False
		return	

	def deinit(self):
		return      

	# Straight from https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
	# Remapping angles to my body frame:
	# +roll = +body y.
	def ToEulerAngles(self, q):
		w, v = q
		x, y, z = v

		sinr_cosp = 2. * (w * x + y * z)
		cosr_cosp = 1. - 2. * (x * x + y * y)
		pitch = np.arctan2(sinr_cosp, cosr_cosp)

		sinp = 2. * (w * y - z * x);
		if (np.abs(sinp) >= 1):
			roll = np.sign(sinp) * (np.pi / 2.) # use 90 degrees if out of range
		else:
			roll = np.arcsin(sinp)

		siny_cosp = 2. * (w * z + x * y)
		cosy_cosp = 1. - 2. * (y * y + z * z)
		yaw = np.arctan2(siny_cosp, cosy_cosp)

		# print(np.array([yaw, pitch, roll])*180./np.pi)
		return yaw, pitch, roll

	# At the moment, this is purely orientation control. We ignore the CM's position.
	def process_state(self, q, w):
		throttle = self.pilot.get_throttle()
		if throttle < 0.001:
			motor_duty_cycles = [0., 0., 0., 0.]
			self.zeta = 0.
			self.pwm.update(motor_duty_cycles)
			self.pilot.update_state(q, w, motor_duty_cycles)
			return

		yaw_ref, pitch_ref, roll_ref = self.ToEulerAngles(self.pilot.get_reference())
		yaw, pitch, roll = self.ToEulerAngles(q)

		# y-jig in body frame is roll axise
		roll_err = roll_ref - roll
		proportional = self.kp*roll_err
		derivative = -self.kd*w[1]
		self.zeta += (roll_err * self.dt_s)
		self.zeta = np.maximum(np.minimum(self.zeta, self.clip_int), -self.clip_int)
		integral = self.ki*self.zeta
		u_dt = proportional + derivative + integral
		u_dt = np.maximum(np.minimum(u_dt, self.clip_total), -self.clip_total)
		alpha = self.MAX_THRUST * throttle
		d_dt = 0.25*np.array([alpha - (u_dt/self.beta), alpha + (u_dt/self.beta)])
		d_dt = np.maximum(np.minimum(d_dt, 1.), 0.)
		motor_duty_cycles = [d_dt[0], d_dt[1], d_dt[1], d_dt[0]]

		# print("%0.2f, %0.2f, %0.2f, %0.2f, %0.2f" % (roll_ref, roll, self.zeta, w[1], u_dt))
		self.pwm.update(motor_duty_cycles)
		self.pilot.update_state(q, w, motor_duty_cycles)	
