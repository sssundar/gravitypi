import sys, evdev
from time import sleep, time
from board import OUTBOX_GAMEPAD, pack_gamepad
from quaternions import *
from helpers import mail, read
import numpy as np

import pprint
pp = pprint.PrettyPrinter(indent=4)
verbose = False

# Wipe any existing outboxes.
outbox = open(OUTBOX_GAMEPAD, "wb")

# Make a cleanup routine. 
def die():
	global outbox
	outbox.close()
	sys.exit(1)

# Make sure a gamepad is connected...
devices = [evdev.InputDevice(path) for path in evdev.list_devices()]
gamepad = None
for device in devices:
	if 'Microsoft X-Box' in device.name:
		gamepad = device.path
		break
if gamepad is None:		
	die()

# Signals To Track
state = {
	"saw_shutdown" : False,
	"throttle" : 0.,
	"theta_roll" : 0.,
	"theta_pitch" : 0.,
	"w_yaw_ccw" : 0.,
	"w_yaw_cw" : 0.,
}

def parse_capabilities(gamepad):
	capabilities = gamepad.capabilities(verbose=True)

	# code
	shutdown = None

	# 		 				 		   			  		 scaling factor 
	# [code, lower_bound, upper_bound, dead zone, unity, from 1.0 to units]
	throttle = None # ABS_RY 	throttle is a unitless value from [0,1].
					# 			to get joystick direction to match to 'throttle up'
					# 			we need to invert the axis

	roll = None   	# ABS_X 	roll and pitch are angles in radians
	pitch = None 	# ABS_Y 	about our body x,y axes, respectively. 
					# 			the signs were chosen so the quad 
					# 			body z vector would be parallel to
					# 			the joystick vector. I think this is
					# 			'southpaw'					

	dyaw_ccw = None	# ABS_Z 	Think of dyaw_ccw - dyaw_cw as the yaw anglular velocity
	dyaw_cw = None 	# ABS_RZ 	in radians about our body z axis that will be integrated
					# 			by the recipient of this state to yield a yaw angle.

	for c in capabilities.keys():
		key, _ = c

		if key == "EV_KEY":
			buttons = capabilities[c]
			for names, code in buttons:
				if "BTN_Y" in names:
					shutdown = code

		elif key == "EV_ABS":
			joysticks = capabilities[c]
			for identifier, info in joysticks:
				name, code = identifier
				if name == "ABS_RY":
					throttle = [code, -info.max, 0, 3, info.max, -0.25]
				elif name == "ABS_X":
					roll = [code, -info.max, info.max, 3, info.max, np.pi/18]
				elif name == "ABS_Y":
					pitch = [code, -info.max, info.max, 3, info.max, np.pi/18]
				elif name == "ABS_Z":
					dyaw_ccw = [code, 0, info.max, info.max/15, info.max, np.pi/5]
				elif name == "ABS_RZ":
					dyaw_cw = [code, 0, info.max, info.max/15, info.max, np.pi/5]
	
	if (shutdown is None) or (throttle is None) or (roll is None) or (pitch is None) or (dyaw_ccw is None) or (dyaw_cw is None):
		die()
	return shutdown, throttle, roll, pitch, dyaw_ccw, dyaw_cw

# Parsing Gamepad Events
gamepad = evdev.InputDevice(gamepad)

shutdown_cfg, throttle_cfg, roll_cfg, pitch_cfg, dyaw_ccw_cfg, dyaw_cw_cfg = parse_capabilities(gamepad)

is_shutdown_event = lambda e: e.code == shutdown_cfg
is_controls_event = lambda e: e.type == evdev.ecodes.EV_ABS
is_throttle_event = lambda e: e.code == throttle_cfg[0]
is_roll_event = lambda e: e.code == roll_cfg[0]
is_pitch_event = lambda e: e.code == pitch_cfg[0]
is_dyaw_ccw_event = lambda e: e.code == dyaw_ccw_cfg[0]
is_dyaw_cw_event = lambda e: e.code == dyaw_cw_cfg[0]

def update_state(state, key, cfg, event):
	_, lb, ub, dz, unity, scale = cfg
	value = event.value
	value = max(lb, value)
	value = min(ub, value)
	if abs(value) < dz:
		value = 0
	value = float(value) / abs(unity)
	value *= scale
	state[key] = value

# We will be killed with a SIGINT by the pilot-comms process.
try:
	for event in gamepad.read_loop():	
		state_changed = True
		if is_controls_event(event):		
			if is_throttle_event(event):
				update_state(state, "throttle", throttle_cfg, event)
			elif is_roll_event(event):
				update_state(state, "theta_roll", roll_cfg, event)
			elif is_pitch_event(event):
				update_state(state, "theta_pitch", pitch_cfg, event)
			elif is_dyaw_ccw_event(event):
				update_state(state, "w_yaw_ccw", dyaw_ccw_cfg, event)
			elif is_dyaw_cw_event(event):
				update_state(state, "w_yaw_cw", dyaw_cw_cfg, event)
		elif is_shutdown_event(event):
			state["saw_shutdown"] = True
		else:
			state_changed = False

		# Python flush just writes to OS buffers so other processes
		# with the same file open see the changes. It's not hitting
		# the disk, so write on any change.
		if state_changed:
			if verbose:
				pp.pprint(state) 

			mail(outbox, 
				pack_gamepad(
					shutdown=state["saw_shutdown"], 
					throttle=state["throttle"], 
					theta_roll=state["theta_roll"], 
					theta_pitch=state["theta_pitch"], 
					w_yaw=state["w_yaw_ccw"] - state["w_yaw_cw"]))
finally:
	die()