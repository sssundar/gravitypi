/* No idea why CFFI adds this. Wasn't seeing how to remove it in docs & didn't feel like blocking on it. */
#ifdef NDEBUG
	#undef NDEBUG
#endif
#include <assert.h>

#include <string.h>

#include "sync.h"

static void imuq_init(struct imu_queue_s *q) {
	memset(q, 0, sizeof(*q));
	q->length = N_SAMPLES;		
}

static bool imuq_can_enqueue(struct imu_queue_s *q) {	
	return (((q->wr_idx + 1) % q->length) != q->rd_idx);
}

static void imuq_enqueue(struct imu_queue_s *q, struct imu_s *src) {
	assert(imuq_can_enqueue(q));
	memcpy(&q->circular_buffer[q->wr_idx], src, sizeof(*src));
	q->wr_idx = (q->wr_idx + 1) % q->length;
}

bool imuq_can_dequeue(struct imu_queue_s *q) {
	return q->wr_idx != q->rd_idx;
}

void imuq_dequeue(struct imu_queue_s *q, struct imu_s *dst) {
	assert(imuq_can_dequeue(q));
	memcpy(dst, &q->circular_buffer[q->rd_idx], sizeof(*dst));
	q->rd_idx = (q->rd_idx + 1) % q->length;
}

static void dbq_init(struct debugging_queue_s *q) {
	memset(q, 0, sizeof(*q));
	q->length = N_SAMPLES;		
}

static void dbq_enqueue(struct debugging_queue_s *q, struct vector_s *src, enum sensor_e datatype) {
	memcpy(&q->circular_buffer[q->wr_idx], src, sizeof(*src));
	q->datatype[q->wr_idx] = datatype;
	q->wr_idx = (q->wr_idx + 1) % q->length;	
}

bool dbq_can_dequeue(struct debugging_queue_s *q) {
	return q->wr_idx != q->rd_idx;
}

void dbq_dequeue(struct debugging_queue_s *q, struct vector_s *dst, enum sensor_e *datatype) {
	assert(dbq_can_dequeue(q));
	memcpy(dst, &q->circular_buffer[q->rd_idx], sizeof(*dst));
	*datatype = q->datatype[q->rd_idx];
	q->rd_idx = (q->rd_idx + 1) % q->length;	
}

void sync_init(	struct sync_s *s, 
				struct sync_cfg_s *configuration,
				struct vector_queue_s *accelq, 
				struct vector_queue_s *gyroq, 
				struct vector_queue_s *compassq) {
	memset(s, 0, sizeof(*s));

	memcpy(&s->configuration, configuration, sizeof(s->configuration));
	
	dbq_init(&s->inputq);
	imuq_init(&s->outputq);

	s->sensor_qs[ACCEL] = accelq;
	s->sensor_qs[GYRO] = gyroq;
	s->sensor_qs[COMPASS] = compassq;
}

struct debugging_queue_s *sync_get_inq(struct sync_s *s) {
	return &s->inputq;
}

struct imu_queue_s *sync_get_outq(struct sync_s *s) {
	return &s->outputq;
}

/* Dequeue from a sensor vector queue and log the input sample for later debugging. */
static void vq_dq(struct sync_s *s, enum sensor_e datatype, struct vector_queue_s *q, struct vector_s *dst) {
	vq_dequeue(q, dst);
	dbq_enqueue(&s->inputq, dst, datatype);
}

static void initialize_window(struct sync_s *s, enum sensor_e datatype) {
	struct vector_queue_s *q = s->sensor_qs[datatype];
	struct sensor_window_s *window = &s->windows[datatype];
	struct vector_s sample;
	
	vq_dq(s, datatype, q, &sample);
	memcpy(&window->lower, &sample, sizeof(sample));
	
	vq_dq(s, datatype, q, &sample);
	memcpy(&window->upper, &sample, sizeof(sample));
}

static void update_window(struct sync_s *s, float target_s, enum sensor_e datatype) {
	struct vector_queue_s *q = s->sensor_qs[datatype];
	struct sensor_window_s *window = &s->windows[datatype];
	struct vector_s sample;
	
	assert(window->lower.t_s <= target_s);
	assert(window->lower.t_s < window->upper.t_s);

	while (window->upper.t_s < target_s) {		
		memcpy(&window->lower, &window->upper, sizeof(sample));
		vq_dq(s, datatype, q, &sample);
		memcpy(&window->upper, &sample, sizeof(sample));
	}
}

static void linear_interpolation(struct sync_s *s, float target_s, enum sensor_e datatype, struct imu_s *dst) {	
	struct sensor_window_s *window = &s->windows[datatype];
	struct vector_s *v = &dst->synchronized_sample[datatype];	
	float slope;

	v->t_s = target_s;
	float dt_s = target_s - window->lower.t_s;
	float run = window->upper.t_s - window->lower.t_s;

	slope = (window->upper.x - window->lower.x) / run;
	v->x = slope*dt_s + window->lower.x;

	slope = (window->upper.y - window->lower.y) / run;
	v->y = slope*dt_s + window->lower.y;

	slope = (window->upper.z - window->lower.z) / run;
	v->z = slope*dt_s + window->lower.z;	
}

void sync_process_systick(	struct sync_s *s, 
							float event_s) {	
	if (s->n_skipped_ticks < s->configuration.n_ticks_to_skip) {
		s->n_skipped_ticks +=1;
		return;
	}

	float t_final_s = event_s - s->configuration.delay_s;

	if (!s->has_started) {
		float one_tick_back_s = t_final_s - s->configuration.systick_s;
		int rounded_ms = one_tick_back_s * 1000;
		s->t_sync_s = ((float) rounded_ms) / 1000;
		s->has_started = true;
	}	

	int datatype;
	struct imu_s synced;
	while ((s->t_sync_s + s->configuration.sampling_period_s) <= t_final_s) {
		/* 	s->t_sync_s is the last synchronized sample timestamp we output
			we need to step forwards by s->configuration.sampling_period_s 
			till we would pass t_final_s */
		
		s->t_sync_s += s->configuration.sampling_period_s;

		/* Make sure each sensor has window[0] <= s->t_sync_s <= window[1] */
		/* Interpolate between each sensor window to get a sample at s->t_sync_s */

		if (!s->initialized_sensor_windows) {
			for (datatype = 0; datatype < NUM_SENSORS; datatype++) {
				initialize_window(s, datatype);
			}
			s->initialized_sensor_windows = true;
		}

		for (datatype = 0; datatype < NUM_SENSORS; datatype++) {
			update_window(s, s->t_sync_s, datatype);
			linear_interpolation(s, s->t_sync_s, datatype, &synced);
		}
		
		/* Update the output queue with the synchronized IMU sample */
		imuq_enqueue(&s->outputq, &synced);
	}
}