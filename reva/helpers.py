import numpy as np

def hexify(byte):
	return format(byte, '#04x')

def rad_to_deg(rad):
	return rad * 180./np.pi

def mail(outbox, binary_data):
	outbox.seek(0, 0)
	outbox.write(binary_data)
	outbox.flush()

def read(inbox, encoding, unpacker=None, default=None):
	inbox.seek(0, 0)
	
	data = inbox.read(encoding.size)
	if (not data) or (len(data) != encoding.size):		
		if default is None:
			return None
		data = default
	
	if unpacker is None:
		return data

	return (data, unpacker(data))
