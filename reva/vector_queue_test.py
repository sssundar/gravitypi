from _optimizations import ffi as ffi
from _optimizations import lib as lib
import numpy as np

# Quick sanity checks of vector queue logic.
# python3 optimize.py
# python3 vector_queue_test.py
# If we did this in a unit test framework we could treat asserts as 'passes' when expected... 

def config_q(cfg, nominal_sampling_period_s):
	cfg.quadx_sensor_idx = 0
	cfg.quady_sensor_idx = 1
	cfg.quadz_sensor_idx = 2
	cfg.x_scale = 1.
	cfg.y_scale = 1.
	cfg.z_scale = 1.
	cfg.x_offset = 0.0
	cfg.y_offset = 0.0
	cfg.z_offset = 0.0
	cfg.nominal_sampling_period_s = nominal_sampling_period_s
	cfg.fifo_wtm_lvl = 0
	return

q_cfg = ffi.new("struct sensor_configuration_s *")
config_q(q_cfg, 0.01)
q = ffi.new("struct vector_queue_s *")
v = ffi.new("struct vector_s *")

def readout(q, v, n):
	for idx in range(n):
		lib.vq_dequeue(q, v)
		print(v.t_s)

# Enqueue a watermarked sample into an empty queue, then read out the queue using dequeue. 
lib.vq_init(q, q_cfg)
v.t_s = 1.0
v.x = v.y = v.z = 0.5
lib.vq_enqueue(q, v)
v.t_s = -1
lib.vq_dequeue(q, v)
assert(v.t_s == 1.0)

# Make sure you cannot dequeue from an empty queue or a queue that has not seen a watermark (asserts)
# lib.vq_dequeue(q, v) 

# Enqueue 4 more samples without watermarks and one sample with a watermark, then read out the queue using dequeue.
lib.vq_init(q, q_cfg)
v.t_s = -1
lib.vq_enqueue(q, v)
lib.vq_enqueue(q, v)
lib.vq_enqueue(q, v)
lib.vq_enqueue(q, v)
v.t_s = 1.0
lib.vq_enqueue(q, v)
readout(q, v, 5)

# Enqueue 5 more samples without watermarks, and one sample with a watermark, then read out the queue using dequeue.
v.t_s = -1
lib.vq_enqueue(q, v)
lib.vq_enqueue(q, v)
lib.vq_enqueue(q, v)
lib.vq_enqueue(q, v)
v.t_s = 2.0
lib.vq_enqueue(q, v)
readout(q, v, 5)

# Keep repeating this till you've filled the queue twice, and make sure sample timestamps look reasonable.
for n in range(50):
	v.t_s = -1
	lib.vq_enqueue(q, v)
	lib.vq_enqueue(q, v)
	lib.vq_enqueue(q, v)
	lib.vq_enqueue(q, v)
	v.t_s = 3.0 + n
	lib.vq_enqueue(q, v)
	readout(q, v, 5)

# Make sure you cannot enqueue to a full queue (asserts)
# for n in range(129):
# 	v.t_s = -1
# 	lib.vq_enqueue(q, v)
