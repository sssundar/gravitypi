import scheduler
import sys

if __name__ == "__main__":
	scheduler.init()

	if (len(sys.argv) >= 2) and (sys.argv[1] in ["trace", "info", "warning", "error"]):
		log_level = sys.argv[1]
	else:
		log_level = "error"		
	print("log verbosity %s" % log_level)

	scheduler.drone_ctx.setup(log_level=log_level)
	scheduler.drone_ctx.spin()