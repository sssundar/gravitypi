from time import time, sleep
import datetime, threading

# A function creating drift-free 20Hz ticks, to be run in a Python thread configured as a daemon.
# That is, this function runs in a different Python thread than the scheduler, but in the same process.
# The daemon is cleaned up automatically when the scheduler process exits.
# https://stackoverflow.com/a/18180189/1623303
def systick(): 
	from callbacks import systick_20Hz_callback, systick_10Hz_callback, systick_1Hz_callback # to avoid circular dependency on scheduler.py in import
	t_20Hz_s = 0.05
	n_ticks = 0

	next_call = time()
	while True:			
		next_call = next_call + t_20Hz_s;
		sleep(next_call - time())
		
		n_ticks += 1
		
		systick_20Hz_callback()

		if (n_ticks & 0x01) == 0:
			systick_10Hz_callback()
		
		if n_ticks == 20:
			systick_1Hz_callback()
			n_ticks = 0

def generate_periodic_events():
	ticker = threading.Thread(target=systick)
	ticker.daemon = True
	ticker.start()