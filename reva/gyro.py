from board import GYRO_INTERRUPT_PIN, CAPTURING_DYNAMICS
from helpers import hexify
from time import time, sleep
from _optimizations import ffi as ffi
from _optimizations import lib as lib
import numpy as np

def configure_gyro_vq(queue_cfg, nominal_sampling_period_s, fifo_wtm_lvl):
									# Rotation to common sensor frame
										# No rotation required because Adafruit 1604 IMU 
										# already aligns sensors to a common sensor frame.									
	queue_cfg.quadx_sensor_idx = 0 	# X to X
	queue_cfg.quady_sensor_idx = 1 	# Y to Y
	queue_cfg.quadz_sensor_idx = 2 	# Z to Z	 												
	if not CAPTURING_DYNAMICS:
		xyz_scale = 1./ (0.00875	* (np.pi/180.)) 	# 8.75 x 10^-3 dps/lsb at +/-245 dps full scale.														
														# Converted to rad/s per lsb then inverted to give
														# lsb per rad/s. Have not calibrated this. 
														# Error here will show up as drift.
	else:
		xyz_scale = 1./ (0.07	* (np.pi/180.)) 		# We need to increase full-scale to 2000 dps
														# 70 x 10^-3 dps/lsb at +/-2000 dps full scale.
														# to avoid saturating. When we capture dynamics we tumble rapidly.
	queue_cfg.x_scale = xyz_scale				
	queue_cfg.y_scale = xyz_scale	
	queue_cfg.z_scale = xyz_scale	
	queue_cfg.x_offset = 0.
	queue_cfg.y_offset = 0.
	queue_cfg.z_offset = 0.
	queue_cfg.nominal_sampling_period_s = nominal_sampling_period_s
	queue_cfg.fifo_wtm_lvl = fifo_wtm_lvl
	return

# L3GD20H Gyrometer. 1MHz I2C max.
class Gyro():
	def __init__(self, logger):
		self.name = "gyro"
		self.logger = logger

		# i2c constants
		self.address = 0x6B
		self.whoami_reg = 0x0F
		self.register_address_autoincrement = 0x80
		scale = 0x80 if (not CAPTURING_DYNAMICS) else 0xB0

		# device configuration
		self.configuration = [
			# 	Reg  	W(#)	Delay 	Verify 	Description
			# 			R(None)			
			[	0x39, 	0x04, 	0.02,	False, 	"LOW_ODR: Software reset the gyrometer, powered down."],
			[ 	0x24, 	0x80,   0,		False, 	"CTRL5: Request flash reboot."],
			[ 	0x20, 	0x18,	0.02, 	False, 	"CTRL1: 100Hz XYZ with 25Hz LPF cutoff, XYZ disabled, sleep mode. Execute flash refresh."],
			[ 	0x23,   scale,	0, 		True, 	"CTRL4: Block update in little endian (LSB first), 245 (0x80) or 500 (0x90) or 2000 (0xB0) dps resolution."],
			[	0x22, 	0x04,	0, 		True, 	"CTRL3: Push-pull active high FIFO watermark interrupt on GRDY"],			
			[	0x2E, 	0x00, 	0, 		False, 	"FIFO_CTRL: Bypass mode to clear the FIFO of any possible error state."],
			[	0x2E, 	0x48, 	0, 		True, 	"FIFO_CTRL: FIFO streaming. Discards older samples if full. FIFO threshold at 8/32 samples."],
			[   0x24, 	0x40, 	0, 		True, 	"CTRL5: FIFO Enable. Single LPF in signal path."],
			[ 	0x20, 	0x1F,	0, 		True, 	"CTRL1: 100Hz XYZ with 25Hz LPF cutoff, XYZ enabled, power on"],
		]
		self.nominal_sampling_period_s = 0.01
		self.fifo_wtm_lvl = 8

		# fifo error flags and data address
		self.fifo_src_reg_a = 0x2F 
		self.fifo_wtm_flag  = 0x80 
		self.fifo_ovr_flag  = 0x40
		self.fifo_fill_lvl  = 0x1F
		self.fifo_data_addr = 0x28 # one 32-element FIFO per gyrometer axis
		self.max_fifo_samples = 32
		self.sample_bytes   = 6    # x(lsb)x(msb) y(lsb)y(msb) z(lsb)z(msb)

		# preallocated i2c buffers
		self.src = np.zeros(self.max_fifo_samples * self.sample_bytes, dtype='uint8')
		self.src_ptr = ffi.cast("uint8_t *", self.src.ctypes.data)
		self.dst = self.src
		self.dst_ptr = self.src_ptr

		# sample batches, in radians per second, with second timestamps extrapolated between watermarks
		# for downstream synchronization with other sensors
		self.vector_queue = ffi.new("struct vector_queue_s *")
		self.queue_cfg = ffi.new("struct sensor_configuration_s *")	
		configure_gyro_vq(self.queue_cfg, self.nominal_sampling_period_s, self.fifo_wtm_lvl)
		lib.vq_init(self.vector_queue, self.queue_cfg)		

		# configure the Pi to receive interrupts		
		import wiringpi # to allow access to configure_*_vq for offline tests on devices (like a laptop) without wiringpi
		wiringpi.pinMode(GYRO_INTERRUPT_PIN, wiringpi.GPIO.INPUT)
		wiringpi.pullUpDnControl(GYRO_INTERRUPT_PIN, wiringpi.GPIO.PUD_DOWN)
		from callbacks import gyro_callback # to avoid circular dependency on scheduler.py in import
		wiringpi.wiringPiISR(GYRO_INTERRUPT_PIN, wiringpi.GPIO.INT_EDGE_RISING, gyro_callback)
		
		# configure the gyrometer
		self.configure()
		return

	def configure(self):
		for addr, value, delay, _, _ in self.configuration:
			self.src[0] = value
			lib.i2cbus_explicit_write(self.address, addr, self.src_ptr, 1)			
			if delay > 0:
				sleep(delay)

		passed = True
		for addr, expected, _, check, _ in self.configuration:
			if check:
				lib.i2cbus_explicit_read(self.address, addr, self.dst_ptr, 1)
				byte = self.dst[0]				
				passed = passed and (byte == expected)		

		if not passed:
			self.logger.error(self, "misconfigured")

	def deinit(self):
		import wiringpi # to allow access to configure_*_vq for offline tests on devices (like a laptop) without wiringpi
		from callbacks import null_callback # to avoid circular dependency on scheduler.py in import
		wiringpi.wiringPiISR(GYRO_INTERRUPT_PIN, wiringpi.GPIO.INT_EDGE_RISING, null_callback)
		return

	# empty the gyrometer FIFO
	# we will only call this function on a watermark interrupt
	def handle_task(self, flagged_at):
		# grab the status of the fifo
		lib.i2cbus_explicit_read(self.address, self.fifo_src_reg_a, self.dst_ptr, 1)
		status = self.dst[0]			
		ready = (status & self.fifo_wtm_flag) != 0
		overrun = (status & self.fifo_ovr_flag) != 0
		n_samples = status & self.fifo_fill_lvl

		if overrun:
			self.logger.error(self, "overrun")

		if not ready:
			return			

		if n_samples < (self.fifo_wtm_lvl+1):
			self.logger.error(self, "watermark timing assumption broken")

		# Burst read
		lib.i2cbus_explicit_read(self.address, self.register_address_autoincrement | self.fifo_data_addr, self.dst_ptr, self.sample_bytes * n_samples);
		lib.vq_unpack_le_words(self.vector_queue, flagged_at, self.dst_ptr, n_samples)
		self.logger.trace(self, (flagged_at, n_samples, self.dst))
		return
		