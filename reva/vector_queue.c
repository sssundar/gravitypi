/* No idea why CFFI adds this. Wasn't seeing how to remove it in docs & didn't feel like blocking on it. */
#ifdef NDEBUG
	#undef NDEBUG
#endif
#include <assert.h>

#include <string.h>

#include "vector_queue.h"

static bool vq_can_enqueue(struct vector_queue_s *q) {	
	return (((q->wr_idx + 1) % q->length) != q->rd_idx) && ((!q->have_seen_wtm) || (q->wr_idx != q->wtm_idx));
}

static bool vq_can_dequeue(struct vector_queue_s *q) {
	return q->wr_idx != q->rd_idx;
}

void vq_init(struct vector_queue_s *q, struct sensor_configuration_s *cfg) {
	memset(q, 0, sizeof(*q));
	memcpy(&q->cfg, cfg, sizeof(*cfg));
	q->length = N_SAMPLES;		
}

void vq_enqueue(struct vector_queue_s *q, struct vector_s *src) {
	assert(vq_can_enqueue(q));

	/* Push in the sample */
	memcpy(&q->circular_buffer[q->wr_idx], src, sizeof(*src));

	int sample_idx = q->wr_idx;
	q->wr_idx = (q->wr_idx + 1) % q->length;

	bool is_sample_watermarked = q->circular_buffer[sample_idx].t_s >= 0;	
	if (!is_sample_watermarked) {
		return;
	}
	
	/* 	Cases of Interest
		1. First watermark is observed at sample_idx >= 0
		2. Consecutive watermarks are observed

		Assumptions
		1. Watermarks are never overwritten before they are used for extrapolation (burst sizes are small relative to the buffer size) */	
	float sampling_period_s = q->cfg.nominal_sampling_period_s;
	int n_samples = (q->wtm_idx <= sample_idx) ? (sample_idx - q->wtm_idx) : (N_SAMPLES + sample_idx - q->wtm_idx);	
	if (q->have_seen_wtm) {	
		float dt_s = q->circular_buffer[sample_idx].t_s - q->circular_buffer[q->wtm_idx].t_s;
		assert((dt_s > 0) && (n_samples > 0));
		sampling_period_s = dt_s / n_samples;
	}
	for (int idx = 0; idx < n_samples; idx++) {		
		q->circular_buffer[(q->wtm_idx + idx) % q->length].t_s = q->circular_buffer[sample_idx].t_s - sampling_period_s*(n_samples - idx);
	}
	q->wtm_idx = sample_idx;
	q->have_seen_wtm = true;
}

void vq_dequeue(struct vector_queue_s *q, struct vector_s *dst) {
	assert(vq_can_dequeue(q));
	assert(q->circular_buffer[q->rd_idx].t_s >= 0); 
	/* Sample has an extrapolated or watermarked timestamp */
	memcpy(dst, &q->circular_buffer[q->rd_idx], sizeof(*dst));
	q->rd_idx = (q->rd_idx + 1) % q->length;
}

void vq_unpack_le_words(struct vector_queue_s *q, float wtm_s, uint8_t *burst_xyz_le_interleaved_bytes, int n_samples) {
	int base = 0;
	int16_t xyz_sensor[3];
	struct vector_s v_quad;
	for (int idx = 0; idx < n_samples; idx++) {
		base = idx*6;
		xyz_sensor[0] = (int16_t) (((uint16_t) (burst_xyz_le_interleaved_bytes[base+0])) + ((uint16_t) (burst_xyz_le_interleaved_bytes[base+1] << 8)));
		xyz_sensor[1] = (int16_t) (((uint16_t) (burst_xyz_le_interleaved_bytes[base+2])) + ((uint16_t) (burst_xyz_le_interleaved_bytes[base+3] << 8)));
		xyz_sensor[2] = (int16_t) (((uint16_t) (burst_xyz_le_interleaved_bytes[base+4])) + ((uint16_t) (burst_xyz_le_interleaved_bytes[base+5] << 8)));
	
		/* Rotation to quadcopter frame */
		v_quad.x = (xyz_sensor[q->cfg.quadx_sensor_idx]*1.f/q->cfg.x_scale) - q->cfg.x_offset;
		v_quad.y = (xyz_sensor[q->cfg.quady_sensor_idx]*1.f/q->cfg.y_scale) - q->cfg.y_offset;
		v_quad.z = (xyz_sensor[q->cfg.quadz_sensor_idx]*1.f/q->cfg.z_scale) - q->cfg.z_offset;

		/* 	watermark the fifo_wtm_lvl+1-th sample
			as the wtm interrupt is not raised until we 
			surpass fifo_wtm_lvl samples on our sensors */
		v_quad.t_s = (idx == q->cfg.fifo_wtm_lvl) ? wtm_s : -1.0; 

		vq_enqueue(q, &v_quad);
	}
}

void vq_unpack_be_words(struct vector_queue_s *q, float wtm_s, uint8_t *burst_xyz_be_interleaved_bytes, int n_samples) {
	int base = 0;
	int16_t xyz_sensor[3];	
	struct vector_s v_quad;
	for (int idx = 0; idx < n_samples; idx++) {
		base = idx*6;
		xyz_sensor[0] = (int16_t) (((uint16_t) (burst_xyz_be_interleaved_bytes[base+1])) + ((uint16_t) (burst_xyz_be_interleaved_bytes[base+0] << 8)));
		xyz_sensor[1] = (int16_t) (((uint16_t) (burst_xyz_be_interleaved_bytes[base+3])) + ((uint16_t) (burst_xyz_be_interleaved_bytes[base+2] << 8)));
		xyz_sensor[2] = (int16_t) (((uint16_t) (burst_xyz_be_interleaved_bytes[base+5])) + ((uint16_t) (burst_xyz_be_interleaved_bytes[base+4] << 8)));
	
		/* Rotation to quadcopter frame */
		v_quad.x = (xyz_sensor[q->cfg.quadx_sensor_idx]*1.f/q->cfg.x_scale) - q->cfg.x_offset;
		v_quad.y = (xyz_sensor[q->cfg.quady_sensor_idx]*1.f/q->cfg.y_scale) - q->cfg.y_offset;
		v_quad.z = (xyz_sensor[q->cfg.quadz_sensor_idx]*1.f/q->cfg.z_scale) - q->cfg.z_offset;

		/* 	watermark the fifo_wtm_lvl+1-th sample
			as the wtm interrupt is not raised until we 
			surpass fifo_wtm_lvl samples on our sensors */
		v_quad.t_s = (idx == q->cfg.fifo_wtm_lvl) ? wtm_s : -1.0; 

		vq_enqueue(q, &v_quad);
	}
}
