# References
# [1] http://blog.kevindoran.co/bluetooth-programming-with-python-3/
# [2] https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles

from board import *
from helpers import *
import os, sys, struct, bluetooth
from time import sleep, time
from quaternions import *
import numpy as np

# Defaults
default_dashboard = pack_quad(shutdown=False, q_est=[1., np.array([0., 0., 0.])], w_eff=[0., 0., 0.], motor_duty_cycles=[0., 0., 0., 0.], v_battery=0.)
default_gamepad = pack_gamepad(shutdown=False, throttle=0., theta_roll=0., theta_pitch=0., w_yaw=0.)

# Clear my inbox, also making sure it exists.
from_gamepad = open(OUTBOX_GAMEPAD, "wb")
from_gamepad.close()
from_gamepad = open(OUTBOX_GAMEPAD, "rb")

# Wipe any existing outboxes.
to_dashboard = open(INBOX_DASHBOARD, "wb")

# Start up the gamepad and dashboard
os.system(GAMEPAD_MAIN)
os.system(DASHBOARD_MAIN)

sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
sock.connect((RPI_MAC_ADDRESS, RPI_PORT))

# Indicator Flags
have_gamepad = False
sent_comms_req = True
got_comms_ack = True
is_comms_dead = False
sent_shutdown_req = False
got_shutdown_ack = False
shutdown_req_timed_out = False

# Timeouts for Quad Shutdown Req-Ack
t_shutdown_req_s = None
SHUTDOWN_TIMEOUT_S = 5

# Pilot Heartbeat
heartbeat = 0

# q_ref
yaw = 0.

# Comms System Time
COMMS_TICK_S = 0.1
next_tick = time()

try:
	while True:		
		heartbeat += 1
		next_tick += COMMS_TICK_S

		# Read gamepad state. We do not expect this thread to crash after it successfully initializes.
		# As a simplified check-of-existence, any output from the gamepad changes even once, consider it good forever.
		request, (send_shutdown_req, throttle, roll, pitch, w_yaw) = read(from_gamepad, GAMEPAD_STATE, unpack_gamepad, default_gamepad)
		have_gamepad = have_gamepad or send_shutdown_req or (throttle != 0.) or (roll != 0.) or (pitch != 0.) or (w_yaw != 0.)		

		# Integrate the w_yaw to get all three Euler angles, represent them as quaternions,
		# and take their product in a (yaw, pitch, roll) airplane 3-2-1 sequence to get q_ref from
		# the gamepad. 		
		yaw += w_yaw * COMMS_TICK_S
		q_yaw = [np.cos(yaw/2), np.array([0., 0., np.sin(yaw/2)])]
		q_pitch = [np.cos(pitch/2), np.array([np.sin(pitch/2), 0., 0.])]
		q_roll = [np.cos(roll/2), np.array([0., np.sin(roll/2), 0.])]
		q_ref = quaternion_product(p=quaternion_product(p=q_yaw, q=q_roll, normalize=True), q=q_pitch, normalize=True)

		# Debounce pilot shutdown request
		if send_shutdown_req and (not sent_shutdown_req):
			t_shutdown_req_s = time()			
		sent_shutdown_req = sent_shutdown_req or send_shutdown_req
		request = pack_pilot(shutdown=sent_shutdown_req, q=q_ref, throttle=throttle)				
		
		# Send pilot request to quad, and receive quad state in return.
		sock.send(request)
		response = sock.recv(QUAD_STATE.size)
		shutdown_ack, q_est, w_eff, motor_duty_cycles, v_battery = unpack_quad(response)

		# Update dashboard. 
		got_shutdown_ack = got_shutdown_ack or shutdown_ack
		shutdown_req_timed_out = sent_shutdown_req and (not got_shutdown_ack) and ((time() - t_shutdown_req_s) > SHUTDOWN_TIMEOUT_S)
		is_comms_dead = got_shutdown_ack or shutdown_req_timed_out

		mail(to_dashboard, 
			pack_dashboard(
				pilot_heartbeat=heartbeat,
				w_eff=w_eff, 
				v_battery=v_battery, 
				q_ref=q_ref, 
				q_est=q_est, 
				motor_duty_cycles=motor_duty_cycles, 
				have_gamepad=have_gamepad,
				sent_comms_req=sent_comms_req,
				got_comms_ack=got_comms_ack,
				is_comms_dead=is_comms_dead,
				sent_shutdown_req=sent_shutdown_req,
				got_shutdown_ack=got_shutdown_ack,
				shutdown_req_timed_out=shutdown_req_timed_out))

		if is_comms_dead:
			break
		
		dt_s = next_tick-time()
		got_comms_ack = dt_s > 0
		while (dt_s <= 0):
			# We're going too slowly. Comms indicator flag lets the user know. 			
			next_tick += COMMS_TICK_S
			dt_s = next_tick-time()
		sleep(dt_s)
finally:
	# Quad-comms may already be dead, if we are responding to got_shutdown_ack.
	# If it isn't, closing the socket will definitely kill it.
	sock.close()

	# The dashboard depends on our heartbeat. We stop sending, it dies.
	to_dashboard.close()			
	from_gamepad.close()

	# The gamepad is in an event loop, so if the user let go of the joysticks,
	# it sees no new events, and will not bother reading our heartbeat. 
	# Kill it directly.
	os.system("pkill -f %s" % GAMEPAD_SCRIPT)
	
	