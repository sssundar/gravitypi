from board import DEFAULT_CALIBRATION_TRACES, CAPTURING_DYNAMICS
if CAPTURING_DYNAMICS:
	print("This script assumes you are giving it traces captured without CAPTURING_DYNAMICS enabled. Please disable it.")
	sys.exit(1)
from quaternions import *
from simulator import Simulator
from state_estimator import gradient_f
from helpers import rad_to_deg
from _optimizations import ffi as ffi
from _optimizations import lib as lib
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import least_squares

# Soft/Hard Iron Calibration References
# [1a] GDrive/Drone/Prototype Rev A/Calibration Math
# [1a] https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html
# [1c] https://www.programcreek.com/python/example/97232/scipy.optimize.least_squares

# IMU Offset Calibration References
# [1a] Unit test in Github/sssundar/Drone/simulator/explore_offline_calibration.py
# [1b] Derivations from notes on 7/6/2019.

class Calibrator():
	# [in] trace_compass_sphere: 	a trace where the quad has been rotated to point 
	# 								at as many points on a sphere as possible, for
	# 								estimating hard- and soft- iron effects on the compass
	# 
	# [in] trace_axes: 				a trace where the quad has been held still
	# 								horizontally for some time, then rotated about 
	# 								its y axis for some time. the first tells us
	# 								the quad z axis relative to the imu, via gravity,
	# 								and the second tells us the quad y axis via 
	# 								the gyro. using these two we can infer
	# 								the imu-to-quad rotation quaternion.
	# 
	# [in] axes_annotations: 		time windows for trace_axes measurements,
	# 								manually annotated
	# 
	# We use measurements from historical logs for all inputs if none are provided. Why?
	#
	# 1) Because of magnetic distortion compensation in the state estimator we don't actually
	# care about the specifics of the magnetic field as long as rotation causes changes
	# in the observed field vector that are greater than the centroid vector. 
	#
	# 2) We may get the q_imu_to_quad slightly wrong if the IMU has shifted slightly since
	# our default calibration was updated... but it's better than having no calibration at all.
	def __init__(self, trace_compass_sphere=None, trace_axes=None, axes_annotations=None):
		self.trace_compass_sphere = trace_compass_sphere if (trace_compass_sphere is not None) else DEFAULT_CALIBRATION_TRACES["compass sphere"]
		self.trace_axes = trace_axes if (trace_axes is not None) else DEFAULT_CALIBRATION_TRACES["axes"]
		self.axes_annotations = axes_annotations if (axes_annotations is not None) else DEFAULT_CALIBRATION_TRACES["axes annotations"]
		return

	def calibrate_compass(self):
		# Parse the raw compass measurements from the trace
		s = Simulator("iosink", self.trace_compass_sphere)
		s.run()
		x = s.iosink.raw[lib.COMPASS]["x"]
		y = s.iosink.raw[lib.COMPASS]["y"]
		z = s.iosink.raw[lib.COMPASS]["z"]
		n = len(x)
		v = np.zeros((3, n), dtype='float')
		for idx in range(n):
			v[0, idx] = x[idx]
			v[1, idx] = y[idx]
			v[2, idx] = z[idx]

		def unpack(x):
			W_inv = np.zeros((3, 3), dtype='float')
			W_inv[0, 0] = x[0]
			W_inv[0, 1] = x[1]
			W_inv[1, 0] = x[1]
			W_inv[0, 2] = x[2]
			W_inv[2, 0] = x[2]
			W_inv[1, 1] = x[3]
			W_inv[1, 2] = x[4]
			W_inv[2, 1] = x[4]
			W_inv[2, 2] = x[5]

			h = np.zeros((3,1), dtype='float')
			h[0, 0] = x[6]
			h[1, 0] = x[7]
			h[2, 0] = x[8]

			return W_inv, h

		# We have the following parameters to estimate:
		#  W^-1 converts a zero-centered soft iron ellipsoid to a unit sphere 
		#  h 	removes the hard iron offset which dominates m
		# to yield the corrected measurement m' of our local magnetic field,
		#  m' = W^-1 (m - h)
		# 
		# Note
		#  W^-1 is constrained to be symmetric (6 degrees of freedom)
		#  h has 3 degrees of freedom		
		def cost_function(x):
			W_inv, h = unpack(x)			
			vp = np.matmul(W_inv, v - h)			
			cost = (1./n)*np.power(np.power(vp, 2).sum(axis=0) - 1, 2).sum()			
			return cost

		# Our initial guess for h is the mean of all the vectors in the sphere
		# Our initial guess for W_inv is the identity matrix.
		h0 = (1./n)*v.sum(axis=1)
		x0 = np.array([1., 0., 0., 1., 0., 1., h0[0], h0[1], h0[2]])
		print("\nCalibrating compass...")
		result = least_squares(cost_function, x0, max_nfev=10000)
		W_inv, h = unpack(result.x)
		
		vp = np.matmul(W_inv, v - h)
		
		fig = plt.figure()
		ax = fig.add_subplot(111, projection='3d')
		# Plot the raw sensor readings (orange) against the calibrated sensor readings (blue)
		s.iosink.plot_magnetic_field(u=vp[0, :], v=vp[1, :], w=vp[2, :], ax=ax, static=True)
		s.iosink.plot_magnetic_field(ax=ax, static=True)
		plt.show()

		self.W_inv = W_inv
		self.h = h	
		print("")

	def calibrate_imu_offset(self, verbose=False):
		# Parse the raw accel, gyro measurements from the trace
		# Assume the gyro and accelerometer are already aligned in x,y,z 
		# Assume the IMU as a whole is offset from the quad frame.
		s = Simulator("iosink", self.trace_axes)
		s.run()
		
		# Parse the annotations for this trace.
		experiments = ["gyro zero-level", "y-axis", "z-axis"]
		measurements = {}
		for expt in experiments:
			measurements[expt] = None

		with open(self.axes_annotations, "r") as annots:
			for line in annots:
				if line[0] == '#':
					continue

				goal, v_ground_truth, sensor, t_start_s, t_end_s = [token.strip() for token in line.strip().split(",")]				
				v_ground_truth = np.array([float(x) for x in v_ground_truth.strip()[1:-1].split()])
				if (goal == 'axis') and (sensor == 'accel'):
					measurements["z-axis"] = {
						"truth" : v_ground_truth, 
						"est" : None,
						"type" : lib.ACCEL, 
						"zero-level" : None,
						"magnitude" : False,
						"start" : float(t_start_s),
						"end" : float(t_end_s),
						}
				elif (goal == 'axis') and (sensor == 'gyro'):
					measurements["y-axis"] = {
						"truth" : v_ground_truth, 
						"est" : None,
						"type" : lib.GYRO, 
						"zero-level" : "gyro zero-level",
						"magnitude" : False,
						"start" : float(t_start_s),
						"end" : float(t_end_s),
						}					
				elif (goal == 'zero level') and (sensor == 'gyro'):
					measurements["gyro zero-level"] = {
						"truth" : v_ground_truth, 
						"est" : None,
						"type" : lib.GYRO, 
						"zero-level" : None,
						"magnitude" : True,
						"start" : float(t_start_s),
						"end" : float(t_end_s),
						}						
				else:					
					print("ERROR: Calibration measurement (%s) not yet supported." % line.strip())
					assert(False)

		# Analyze the measurements
		# To identify an axis, take a time-average of normalized vectors which we know point in the ~direction of that axis.
		# To identify an instantaneous zero-level, we take a time-average of raw vectors which we know point in the ~direction of that level.
		for expt in experiments:			
			sensor = measurements[expt]["type"]
			start = measurements[expt]["start"]
			end = measurements[expt]["end"]
			normalize = not measurements[expt]["magnitude"]
			level = measurements[expt]["zero-level"]

			t = np.array(s.iosink.raw[sensor]["t"])
			mask = (t >= start) * (t <= end)
			t = t[mask]
			n = len(t)
			assert(n > 0)
			x = np.array(s.iosink.raw[sensor]["x"])[mask]
			y = np.array(s.iosink.raw[sensor]["y"])[mask]
			z = np.array(s.iosink.raw[sensor]["z"])[mask]
			
			v = np.zeros((3, n), dtype='float')
			for idx in range(n):
				v[0, idx] = x[idx]
				v[1, idx] = y[idx]
				v[2, idx] = z[idx]

			if level is not None:
				v -= measurements[level]["est"]

			if normalize:
				norm = 1. / np.sqrt(np.power(v, 2).sum(axis=0, keepdims=True))
				v = np.multiply(v, norm)
			
			# Take a time average of the measurements
			v = v.sum(axis=1, keepdims=True) / n

			if normalize:
				v /= vector_norm(v)

			measurements[expt]["est"] = v

		# Now, we know the y- and z- axes in the quad frame as seen from the IMU
		# Find the quaternion which rotates the vectors observed in the IMU frame
		# to the ground truth (by construction of the experiment) in the quad frame. 
		# See 
		y_hat_gt = measurements["y-axis"]["truth"].squeeze()
		y_hat_imu = measurements["y-axis"]["est"].squeeze()
		z_hat_gt = measurements["z-axis"]["truth"].squeeze()
		z_hat_imu = measurements["z-axis"]["est"].squeeze()

		def unpack(x):
			q = [x[0], np.array([x[1], x[2], x[3]])]
			return q

		def cost_function(x):
			q = unpack(x)
			y_hat_est = quaternion_rotation(qv=[0, y_hat_imu], qr=q)[1]
			z_hat_est = quaternion_rotation(qv=[0, z_hat_imu], qr=q)[1]
			cost = np.power(z_hat_est - z_hat_gt, 2).sum() + np.power(y_hat_est - y_hat_gt, 2).sum()			
			return cost

		# This is doing quite well. I notice if I have measurement error, say misalignment 
		# in test jig, so y_hat_est and and z_hat_est are not quite y, z by O(10^-2) degrees, then this
		# propagates to an O(10^-2) error in my final angle estimate as well. I can live with
		# this for Rev A.	
		print("\nCalibrating IMU to Quad frame transformation...")
		x0 = np.array([1., 0., 0., 0.]) # Assume perfect alignment of imu, quad frames to start.				
		result = least_squares(cost_function, x0, max_nfev=10000)
		q = unpack(result.x)
		self.q_imu_to_quad = [e / quaternion_norm(q) for e in q]

		# Sanity check!
		y_hat_est = quaternion_rotation(qv=[0, y_hat_imu], qr=self.q_imu_to_quad)[1]
		z_hat_est = quaternion_rotation(qv=[0, z_hat_imu], qr=self.q_imu_to_quad)[1]
		y_hat_est = y_hat_est / vector_norm(y_hat_est)
		z_hat_est = z_hat_est / vector_norm(z_hat_est)
		if verbose:
			print(y_hat_imu, y_hat_gt, y_hat_est)
			print(z_hat_imu, z_hat_gt, z_hat_est)
			print(self.q_imu_to_quad)
			
		# dot(x,y) = |x||y|cos theta
		# |x| = |y| = 1 		
		improvement = {
			"y" : [vector_dot(y_hat_imu, y_hat_gt), vector_dot(y_hat_est, y_hat_gt)],
			"z" : [vector_dot(z_hat_imu, z_hat_gt), vector_dot(z_hat_est, z_hat_gt)]
		}

		for k in improvement.keys():
			init, final = improvement[k]
			assert(abs(init) <= 1.)
			assert(abs(final) <= 1.)
			print("Calibrated initial %0.3f degree offset in %s-axis to %0.3f degrees." % (rad_to_deg(np.arccos(init)), k, rad_to_deg(np.arccos(final))))		
		print("")
		return

	def dump(self):
		lines = []
		
		lines.append("# Auto-generated by reva/calibration.py")
		lines.append("")
		lines.append("import numpy as np")
		lines.append("")
		lines.append("# Transforming a measurement vector v_imu from the IMU frame by")
		lines.append("# q_imu_to_quad * v_imu * q_imu_to_quad^-1 rotates v_imu to v_quad.")
		lines.append("q_imu_to_quad = [%f, np.array([%f, %f, %f])]" % (self.q_imu_to_quad[0], self.q_imu_to_quad[1][0], self.q_imu_to_quad[1][1], self.q_imu_to_quad[1][2]))
		lines.append("")
		lines.append("# Transforming a magnetometer vector m as m' = W^-1 (m-h) yields")
		lines.append("# a heading m' that approximately lies on a zero-centered unit sphere.")
		lines.append("W_inv = np.array([ [%f, %f, %f], [%f, %f, %f], [%f, %f, %f] ], dtype='float')" % tuple(self.W_inv.reshape(-1)))
		lines.append("h = np.array([%f, %f, %f], dtype='float')" % tuple(self.h.reshape(-1)))

		with open("reva/frames.py", "w") as output:
			for line in lines:
				output.write(line + "\n")
		
if __name__ == "__main__":
	c = Calibrator()
	c.calibrate_imu_offset()
	c.calibrate_compass()
	c.dump()