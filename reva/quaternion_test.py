from quaternions import *
from state_estimator import gradient_f
from _optimizations import ffi as ffi
from _optimizations import lib as lib
import numpy as np
from matplotlib import pyplot as plt
import ipdb

# A statistical regression test for quaternion math implemented in C vs Numpy.
	# Benchmark, 7/20/2019. 
		# C vs Python Quaternion Math, n=10000 random ~N(0,1) quaternions
		# product error: max in any element 1.044539%, mean across set 0.000132%
		# inverse error: max in any element 0.000022%, mean across set 0.000005%
		# rotation error: max in any element 0.339454%, mean across set 0.000081%
		# mcf_gradient error: max in any element 1.136074%, mean across set 0.000161%
	# ...
	
# Takes a ffi.new("struct quat_s *") instance and converts it to a Python quaternion.
q_to_py = lambda c: [c[0].r, np.array([c[0].v.x, c[0].v.y, c[0].v.z])]
v_to_py = lambda c: np.array([c[0].x, c[0].y, c[0].z])

# Compute element by element percent error between two quaternions. Return the maximum.
def percent_error(result, expected):
	return 100*abs(result - expected) / expected
def vect_max_elementwise_error(result, expected):
	errors = []
	for idx in range(3):
		errors.append(percent_error(result[idx], expected[idx]))
	return max(errors)
def quat_max_elementwise_error(result, expected):
	errors = []
	errors.append(percent_error(result[0], expected[0]))
	for idx in range(3):
		errors.append(percent_error(result[1][idx], expected[1][idx]))
	return max(errors)

#####################################################
# Testing quat_product against quaternion_product   #
# Testing quat_inverse against quaternion_inverse   #
# Testing quat_rotation against quaternion_rotation #
#####################################################
unit = ffi.new("struct quat_s *")
unit[0].r = 1.
unit[0].v.x = 0.
unit[0].v.y = 0.
unit[0].v.z = 0.

p = ffi.new("struct quat_s *")
q = ffi.new("struct quat_s *")
u = ffi.new("struct quat_s *")
pq = ffi.new("struct quat_s *")

v = ffi.new("struct vect_s *v")
vp = ffi.new("struct vect_s *v")

a = ffi.new("struct vect_s *")
b = ffi.new("struct vect_s *")
m = ffi.new("struct vect_s *")

stats = { 		# Array of dictionaries, keys: args, result, expected, % error
	"product" : [],
	"inverse" : [],
	"rotation" : [],
	"mcf_gradient" : [],
}

n = 10000
for idx in range(n):
	p[0].r = np.random.randn()
	p[0].v.x = np.random.randn()
	p[0].v.y = np.random.randn()
	p[0].v.z = np.random.randn()

	q[0].r = np.random.randn()
	q[0].v.x = np.random.randn()
	q[0].v.y = np.random.randn()
	q[0].v.z = np.random.randn()

	normalize = False
	lib.quat_product(p, q, pq, normalize)
	args = [q_to_py(p), q_to_py(q), normalize]
	result = q_to_py(pq)
	expected = quaternion_product(q_to_py(p), q_to_py(q), normalize)	
	stats["product"].append({"args": args, "result": result, "expected": expected, "error": quat_max_elementwise_error(result, expected)})

	lib.quat_inverse(p, u)
	args = [q_to_py(p)]
	result = q_to_py(u)
	expected = quaternion_inverse(q_to_py(p))
	stats["inverse"].append({"args": args, "result": result, "expected": expected, "error": quat_max_elementwise_error(result, expected)})

	lib.quat_product(unit, q, u, True) # Normalize q to create a pure rotation, u
	v[0].x = p[0].v.x
	v[0].y = p[0].v.y
	v[0].z = p[0].v.z
	lib.quat_rotation(v, q, vp)
	args = [v_to_py(v), q_to_py(u)]
	result = v_to_py(vp)
	expected = quaternion_rotation([0., v_to_py(v)], q_to_py(u))[1]
	stats["rotation"].append({"args": args, "result": result, "expected": expected, "error": vect_max_elementwise_error(result, expected)})

	u[0].r = np.random.randn()
	u[0].v.x = np.random.randn()
	u[0].v.y = np.random.randn()
	u[0].v.z = np.random.randn()
	lib.quat_product(unit, u, q, True) # Normalize u to create a pure rotation, q
	a[0].x = np.random.randn()
	a[0].y = np.random.randn()
	a[0].z = np.random.randn()
	b[0].x = np.random.randn()
	b[0].y = np.random.randn()
	b[0].z = np.random.randn()
	m[0].x = np.random.randn()
	m[0].y = np.random.randn()
	m[0].z = np.random.randn()
	lib.scale_vect(a, lib.l2_vect_norm(a), a);
	lib.scale_vect(b, lib.l2_vect_norm(b), b);
	lib.scale_vect(m, lib.l2_vect_norm(m), m);
	lib.mcf_gradient(q, a, b, m, u)
	args = [q_to_py(q), v_to_py(a), v_to_py(b), v_to_py(m)]
	result = q_to_py(u)
	expected = gradient_f(*args)
	expected = [expected[0], np.array(expected[1:])]	
	stats["mcf_gradient"].append({"args": args, "result": result, "expected": expected, "error": quat_max_elementwise_error(result, expected)})

def get_error(stats, keyword):
	result = []
	for el in stats[keyword]:
		result.append(el["error"])
	return result

print("C vs Python Quaternion Math, n=%d random ~N(0,1) quaternions" % n)
for k in stats.keys():
	e = get_error(stats, k)
	print("%s error: max in any element %f%s, mean across set %f%s" % (k, max(e), "%", np.mean(e), "%"))