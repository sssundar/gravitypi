from _i2cbus import ffi as ffi
from _i2cbus import lib as lib
import numpy as np
from time import time
from mock_gyro import Test

# Usage
# python3 i2cbus_builder.py
# python3 i2c-speedup-cffi.py

bus = lib.i2cbus_open(1)
gyro = Test()

start_s = time()
print(start_s)
for idx in range(100):
	gyro.read_sample()
end_s = time()
print(end_s)
delta_ms = int(1000*(end_s - start_s))

lib.i2cbus_close()
print("Took %d ms to read using cffi" % delta_ms)