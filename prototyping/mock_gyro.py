from _i2cbus import ffi as ffi
from _i2cbus import lib as lib
import numpy as np

class Test():
	def __init__(self):
		self.address = 0x6B
		self.reg = 0x28
		self.nbytes = 6
		self.dst = np.zeros(self.nbytes, dtype='uint8')
		self.ptr = ffi.cast("uint8_t *", self.dst.ctypes.data)

	def read_sample(self):
		lib.i2cbus_explicit_read(self.address, self.reg, self.ptr, self.nbytes)
