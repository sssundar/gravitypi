import numpy as np
import matplotlib.pyplot as plt
import ipdb

def generate_waveform(waveform_hz=1., nominal_sampling_frequency_hz=100., percent_error=0., t_final_s=10., samples_per_batch=10, debug=False):
	dt_s = 1./(nominal_sampling_frequency_hz*(1.+(percent_error*1./100)))
	n_samples = int(t_final_s/dt_s)
	t_raw_s = np.arange(n_samples)*dt_s
	q_raw = np.sin(2*np.pi*waveform_hz*t_raw_s)
	if debug:
		plt.plot(t_raw_s, q_raw)
		plt.show()
	q_batched = []
	for idx in range(0, n_samples, samples_per_batch):
		if (idx + samples_per_batch) < n_samples:
			q_batched.append([t_raw_s[idx+samples_per_batch-1], q_raw[idx:idx+samples_per_batch]])
		else:
			q_batched.append([t_raw_s[-1], q_raw[idx::]])
	return t_raw_s, q_raw, q_batched

def synchronize(q_batched, sampling_frequency_hz=100., tick_frequency_hz=20., t_final_s=10., delay_s=0.15, debug=False):
	t_sync_s = []
	q_sync = []
	
	tick_period_s = 1./tick_frequency_hz		
	dt_s = 1./sampling_frequency_hz

	n_buffer = int(sampling_frequency_hz*delay_s)
	t_raw_est_s = list(np.array(range(-n_buffer, 0))*dt_s)
	last_wtm_s = t_raw_est_s[-1]
	q_raw = [0.]*n_buffer
	assert(len(q_raw)==len(t_raw_est_s))
	bdx = 0
		
	system_time_s = tick_period_s
	target_s = system_time_s - delay_s
	sync_s = -n_buffer*dt_s # start at an integer multiple of your sampling period

	while system_time_s < t_final_s:		
		if system_time_s > t_raw_est_s[-1]:
			wtm_s, batch = q_batched.pop(0)
			n_samples = len(batch)
			dt_est_s = (wtm_s - last_wtm_s) / n_samples		
			last_wtm_s = wtm_s

			t_raw_est_s.extend( list(wtm_s - np.arange(n_samples)*dt_est_s)[::-1] )
			q_raw.extend( batch )
			
		while sync_s <= target_s:
			assert(sync_s >= t_raw_est_s[bdx])
			while sync_s >= t_raw_est_s[bdx+1]:
				bdx += 1

			t_sync_s.append(sync_s)
			m = (q_raw[bdx+1] - q_raw[bdx])/(t_raw_est_s[bdx+1]-t_raw_est_s[bdx])
			q_sync.append( m*(sync_s - t_raw_est_s[bdx]) + q_raw[bdx] )

			sync_s += dt_s		

		system_time_s += tick_period_s
		target_s = system_time_s - delay_s

	return t_sync_s, q_sync

if __name__ == "__main__":
	t_raw_s, q_raw, q_batched = generate_waveform(percent_error=-1.0)
	t_sync_s, q_sync = synchronize(q_batched)

	plt.plot(t_raw_s, q_raw, 'k.-')
	plt.plot(t_sync_s, q_sync, 'r.--')
	plt.show()