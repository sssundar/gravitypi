# Goal: Talk to PWM to make the warning LED flash.

from time import time, sleep
from _i2cbus import ffi as ffi
from _i2cbus import lib as lib
import numpy as np

def hexify(byte):
	return format(byte, '#04x')

# PCA9865: PWM Extender. 1MHz I2C.
class PWMExtender():
	# TODO Fix	
	#def __init__(self, logger, scheduler, controller, battery):
	def __init__(self):
		self.name = "pwm"
		# self.logger = logger
		# self.scheduler = scheduler
		# self.controller = controller
		# self.battery = battery
		lib.i2cbus_open(1)

		# i2c constants				
		self.rs_addr = 0x00 
		self.address = 0x40 # with A5-A0 grounded
		# The register for LED0_ON_L is our base address as
		# we use LED0-4 and these are consecutive addresses.
		self.base_addr = 0x06

		self.max_config_bytes = 20		
		self.src = np.zeros(self.max_config_bytes, dtype='uint8')
		self.src_ptr = ffi.cast("uint8_t *", self.src.ctypes.data)
		self.dst = self.src
		self.dst_ptr = self.src_ptr

		# i2c configuration
		self.configuration = [
			# 	i2c addr 		reg_or_data data 	delay 	verify 	description
			[ 	self.rs_addr, 	0x06, 		None, 	0.01, 	False, 	"Software reset chip to totem pole drivers, all channels off."],
			[	self.address, 	0xFE, 		0x05, 	0, 		True,  	"Prescale for ~1kHz PWM. Writes are blocked after we wake up via Mode 1 settings (sleep bit 0)"],
			[ 	self.address, 	0x00, 		0x20, 	0.01, 	True, 	"Wake up by setting sleep bit to 0 in Mode 1 register. Autoincrement register addresses. No sub- or all- call."],
		]
		
		# remember result of self.battery.get_warning_led_pwm_duty_cycle() to reduce load on i2c bus	
		self.last_warning_led_duty_cycle = None
	
		# configure the pwm
		self.configure()		
		return
	
	def configure(self):
		for i2c_addr, reg_or_data, data, delay, _, _ in self.configuration:
			if data is not None:
				self.src[0] = data
				lib.i2cbus_explicit_write(i2c_addr, reg_or_data, self.src_ptr, 1)
			else:
				self.src[0] = reg_or_data
				lib.i2cbus_implicit_write(i2c_addr, self.src_ptr, 1)

			if delay > 0:
				sleep(delay)

		passed = True
		for _, reg_or_data, expected, _, check, _ in self.configuration:
			if check:
				lib.i2cbus_explicit_read(self.address, reg_or_data, self.dst_ptr, 1)
				byte = self.dst[0]
				passed = passed and (byte == expected)

		if not passed:
			# TODO Fix
			# self.logger.error(self, "misconfigured")
			print("misconfigured")

	# We do not make use of 'full on' or 'immediate off' 
	# (LED_ON|OFF[12]) to keep the logic simple
	def get_duty_cycle_registers(self, duty_cycle):		
		duty_cycle = max(0.0, duty_cycle)
		duty_cycle = min(0.9999, duty_cycle)

		ON_L = 0
		ON_H = 0
		OFF = int(4096.*duty_cycle)
		OFF_L = OFF & 0xFF
		OFF_H = (OFF >> 8) & 0xFF

		return [ON_L, ON_H, OFF_L, OFF_H]

	def write(self, consecutive_registers, n_bytes):
		np.copyto(self.src, consecutive_registers, casting='unsafe')
		lib.i2cbus_explicit_write(self.address, self.base_addr, self.src_ptr, n_bytes)
		return

	def deinit(self):
		# Turn off all 5 PWM channels
		self.write([0]*20, 20)
		lib.i2cbus_close() # TODO Remove or make a secondary I2C master for use by comms.
		return		

	# TODO Fix
	# This function will be called at ~20Hz, polled.
	# def handle_task(self, flagged_at):
	def handle_task(self):
		consecutive_registers = []

		# Ask controller what duty cycles it wants for each motor
		# TODO Fix
		duty_cycles = [0., 0., 0., 0.]		
		for dc in duty_cycles:
			consecutive_registers.extend(self.get_duty_cycle_registers(dc))

		# Ask battery what duty cycle it wants for the warning led
		# TODO Fix
		# warning_led_duty_cycle = self.battery.get_warning_led_pwm_duty_cycle()
		if self.last_warning_led_duty_cycle is None:
			warning_led_duty_cycle = 0.5 
		else:
			if self.last_warning_led_duty_cycle == 0.5:
				warning_led_duty_cycle = 0.0
			else:
				warning_led_duty_cycle = 0.5
		
		warning_duty_cycle_changed = (self.last_warning_led_duty_cycle is None) or (warning_led_duty_cycle != self.last_warning_led_duty_cycle)
		self.last_warning_led_duty_cycle = warning_led_duty_cycle
		consecutive_registers.extend(self.get_duty_cycle_registers(warning_led_duty_cycle))		

		if warning_duty_cycle_changed:	
			self.write(consecutive_registers, 20)
		else:
			self.write(consecutive_registers, 16)

		return

if __name__ == "__main__":
	pwm = PWMExtender()
	try:
		while True:
			sleep(1)
			pwm.handle_task()
	except KeyboardInterrupt:
		pwm.deinit()
