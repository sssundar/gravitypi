# As my quad is currently flipping upside down >< every time I open up the throttle, 
# it's time to try and understand the observed scale of moments of inertia & torque, thrust. 

import sys, os
REVA = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "reva")
sys.path.append(REVA)
from simulator import Simulator
from board import MOTOR_ORIENTATION, CAPTURING_DYNAMICS
if not CAPTURING_DYNAMICS:
	print("This script assumes you are giving it traces captured with CAPTURING_DYNAMICS enabled. Please enable it.")
	sys.exit(1)
from _optimizations import ffi as ffi
from _optimizations import lib as lib
from quaternions import *
from controller import PIDController
from frames import q_imu_to_quad
import numpy as np
from numpy import cos as c
from numpy import sin as s
import matplotlib.pyplot as plt
from scipy import signal
from scipy.integrate import odeint
from scipy.optimize import least_squares
import ipdb
import pprint
pp = pprint.PrettyPrinter(indent=4)

def check_time_series(s, experiment):
	b, a = signal.butter(3, 0.1)
	w, h = signal.freqz(b, a, fs=100)
	# plt.semilogx(w, 20 * np.log10(abs(h)))
	# plt.title('Butterworth filter frequency response')
	# plt.xlabel('Frequency [radians / second]')
	# plt.ylabel('Amplitude [dB]')
	# plt.margins(0, 0.1)
	# plt.grid(which='both', axis='both')
	# plt.axvline(100, color='green') # cutoff frequency
	# plt.show()

	fig, ax = plt.subplots(4, 1, sharex=True)
	x = s.iosink.synced[lib.COMPASS]["x"]
	y = s.iosink.synced[lib.COMPASS]["y"]
	z = s.iosink.synced[lib.COMPASS]["z"]

	ax[0].plot(s.iosink.synced[lib.COMPASS]["t"], x, 'r-')
	ax[0].plot(s.iosink.synced[lib.COMPASS]["t"], y, 'g-')
	ax[0].plot(s.iosink.synced[lib.COMPASS]["t"], z, 'b-')
	ax[0].set_ylabel("Compass [unit]")

	x = s.iosink.synced[lib.GYRO]["x"]
	y = s.iosink.synced[lib.GYRO]["y"]
	z = s.iosink.synced[lib.GYRO]["z"]

	ax[1].plot(s.iosink.synced[lib.GYRO]["t"], x, 'r-')
	ax[1].plot(s.iosink.synced[lib.GYRO]["t"], y, 'g-')
	ax[1].plot(s.iosink.synced[lib.GYRO]["t"], z, 'b-')	
	ax[1].set_ylabel("Gyro [rad/s]")
	ax[1].set_ylim([-20,15])

	x = s.iosink.synced[lib.ACCEL]["x"]
	y = s.iosink.synced[lib.ACCEL]["y"]
	z = s.iosink.synced[lib.ACCEL]["z"]

	ax[2].plot(s.iosink.synced[lib.ACCEL]["t"], x, 'r-')
	ax[2].plot(s.iosink.synced[lib.ACCEL]["t"], y, 'g-')
	ax[2].plot(s.iosink.synced[lib.ACCEL]["t"], z, 'b-')
	ax[2].set_ylabel("Accel [g]")
	ax[2].set_ylim([-2.5,2.5])

	x = signal.lfilter(b, a, x)
	y = signal.lfilter(b, a, y)
	z = signal.lfilter(b, a, z)
	n = len(x)
	v = np.zeros((3, n), dtype='float')
	for idx in range(n):
		v[0, idx] = x[idx]
		v[1, idx] = y[idx]
		v[2, idx] = z[idx]
	m = np.sqrt(np.power(v, 2).sum(axis=0))

	ax[3].plot(s.iosink.synced[lib.ACCEL]["t"], x, 'r-')
	ax[3].plot(s.iosink.synced[lib.ACCEL]["t"], y, 'g-')
	ax[3].plot(s.iosink.synced[lib.ACCEL]["t"], z, 'b-')
	ax[3].plot(s.iosink.synced[lib.ACCEL]["t"], m, 'k-')	
	ax[3].set_ylabel("<Accel [g]>")
	ax[3].set_xlabel("Time (s)")
	ax[3].legend(["x", "y", "z", "<|v|>"])
	ax[3].set_ylim([-2.5,2.5])
	ax[3].set_xlim([45.5,47])

	plt.show()
	return

# See notes from 8/11/2019. We throttled the quad, tethered to the calibration jig.
# This constrained it to ~rotate about only its body y-axis. Euler's equations
# with this constraint, neglecting gravitation torque about the y-axis due to 
# the raised COM, and the assumption that the torque about the y-axis is
# directly proportional to (d2+d3-d1-d4) (e.g. identical motors & moment arms)
# leads to...
#
#  d/dt w_y / (d2 + d3 - d1 - d4) = alpha/J_yy
# 
# where J_yy is the moment of inertia about the y-axis and 
# alpha is the scaling factor from duty cycle to torque.
#
# UPDATE: Estimation results suggest it is not ok to neglect the gravity torque.
def check_constrained_rotation(s, experiment):
	t_pwm = np.array(s.iosink.pwm["t"])
	d1 = np.array(s.iosink.pwm["duty_cycle_1"])
	d2 = np.array(s.iosink.pwm["duty_cycle_2"])
	d3 = np.array(s.iosink.pwm["duty_cycle_3"])
	d4 = np.array(s.iosink.pwm["duty_cycle_4"])

	t_w = np.array(s.iosink.synced[lib.GYRO]["t"])
	w_x = s.iosink.synced[lib.GYRO]["x"]
	w_y = s.iosink.synced[lib.GYRO]["y"]
	w_z = s.iosink.synced[lib.GYRO]["z"]

	# Rotate w from the IMU frame to the quad frame.
	to_quad_frame = lambda v: quaternion_rotation(qv=[0,v], qr=quaternion_inverse(q_imu_to_quad))[1]
	n = len(w_x)
	w = np.zeros((3, n), dtype='float')
	for idx in range(n):
		w_imu = np.array([w_x[idx], w_y[idx], w_z[idx]])
		w_quad = to_quad_frame(w_imu)
		w[:, idx] = w_quad

	# First, estimate zero-level gyro offset as system was stationary for quite some time.
	t0, tf = experiment["gyro offset"]
	stationary_mask = (t_w >= t0) * (t_w <= tf)
	w_stationary = w[:, stationary_mask] 
	w_zero = w_stationary.sum(axis=1, keepdims=True) / n
	w -= w_zero	

	# Next, compute d/dt w_y
	ddt_w_y = (w[1, 1:] - w[1, :-1]) / (t_w[1:] - t_w[:-1])

	# Finally, interpolate d/dt w_y to t_pwm, compute alpha/J_yy
	ddt_w_y_interpolated = np.interp(t_pwm, t_w[1:], ddt_w_y)
	normalized_torque = d2 + d3 - d1 - d4
	torque_mask = normalized_torque != 0
	t_inference = t_pwm[torque_mask]
	alpha_Jyy = ddt_w_y_interpolated[torque_mask] / normalized_torque[torque_mask]

	# Compute ~alpha/J_yy we currently have in our PID controller
	pid = PIDController(None, None, None)
	# pp.pprint(pid.DUTY_CYCLE_TO_NORMALIZED_TORQUE)
	myJyy = pid.J[1,1]
	myalpha = pid.e2_THRUST_MOMENT
	my_alpha_Jyy = myalpha/myJyy * np.ones(len(t_inference))    

	# Next, verify that w_x, w_z are ~zero throughout the test & d/dt w_y is meaningful, and plot the resulting alpha/J_yy.
	fig, ax = plt.subplots(2,1)
	
	ax[0].plot(t_inference, alpha_Jyy, 'k-')
	ax[0].plot(t_inference, my_alpha_Jyy, 'r--')
	ax[0].set_xlim(experiment["rotation"])

	ax[1].plot(t_w, w.T)
	ax[1].plot(t_w[1:], ddt_w_y, 'k-')
	ax[1].plot(t_pwm, 100*d1, 'y-')
	ax[1].plot(t_pwm, 100*d2, 'y-')
	ax[1].plot(t_pwm, 100*d3, 'y-')
	ax[1].plot(t_pwm, 100*d4, 'y-')
	ax[1].set_xlim(experiment["rotation"])

	plt.show()

	return

# We randomly walk around parameters space to estimate our moment of inertia
# tensor and our motor duty-cycle to thrust/torque coefficients. 
def estimate_parameters(sim, experiment):
	experiment_type, pwm_idx = experiment["experiment"]
	if pwm_idx is None:
		pwm_idx = 1
	is_free = experiment_type == "free_rotation"
	is_driven = experiment_type == "driven_rotation"
	assert(is_free or is_driven)

	# We drive one motor at a time at steady state, fixed duty cycle.
	# All we need to know is which direction the torque vector points.
	TORQUE_DIR = np.zeros(3)
	TORQUE_DIR[0] = float(MOTOR_ORIENTATION[pwm_idx][1]) 
	TORQUE_DIR[1] = float(-MOTOR_ORIENTATION[pwm_idx][0])
	TORQUE_DIR[2] = float(-MOTOR_ORIENTATION[pwm_idx][2])

	# Rigid body rotation under constant torque
	def ddt_w(w, t, gamma, J, J_inv):
		return np.dot(J_inv, gamma - np.cross(w, np.dot(J, w)))

	# The axis phi is a rotation about the z axis in the body frame (yaw)
	# The axis theta is a rotation about the y axis in the phi-rotated body frame (pitch)
	# The axis psi is a rotation about the x axis in the phi, theta-rotated body frame (roll)
	def rotation(phi, theta, psi):
		R = np.zeros((3,3))

		R[0,0] = c(phi)*c(theta)
		R[1,0] = s(phi)*c(theta)
		R[2,0] = -s(theta)

		R[0,1] = -s(phi)*c(psi) + c(phi)*s(theta)*s(psi)
		R[1,1] = c(phi)*c(psi) + s(phi)*s(theta)*s(psi)
		R[2,1] = c(theta)*s(psi)

		R[0,2] = s(phi)*s(psi) + c(phi)*s(theta)*c(psi)
		R[1,2] = -c(phi)*s(psi) + s(phi)*s(theta)*c(psi)
		R[2,2] = c(theta)*c(psi)
		return R

	def unpack_free(theta):
		P = np.zeros((3,3))
		P[0, 0] = abs(theta[0])
		P[1, 1] = abs(theta[1])
		P[2, 2] = abs(theta[2])
		P_inv = np.zeros((3,3))
		P_inv[0, 0] = 1./abs(theta[0])
		P_inv[1, 1] = 1./abs(theta[1])
		P_inv[2, 2] = 1./abs(theta[2])
		R = rotation(*tuple(theta[3:6]))
		# Invertible & positive semi-definite by construction
		J = np.dot(R, np.dot(P, R.T))
		J_inv = np.dot(R, np.dot(P_inv, R.T))		
		return J, J_inv	

	def unpack_driven(theta):
		theta = np.abs(theta)
		torque_vector = theta[0]*np.multiply(theta[1:4], TORQUE_DIR)
		return torque_vector

	def sim_trace(t_expt, dc):
		# Use simulated dynamics, perfectly known, add noise, and see if you get back the w0 and J you put in.
		w0 = 0.2 + 0.05*np.random.randn(3) 	# Initial angular velocity
											# could infer this too, but with many training sets it gets high dimensional

		Jxx = 1.3e-05
		Jyy = 1.4e-05
		Jzz = 1.1e-04
		P0 = [Jxx, Jyy, Jzz]	 			# Principal moments
		R0 = [0.1, 0.3, -0.2]				# Euler angles of rotation to body frame from principal frame
		sigma_m = np.eye(3)
		sigma_m[0,0] = 0.000945 			# Observed from gyro while quad was still
		sigma_m[1,1] = 0.000992
		sigma_m[2,2] = 0.001013
		
		# estimated as 7-8cm x 40g of thrust with z 1/10 scale of xy
		# scaled by 1e-2 as otherwise motion unrealistic.
		motor_torque_scale = [1e-2, 0.07*0.04*9.8, 0.08*0.04*9.8, 0.001*0.04*9.8]

		theta_0 = np.array(P0 + R0) 	# 6 dimensional parameter vector
		theta_1 = np.array(motor_torque_scale) # 3 dimensional parameter vector
		J_sim, J_sim_inv = unpack_free(theta_0)
		torque_vector = unpack_driven(theta_1)		
		
		w_obs = None					# noise from gyro is pretty low; ignore it for now
		if t_expt is not None:			
			w_obs = odeint(ddt_w, w0, t=t_expt, args=(torque_vector*dc, J_sim, J_sim_inv)).T
			for idx in range(len(t_expt)):			
				w_noise = np.random.multivariate_normal([0, 0, 0], np.power(sigma_m, 2))
				w_obs[:, idx] += w_noise
		
		return w_obs, torque_vector, J_sim, J_sim_inv
	
	train = []
	test = []
	n_train = len(experiment["train"])		
	n_expt = 0
	for trace, dc, t_gyro_zero_start, t_gyro_zero_end, t_start, t_end in experiment["train"] + experiment["test"]:
		n_expt += 1
		if trace == "simulated":
			t_expt = np.linspace(0, t_end-t_start, (t_end-t_start)*100)  	# 100 Hz
			w_sim, _, _, _ = sim_trace(t_expt, dc)			
		else:
			sim = Simulator("iosink", trace)
			sim.run()
		
			t_w = np.array(sim.iosink.synced[lib.GYRO]["t"])
			w_x = sim.iosink.synced[lib.GYRO]["x"]
			w_y = sim.iosink.synced[lib.GYRO]["y"]
			w_z = sim.iosink.synced[lib.GYRO]["z"]

			# Rotate w from the IMU frame to the quad frame			
			to_quad_frame = lambda v: quaternion_rotation(qv=[0,v], qr=quaternion_inverse(q_imu_to_quad))[1]
			n = len(w_x)
			w = np.zeros((3, n), dtype='float')
			for idx in range(n):
				w_imu = np.array([w_x[idx], w_y[idx], w_z[idx]])
				w_quad = to_quad_frame(w_imu)
				w[:, idx] = w_quad

			# First, estimate zero-level gyro offset as system was stationary for quite some time.
			t0, tf = (t_gyro_zero_start, t_gyro_zero_end)
			stationary_mask = (t_w >= t0) * (t_w <= tf)
			w_stationary = w[:, stationary_mask] 			
			w_zero = w_stationary.sum(axis=1, keepdims=True) / stationary_mask.sum()
			w -= w_zero			

			expt_mask = (t_w >= t_start) * (t_w <= t_end)			
			t_expt = t_w[expt_mask]
			w_sim = w[:, expt_mask]			

		if n_expt <= n_train:
			train.append([t_expt, dc, w_sim])
		else:
			test.append([t_expt, dc, w_sim])
		
	# Measurement noise in radians ~ N(w_est[n], eye(3) * sigma_m^2), i.i.d, all n. 
	sigma_m = 0.001

	# When inferrring J_est up to a scale factor:
		# w0 is not inferred; high dimensional with multiple training sets.
		# Priors for P, R are independent.
		# Prior for P, R are uniform and thus ignored in the cost.
	# When inferring the torque_vector:
		# We fix J_est as inferred from a free rotation.
		# w0 is not inferred; high dimensional with multiple training sets.
		# Priors for torque_vector are independent.
		# Priors for torque_vector are uniform and thus ignored in the cost.

	# J_est, inferred from a free rotation up to a scaling factor.
	saved_free_descent = experiment["saved free descent"]
	if saved_free_descent is not None:
		J_est, J_est_inv = unpack_free(saved_free_descent)
		pp.pprint(J_est)

	def cost_free(theta):		
		J, J_inv = unpack_free(theta)
		ln_p = 0
		for t_expt, dc, w_train in train:
			w_est = odeint(ddt_w, w_train[:, 0], t=t_expt, args=(dc*np.zeros(3), J, J_inv)).T
			ln_p += (-1./(2*np.pi*sigma_m**2))*np.power(w_est - w_train, 2).sum(axis=0).sum()
		return -ln_p

	def cost_driven(theta):
		torque_vector = unpack_driven(theta)
		ln_p = 0		
		for t_expt, dc, w_train in train:
			w_est = odeint(ddt_w, w_train[:, 0], t=t_expt, args=(dc*torque_vector, J_est, J_est_inv)).T
			ln_p += (-1./(2*np.pi*sigma_m**2))*np.power(w_est - w_train, 2).sum(axis=0).sum()
		return -ln_p

	cost = cost_free if is_free else cost_driven

	thetas = []
	costs = []

	if is_free:
		Jxx = 0.1*np.random.random()
		Jyy = 0.1*np.random.random()
		Jzz = 0.1*np.random.random()
		P0 = [Jxx, Jyy, Jzz]	 			
		R0 = list(np.random.randn(3))
		theta_0 = np.array(P0 + R0)
	
	if is_driven:
		theta_0 = np.array([1, 0.1*np.random.random(), 0.1*np.random.random(), 0.1*np.random.random()])		

	local_descent = least_squares(cost, theta_0, max_nfev=10, verbose=0)	
	thetas.append(local_descent.x)
	costs.append(np.sqrt(local_descent.cost))
	n_parameters = len(thetas[-1])

	# Tune this for the different estimation problems.
	if is_free:
		step_size = np.eye(n_parameters)*0.1**2
		decay_scale = 1.2
		n = 200
		epoch = n/10
		iters = 20

	if is_driven:		
		decay_scale = 1.2
		step_size = np.eye(n_parameters)*0.1**2 	# TODO May want to divide up to 40x
		step_size[0,0] = 1**2 						# TODO Same
		n = 40		# TODO In early stages, may want to keep this to 1 epoch, view, etc.
		epoch = 20 # n/10
		iters = 20 	# TODO Try increasing

	def again(ss):
		yep = input("Again [y|n]? ") == "y"
		if not yep:
			return False, None
		pp.pprint(ss)
		step_scale = input("Scale step size by... ")
		if len(step_scale) > 0:
			step_size = ss * float(step_scale)**2
		else:
			step_size = ss
		return True, step_size

	first_pass = True
	while True:
		if not first_pass:
			yep, step_size = again(step_size)
			if not yep:
				break
		first_pass = False

		for idx in range(n):
			if (idx+1) % epoch == 0:
				step_size /= decay_scale**2
			
			theta_0_proposed = thetas[-1] + np.random.multivariate_normal([0]*n_parameters, step_size)
			local_descent = least_squares(cost, theta_0_proposed, max_nfev=iters, verbose=0)

			ll_km1 = -costs[-1]
			ll_k = -np.sqrt(local_descent.cost)	 	# they square our cost function
			p_ratio = ll_k - ll_km1
			if p_ratio < 10:
				ratio = np.exp(ll_k - ll_km1)
				p_accept = ratio/(1. + ratio)		
			else:				
				p_accept = 1.
			accepted = np.random.random() <= p_accept

			if accepted:			
				thetas.append(local_descent.x)
				costs.append(-ll_k)
				print("Residual at %0.3f" % costs[-1])

		fig, ax = plt.subplots(3,1)
		ax[0].plot(np.log10(costs))

		local_descent = least_squares(cost, thetas[np.argmin(costs)], max_nfev=150, verbose=0)
		thetas.append(local_descent.x)
		costs.append(np.sqrt(local_descent.cost))

		if is_free:
			J_est, J_est_inv = unpack_free(local_descent.x)
			torque_vector_est = np.zeros(3)
		if is_driven:			
			torque_vector_est = unpack_driven(local_descent.x)

		for t_expt, dc, w_train in train:
			w_est = odeint(ddt_w, w_train[:, 0], t=t_expt, args=(dc*torque_vector_est, J_est, J_est_inv)).T
			ax[1].plot(t_expt, w_est.T, 'r-')
			ax[1].plot(t_expt, w_train.T, 'k-')

		for t_expt, dc, w_test in test:
			w_est = odeint(ddt_w, w_test[:, 0], t=t_expt, args=(dc*torque_vector_est, J_est, J_est_inv)).T
			ax[2].plot(t_expt, w_est.T, 'r-')
			ax[2].plot(t_expt, w_test.T, 'k-')		

		_, torque_vector_sim, J_sim, _ = sim_trace(None, None)

		pp.pprint(local_descent.x)

		pp.pprint(J_sim)
		pp.pprint(J_est)
		pp.pprint(J_est / J_sim)

		pp.pprint(torque_vector_sim)
		pp.pprint(torque_vector_est)
		pp.pprint(torque_vector_est / torque_vector_sim)

		plt.show()
	return

logs = {
	0 : {
		# "trace" : "logs/dynamics/trace_tethered_iosink_1",
		# "trace" : "logs/dynamics/trace_drivetorque_driven_pwm2_pwm4_dc10",
		# "trace" : "logs/dynamics/trace_dynamics_driven_pwm1_dc10", 
		"trace" : "logs/dynamics/trace_dynamics_free",
		# "trace" : "data/dynamics_yjig_motor1_dc5",
		# "trace" : "data/nondynamics_calibration_compass_sphere_methodical",
		# "trace" : "data/nondynamics_calibration_axes",
		"estimator" : check_time_series,
	},
	
	1 : {
		"trace" : "logs/dynamics/trace_tethered_iosink_1",
		"estimator" : check_constrained_rotation,
		"gyro offset" : [10,20],
		"rotation" : [42.1, 42.6],      
	},

	2 : {
		"trace" : "logs/dynamics/trace_unpowered_tumble_iosink",
		"estimator" : check_time_series,
	},

	3 : {		
		"experiment" : ["free_rotation", None],
		"saved free descent" : None,
		"train" : [["simulated", 0, None, None, 0, 3]],
		"test" : [["simulated", 0, None, None, 0, 3]],
		"estimator" : estimate_parameters,
	},
	
	4 : {		
		"experiment" : ["driven_rotation", 1],
		"saved free descent" : np.array([ 0.20048834, -0.18762024, -1.58589549,  1.61578621,  0.22363483, 0.28045701]),		
		"saved driven descent" : np.array([-2.45046582,  2.24939207,  1.63723776, -0.02279823]),
		"train" : [["simulated", 0.1, None, None, 0, 3], ["simulated", 0.25, None, None, 0, 3], ["simulated", 0.5, None, None, 0, 3]],
		"test" : [["simulated", 0.4, None, None, 0, 3]],
		"estimator" : estimate_parameters,
	},	

	5 : {		
		"experiment" : ["free_rotation", None],
		"saved free descent" : None,
		"train" : [	["logs/dynamics/trace_dynamics_free", 0, 28, 30, 31.468, 32.03],
					["logs/dynamics/trace_dynamics_free", 0, 35, 36, 36.98, 37.478],
					["logs/dynamics/trace_dynamics_free", 0, 44.5, 45.5, 46.04, 46.63],
					["logs/dynamics/trace_dynamics_free", 0, 58, 59, 60, 60.49],
					["logs/dynamics/trace_dynamics_free", 0, 67, 68, 69.407, 69.808],
					["logs/dynamics/trace_dynamics_free", 0, 77, 77.5, 78.164, 78.601] ],
		"test" : [["logs/dynamics/trace_dynamics_free", 0, 52, 52.5, 53.414, 54.08]],
		"estimator" : estimate_parameters,
	},

	6 : {		
		"experiment" : ["driven_rotation", 1],		
		"saved free descent" : np.array([ 0.82636334, -2.12769396, -1.50871953,  1.53806519,  0.07294404, 1.59882654]),
		"saved driven descent" : np.array([ 2.33975492e+01, 1.04947192e+01,  1.65715201e+01, 2.17972041e-07]),
		# Maps to torque scaling of -np.array([2.45550709e+02, 3.87732955e+02, 5.10001156e-06]) * dc for PWM1
		# with a J_chassis of 
		# np.array([
		# [ 1.50856451, -0.02084717,  0.02037978],
       	# [-0.02084717,  0.83391386,  0.09393599],
       	# [ 0.02037978,  0.09393599,  2.12029846]
       	# ])
		"train" : [	["logs/dynamics/trace_dynamics_driven_pwm1_dc10", 0.1, 14.5, 15, 16, 16.4], 
					["logs/dynamics/trace_dynamics_driven_pwm1_dc10", 0.1, 32, 33, 34.176, 34.72],
					["logs/dynamics/trace_dynamics_driven_pwm1_dc10", 0.1, 38, 39, 39.7876, 40.4842],
					["logs/dynamics/trace_dynamics_driven_pwm1_dc15", 0.15, 2, 3, 3.73, 4.14],
					["logs/dynamics/trace_dynamics_driven_pwm1_dc15", 0.15, 20.5, 21, 22.7, 23.25],
					["logs/dynamics/trace_dynamics_driven_pwm1_dc15", 0.15, 20.5, 21, 34.25, 34.78],
					["logs/dynamics/trace_dynamics_driven_pwm1_dc20", 0.2, 2, 3, 4.07, 4.44],
					["logs/dynamics/trace_dynamics_driven_pwm1_dc20", 0.2, 2, 3, 22.09, 22.43] ],
		"test" : [["logs/dynamics/trace_dynamics_driven_pwm1_dc15", 0.15, 14.5, 15, 16.43, 16.88]],
		"estimator" : estimate_parameters,
	},
}

if __name__ == "__main__":
	experiment_id = int(sys.argv[1])
	experiment = logs[experiment_id]

	if experiment_id in [0,1,2]:
		sim = Simulator("iosink", experiment["trace"])
		sim.run()
	else:
		sim = None

	experiment["estimator"](sim, experiment)