/* Usage
	gcc -Wall -pthread -o test i2c-speedup-pigpio.c -lpigpio -lrt
	sudo ./test
*/

#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <pigpio.h>

int main(int argc, char** argv) {
	time_t start, stop;

	uint8_t address = 0x6B;
	uint8_t reg = 0x28;
	uint8_t nbytes = 6;

	if (gpioInitialise()<0) return -1;
	
	int dev = i2cOpen(1, address, 0);
	if (dev<0) return -1;

	char buf[6];

	int ret = 0;
	time(&start);
	for (int i = 0; i < 3000; i++) {
		ret = i2cReadI2CBlockData(dev, reg, buf, nbytes);
		if (ret<0) break;
	}
	time(&stop);
	float delta_ms = 1000*(((float) (stop-start)) / 30);

	gpioTerminate();
	if (ret >= 0) {
		printf("Start: %ld, Stop: %ld\n", start, stop);
		printf("Took %0.3f ms to read using pigpio-c\n", delta_ms);
		return 0;
	} else {
		printf("Failed.\n");
		return ret;
	}
}