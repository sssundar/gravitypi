import os, ipdb
import numpy as np
from skimage import io, filters
from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt

DATAPATH = "/home/sushant/Documents/gravitypi/data"

def get_gravity_vector(filename):
	with open(filename, "r") as f:
		lines = [line.strip() for line in f]
	values = {"t": [], "x": [], "y": [], "z": []}
	for line in lines:
		t, x, y, z = [float(x) for x in line.split(",")[:-1]]
		values["t"].append(t)
		values["x"].append(x)
		values["y"].append(y)
		values["z"].append(z)
	g_avg = np.array([sum(values["x"])/len(values["t"]), sum(values["y"])/len(values["t"]), sum(values["z"])/len(values["t"])])
	g = g_avg / np.linalg.norm(g_avg)	
	return g

def get_input(datapath):
	dataset = {}
	for d in os.listdir(datapath):
		fname = d.strip().split(".")[0]
		is_image = 'img' in fname
		idx = int(fname[3:]) if is_image else int(fname[1:])
		if idx not in dataset.keys():
			dataset[idx] = {"img": None, "g": None}
		if is_image:
			dataset[idx]["img"] = os.path.join(datapath, d)
		else:
			dataset[idx]["g"] = os.path.join(datapath, d)

	# # Plot the gravity vector over time for a few simple rotations as a sanity check
	# fig = plt.figure()
	# ax = fig.gca(projection='3d')
	# for k in sorted(dataset.keys()):
	# 	g = get_gravity_vector(dataset[k]["g"])		
	# 	ax.quiver(0, 0, 0, g[0], g[1], g[2], length=0.02, normalize=True)
	# ax.set_xlabel("x")
	# ax.set_ylabel("y")
	# ax.set_zlabel("z")
	# plt.show()

	RAD_TO_DEG = 180./np.pi
	for k in sorted(dataset.keys()):
		x,y,z = get_gravity_vector(dataset[k]["g"])
		dataset[k]["phi"] = np.arccos(z)*RAD_TO_DEG				
		dataset[k]["theta"] = np.arctan2(y,x)*RAD_TO_DEG
	
	EDGE_THRESHOLD = 0.15
	_, ax = plt.subplots(1, len(dataset.keys()), sharex=True, sharey=True)
	for k in sorted(dataset.keys()):
		# print([dataset[k]["phi"], dataset[k]["theta"]])

		img = io.imread(dataset[k]["img"], as_gray=True)		
		edges = filters.sobel(img) > 0.15
		
		# # For each row, print the mean pixel for each edge
		# rows, cols = edges.shape
		# am_tracking = False
		# lb = 0
		# centroids = []
		# for r in range(rows):
		# 	this_row = []
		# 	for c in range(cols):
		# 		if edges[r,c]:
		# 			if not am_tracking:
		# 				am_tracking = True
		# 				lb = c
		# 		else:
		# 			if am_tracking:
		# 				am_tracking = False
		# 				this_row.append( (c-1+lb)*1./2 )						
		# 	if am_tracking:
		# 		am_tracking = False
		# 		this_row.append( (len(cols)-1+lb)*1./2 )
		# 	centroids.append(np.array(this_row))

		# dcenters = []
		# for rdx in range(rows):
			# centers = centroids[rdx]			
			# Manually do this till you figure out how to segment the black tape
			# centers = centers[np.multiply(centers >= 12, centers <= 25)]
			# if len(centers) != 2:
				# continue
			# print(rdx, centers)
			# dcenters.append(centers[1]-centers[0])
		# dcenters = np.array(dcenters)
		# print(np.mean(dcenters), np.std(dcenters))

		ax[k].imshow(img, aspect='auto', cmap='gray')		
		# ipdb.set_trace()
	plt.show()

if __name__ == "__main__":
	get_input(DATAPATH)
