# http://blog.kevindoran.co/bluetooth-programming-with-python-3/

import bluetooth
import sys
from time import sleep, time
import struct

if len(sys.argv) < 2:
	print("usage: python comms-vm.py text|auto")
	sys.exit(1)

serverMACAddress = 'B8:27:EB:B7:10:5F'
port = 3
s = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
s.connect((serverMACAddress, port))
print("connected")
try:
	if sys.argv[1] == 'text':
		while 1:
			text = input()
			if "quit" in text:
				break
			s.send(text)
	else:
		start = time()
		cnt = 0
		while 1:
			# print(time()-start)
			binary_data = struct.pack("<Ifffff", *[cnt, float(cnt), float(cnt), float(cnt), float(cnt), float(cnt)])
			s.send(binary_data) #24 bytes
			sleep(0.1)
			cnt += 1
finally:
	print("closing socket")
	s.close()
