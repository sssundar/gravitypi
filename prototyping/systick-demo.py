import datetime, threading, time

# https://stackoverflow.com/a/18180189/1623303
def foo():
	next_call = time.time()
	while True:
		print(datetime.datetime.now())
		next_call = next_call+0.05;
		time.sleep(next_call - time.time())

if __name__ == "__main__":
	timerThread = threading.Thread(target=foo)
	timerThread.daemon = True
	timerThread.start()

	try:
		while True:
			time.sleep(1)
	except KeyboardInterrupt:
		pass

