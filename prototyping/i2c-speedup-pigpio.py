#/usr/bin/python3

import pigpio
from time import time

address = 0x6B
reg = 0x28
nbytes = 6

local = pigpio.pi()
dev = local.i2c_open(1, address)

start_s = time()
for idx in range(100):
	local.i2c_read_i2c_block_data(dev, reg, nbytes)
delta_ms = int(1000*(time() - start_s))

local.stop()
print("Took %d ms to read using pigpio-python3" % delta_ms)

