from smbus2 import SMBus, i2c_msg
from time import time

bus = SMBus(1)
address = 0x6B
reg = 0x28
nbytes = 6

start_s = time()
print(start_s)
for idx in range(100):
	write = i2c_msg.write(address, [reg])
	read = i2c_msg.read(address, nbytes)
	bus.i2c_rdwr(write, read)	
end_s = time()
print(end_s)
delta_ms = int(1000*(end_s - start_s))

bus.close()
print("Took %d ms to read using smbus2" % delta_ms)