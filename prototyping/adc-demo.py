from time import time, sleep
from _i2cbus import ffi as ffi
from _i2cbus import lib as lib
import numpy as np

def hexify(byte):
	return format(byte, '#04x')

# ADC081C027: Battery Voltage. 400kHz I2C.
class ADC():
	# TODO Fix
	#def __init__(self, logger, scheduler):
	def __init__(self):
		self.name = "battery"

		# self.logger = logger
		# self.scheduler = scheduler		
		lib.i2cbus_open(1)

		# i2c constants
		self.address = 0x51 # i2c address, with ADR0 grounded
		self.sample_bytes = 2 # v(msb)v(lsb)
		self.dst = np.zeros(self.sample_bytes, dtype='uint8')
		self.dst_ptr = ffi.cast("uint8_t *", self.dst.ctypes.data)

		# Scaling
		self.volts_per_lsb = 1./23.739
	
		# We do not need to track the timestamp. Log timestamps suffice.
		self.battery_voltage_v = None
		
		self.warning_voltage_v = 6.6
		self.alert = False
		self.safe_pwm_duty_cycle = 0.
		self.unsafe_pwm_duty_cycle = 0.7

		# configure the adc
		self.configure()
		return

	def read_voltage(self):		
		# the device's internal register address pointer starts out at 0x00 (conversion register) so here, 
		# since we never modify the device configuration from its power-on state,
		# we can just read 2 bytes directly.		
		lib.i2cbus_implicit_read(self.address, self.dst_ptr, self.sample_bytes);
		msb, lsb = list(self.dst)
		v = (((msb & 0x0F) << 4) | ((lsb & 0xF0) >> 4)) * self.volts_per_lsb
		return v

	def configure(self):
		# in the 'normal' power on-state (which we never leave) the device only triggers
		# an ADC read after an I2C read of its conversion register. this read primes us
		# so the next one is meaningful.
		self.read_voltage()

	# TODO Fix
	def deinit(self):
		lib.i2cbus_close()
		return		

	# TODO Fix
	# This function will be called at ~1Hz, polled.
	# def handle_task(self, flagged_at):
	def handle_task(self):			
		self.battery_voltage_v = self.read_voltage()
		self.alert = self.battery_voltage_v <= self.warning_voltage_v

		# self.logger.info(self, "battery at %0.2f V, %s" % (self.battery_voltage_v, 'unsafe' if self.alert else 'safe'))
		print("battery at %0.2f V, %s" % (self.battery_voltage_v, 'unsafe' if self.alert else 'safe'))
		return

	def get_warning_led_pwm_duty_cycle(self):
		if self.alert:
			return self.unsafe_pwm_duty_cycle
		else:
			return self.safe_pwm_duty_cycle

if __name__ == "__main__":
	battery = ADC()
	try:
		while True:
			sleep(1)
			battery.handle_task()
	except KeyboardInterrupt:
		battery.deinit()
