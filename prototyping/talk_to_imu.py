import wiringpi
from smbus2 import SMBus
from picamera import PiCamera
import time, copy

# LSM303DLHC (Accelerometer|Magnetometer|Thermometer)
I2C_ADDR 			= 0x19
ADDR_AUTOINCREMENT  = 0x80 				  # To read multiple registers in sequence from the sensor, the MSB of the SUB address, i.e. 0x80 & SUB, must be set to 1. 
CTRL_REG5_A 		= ["CR5", 0x24, 0x80] # Reboot sensor. The magnetometer and thermometer are disabled by default.
CTRL_REG1_A 		= ["CR1", 0x20, 0x27] # 10Hz, Normal Mode, Accel XYZ Enabled.
CTRL_REG3_A 		= ["CR3", 0x22, 0x10] # INT1 has DRDY1 from the accelerometer.
CTRL_REG4_A 		= ["CR4", 0x23, 0x80] # Block update, LSB at lower address, FS00, High Resolution Disabled, SPI disabled.
STATUS_REG_A 		= ["SRG", 0x27, 0x08] # The bit we care about is ZXYDA (all three available)
OUT_X_L_A 			= 0x28

# LINT1 (Data Ready) Interrupt Pin, in WiringPi pin numbering.
DRDY				= 0

# I2C Bus and Sample Holders
bus 				= None
MAX_SAMPLES 		= 30
samples 			= [None] * MAX_SAMPLES
sample_idx 			= 0
am_sampling			= False

def display_config():
	config_registers = [CTRL_REG1_A, CTRL_REG3_A, CTRL_REG4_A, STATUS_REG_A]
	print("")
	print("Configuration --")
	for label, addr, _ in config_registers:
		byte = bus.read_byte_data(I2C_ADDR, addr)
		print("Register %s: %s" % (label, format(byte, '#04x')))
	print("")

def unpack(utc_ms, block, sreg):
	x = (block[0] + (block[1] << 8))
	y = (block[2] + (block[3] << 8))
	z = (block[4] + (block[5] << 8))

	sample = [x,y,z]
	sample = [(v - (1<<16)) if (v >= (1<<15)) else v for v in sample]	

	# Calibration coefficients for X, Y, Z in 'standard' orientation
	SCALE = [16562.67, 16133.33, 16781.33]
	OFFSET = [-0.003, -0.027, -0.027]

	# Rotation to 'standard' orientation: Z to -Z, Y to X, X to Y
	x = (sample[1]*1./SCALE[0])-OFFSET[0]
	y = (sample[0]*1./SCALE[1])-OFFSET[1]
	z = (-sample[2]*1./SCALE[2])-OFFSET[2]

	return [utc_ms, x, y, z, format(sreg, '#04x')] # units: ms, g, g, g, bitmask

def data_callback():
	global bus, MAX_SAMPLES, samples, sample_idx, am_sampling 
	global I2C_ADDR, STATUS_REG_A, ADDR_AUTOINCREMENT, OUT_X_L_A
	
	if am_sampling:
		sreg = bus.read_byte_data(I2C_ADDR, STATUS_REG_A[1])
		block = bus.read_i2c_block_data(I2C_ADDR, ADDR_AUTOINCREMENT | OUT_X_L_A, 6)		
		samples[sample_idx] = unpack(time.time(), block, sreg)
		sample_idx = (sample_idx + 1) % MAX_SAMPLES		

if __name__ == "__main__":	
	# Let the camera stabilize
	camera = PiCamera()
	camera.resolution = (640, 480)
	camera.start_preview()
	# Camera warm-up time
	time.sleep(2)

	# Let the IMU stabilize
	am_sampling = True
	bus = SMBus(1)
	bus.write_byte_data(I2C_ADDR, CTRL_REG5_A[1], CTRL_REG5_A[2]) 			# Reboot the IMU
	time.sleep(1)  												  			# Give the IMU time to boot

	bus.write_byte_data(I2C_ADDR, CTRL_REG1_A[1], CTRL_REG1_A[2]) 			# Configure the IMU
	bus.write_byte_data(I2C_ADDR, CTRL_REG3_A[1], CTRL_REG3_A[2])
	bus.write_byte_data(I2C_ADDR, CTRL_REG4_A[1], CTRL_REG4_A[2])		
	time.sleep(1)  												  			# Make sure there's no edge on DRDY

	wiringpi.wiringPiSetup()
	wiringpi.pinMode(DRDY, wiringpi.GPIO.INPUT)
	wiringpi.pullUpDnControl(DRDY, wiringpi.GPIO.PUD_DOWN)
	wiringpi.wiringPiISR(DRDY, wiringpi.GPIO.INT_EDGE_RISING, data_callback)

																			# Clear the DRDY signal to allow interrupt edges.
	bus.read_i2c_block_data(I2C_ADDR, ADDR_AUTOINCREMENT | OUT_X_L_A, 6)
	time.sleep(3) 															# Let the circular sample buffer fill up

	# Capture still images alongside 1s windows of acceleration.
	collect_idx = 0	
	am_collecting = lambda: input("Capture? [y|n]: ") == 'y' 
	while am_collecting():
		camera.capture('img%d.jpg' % collect_idx, resize=(64, 64))
		jdx = sample_idx - 1
		snapshot = copy.deepcopy(samples)
		window = []
		for _ in range(10):			
			window.append(snapshot[jdx])
			jdx = (jdx - 1) % MAX_SAMPLES
		with open("g%d.txt" % collect_idx, "w") as trace:
			for utc_ms, x_g, y_g, z_g, sreg in window:
				trace.write("%0.3f, %0.3f, %0.3f, %0.3f, %s\n" % (utc_ms, x_g, y_g, z_g, sreg))
		collect_idx += 1		

	# Clean up
	am_sampling = False	
	time.sleep(1)	
	bus.close()
