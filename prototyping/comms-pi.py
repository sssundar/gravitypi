# http://blog.kevindoran.co/bluetooth-programming-with-python-3/

import bluetooth
from time import time
import struct

hostMACAddress = 'B8:27:EB:B7:10:5F'
port = 3
backlog = 1

fmt = struct.Struct("<Ifffff")
size = fmt.size

fd = open("/home/pi/gravitypi/logs/gamepad", "wb")

s = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
s.bind((hostMACAddress, port))
s.listen(backlog)
print("setup")
try:
	client = None
	client, clientInfo = s.accept()
	print("waiting")
	start = time()
	while 1:
		data = client.recv(size)
		if data:
			# print(time()-start)
			# print(fmt.unpack_from(data))
			fd.seek(0, 0)
			fd.write(data)
			# client.send(data)
except: 
	print("closing socket")
	if client is not None:
		client.close()	
	s.close()
	
	fd.close()
