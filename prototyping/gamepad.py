import evdev
import sys

devices = [evdev.InputDevice(path) for path in evdev.list_devices()]
gamepad = None
for device in devices:
	if 'Microsoft X-Box' in device.name:
		gamepad = device.path
if gamepad is None:
	print("Could not find gamepad.")
	sys.exit(1)

gamepad = evdev.InputDevice(gamepad)
for event in gamepad.read_loop():
	if event.type == evdev.ecodes.EV_ABS:
		print(evdev.categorize(event))
		print(event)