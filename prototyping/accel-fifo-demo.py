import wiringpi
from smbus2 import SMBus
import time, copy

# LSM303DLHC (Accelerometer|Magnetometer|Thermometer)
I2C_ADDR 			= 0x19
SUBADDR_AUTOINC     = 0x80 
CONFIGURATION 		= [
	[0x24, 0x80, 1, True, "CTRL_REG5_A: Reboot accelerometer with magnetometer and thermometer disabled."],	
	[0x22, 0x06, 0, True, "CTRL_REG3_A: FIFO watermark and overrun interrupts enabled on LINT1."],
	[0x23, 0x80, 0, True, "CTRL_REG4_A: Block update in little endian (LSB first) with +/-2g resolution"],
	[0x24, 0x40, 0, True, "CTRL_REG5_A: FIFO enabled without latching of interrupt requests."],
	[0x2E, 0x00, 0, False, "FIFO_CTRL_REG_A: Clear mode bits to wipe away any error state."],
	[0x2E, 0x88, 0, True, "FIFO_CTRL_REG_A: FIFO streaming. Discards older samples if full. FIFO threshold at 8/32."],
	[0x20, 0x57, 0, True, "CTRL_REG1_A: Start 100Hz sampling of x, y, z."],
]
SAMPLING_PERIOD_S = 0.01
FIFO_SRC_REG_A = 0x2F 
FIFO_WTM_FLAG  = 0x80 
FIFO_OVR_FLAG  = 0x40
FIFO_FILL_LVL  = 0x1F
FIFO_DATA_ADDR = 0x28 # one 32-element FIFO per acceleration axis
SAMPLE_BYTES   = 6 	  # x(lsb)x(msb) y(lsb)y(msb) z(lsb)z(msb)

# FIFO Interrupt Pin and Flag, in WiringPi pin numbering.
INTERRUPT_PIN  = 2
should_check_fifo = False
stimulus = []
startresponse = []
endresponse = []

# I2C Bus and Sample Holders
I2CBUS 				= None
MAX_SAMPLES 		= 16*100
samples 			= []
sample_idx 			= 0

def check_configuration():
	passed = True
	for addr, expected, _, check, _ in CONFIGURATION[1:]:
		if check:
			byte = I2CBUS.read_byte_data(I2C_ADDR, addr)
			passed = passed and (byte == expected)
	result = "passed" if passed else "failed"
	print("accelerometer %s configuration check" % result)

def show_register(description, byte):
	print(description + " %s" % format(byte, '#04x'))

def unpack(utc_ms, block):
	x = (block[0] + (block[1] << 8))
	y = (block[2] + (block[3] << 8))
	z = (block[4] + (block[5] << 8))

	sample = [x,y,z]
	sample = [(v - (1<<16)) if (v >= (1<<15)) else v for v in sample]	

	# Calibration coefficients for X, Y, Z in 'standard' orientation
	SCALE = [16562.67, 16133.33, 16781.33]
	OFFSET = [-0.003, -0.027, -0.027]

	# Rotation to 'standard' orientation: Z to -Z, Y to X, X to Y
	x = (sample[1]*1./SCALE[0])-OFFSET[0]
	y = (sample[0]*1./SCALE[1])-OFFSET[1]
	z = (-sample[2]*1./SCALE[2])-OFFSET[2]

	return [utc_ms, x, y, z] # units: ms, g, g, g

def cb():
	global should_check_fifo, stimulus
	should_check_fifo = True
	stimulus.append(time.time())

def ignore():
	pass

def sample(): 	
	global I2CBUS, should_check_fifo, sample_idx, stimulus, startresponse, endresponse

	# configure the Pi
	wiringpi.wiringPiSetup()
	wiringpi.pinMode(INTERRUPT_PIN, wiringpi.GPIO.INPUT)
	wiringpi.pullUpDnControl(INTERRUPT_PIN, wiringpi.GPIO.PUD_DOWN)
	wiringpi.wiringPiISR(INTERRUPT_PIN, wiringpi.GPIO.INT_EDGE_RISING, cb)

	# configure the IMU
	I2CBUS = SMBus(1)
	for addr, value, delay, _, description in CONFIGURATION:
		I2CBUS.write_byte_data(I2C_ADDR, addr, value)		
		if delay > 0:
			time.sleep(delay)
	check_configuration()	
	
	# poll the fifo
	am_sampling = True
	while am_sampling:
		if not should_check_fifo:
			continue

		startresponse.append(time.time())

		status = I2CBUS.read_byte_data(I2C_ADDR, FIFO_SRC_REG_A)
		should_check_fifo = False

		ready = (status & FIFO_WTM_FLAG) != 0
		overrun = (status & FIFO_OVR_FLAG) != 0
		n_samples = status & FIFO_FILL_LVL		

		if overrun:
			print("warning: overran fifo")

		if not ready:
			print("warning: below watermark")			
			
		if n_samples <= 0:
			print("warning: strange sample count")
			continue

		blocks = []
		wtm_utc_ms = time.time()
		for idx in range(n_samples):
			blocks.append(I2CBUS.read_i2c_block_data(I2C_ADDR, SUBADDR_AUTOINC | FIFO_DATA_ADDR, SAMPLE_BYTES))
		
		base_utc_ms = wtm_utc_ms - (n_samples-1)*SAMPLING_PERIOD_S
		for idx in range(n_samples):
			samples.append(unpack(base_utc_ms + idx*SAMPLING_PERIOD_S, blocks[idx]))
			sample_idx += 1
			if sample_idx >= MAX_SAMPLES:				
				am_sampling = False
				break

		endresponse.append(time.time())

	# clean up		
	wiringpi.wiringPiISR(INTERRUPT_PIN, wiringpi.GPIO.INT_EDGE_RISING, ignore)
	should_check_fifo = False
	I2CBUS.close()

	# display the samples
	if len(samples) > 0:
		for ms, x, y, z in samples:
			print("%0.3f, %0.3f, %0.3f, %0.3f" % (ms, x, y, z))
	else:
		print("No samples to dump.")

	print("")
	assert(len(stimulus) == len(startresponse))
	assert(len(endresponse) == len(startresponse))
	for kdx in range(len(stimulus)):
		print("%0.3f ms, %0.3f ms" % ((startresponse[kdx]-stimulus[kdx])*1000, (endresponse[kdx]-startresponse[kdx])*1000))


if __name__ == "__main__":
	sample()