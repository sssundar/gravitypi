from cffi import FFI
import sys

if __name__ == "__main__":
	ffibuilder = FFI()
	ffibuilder.cdef("""
		void i2cbus_open(uint8_t bus_id);
		void i2cbus_close(void);
		void i2cbus_implicit_read(uint8_t addr, uint8_t *dst, int n_bytes);
		void i2cbus_explicit_read(uint8_t addr, uint8_t reg, uint8_t *dst, int n_bytes);
		void i2cbus_implicit_write(uint8_t addr, uint8_t *src, int n_bytes);
		void i2cbus_explicit_write(uint8_t addr, uint8_t reg, uint8_t *src, int n_bytes);
	""")

	ffibuilder.set_source("_i2cbus",
	"""
		#include "i2cbus.h"
	""",
	sources=["i2cbus.c"])

	ffibuilder.compile(verbose=True)