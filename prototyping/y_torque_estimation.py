import sys, os
REVA = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "reva")
sys.path.append(REVA)
from simulator import Simulator
from board import CAPTURING_DYNAMICS
if not CAPTURING_DYNAMICS:
	print("This script assumes you are giving it traces captured with CAPTURING_DYNAMICS enabled. Please enable it.")
	sys.exit(1)
from _optimizations import ffi as ffi
from _optimizations import lib as lib
from quaternions import *
from frames import q_imu_to_quad
import numpy as np
from numpy import cos as c
from numpy import sin as s
from scipy.integrate import odeint
import matplotlib.pyplot as plt
import ipdb

# All of these were 3s spurts of motor drive using CAPTURING_DYNAMICS so they are already time aligned. 
traces = [ 	[1, "logs/dynamics/dynamics_yjig_motor1_dc5", None, None, None, None, -1., 'r-'],
			[2, "logs/dynamics/dynamics_yjig_motor2_dc5", None, None, None, None, 1., 'g-'],
			[3, "logs/dynamics/dynamics_yjig_motor3_dc5", None, None, None, None, 1., 'b-'],
			[4, "logs/dynamics/dynamics_yjig_motor4_dc5", None, None, None, None, -1., 'k-'], ]

motor_sign = lambda mid: traces[mid-1][-2]
color = lambda mid: traces[mid-1][-1]
trace = lambda mid: traces[mid-1][1]
get_motorid = lambda annot: annot[0]

def grab_w_y(ax, motorid):
	s = Simulator("iosink", trace(motorid))
	s.run()

	t = np.array(s.iosink.synced[lib.GYRO]["t"])
	w = np.array(s.iosink.synced[lib.GYRO]["y"])

	mask = np.multiply(t>3.5, t<5.5)
	t = t[mask]
	w = w[mask]
	t -= min(t)

	ax.plot(t, motor_sign(motorid) * w, color(motorid), label="%dm" % motorid)

def visualize_measurements(ax):
	for annot in traces:
		grab_w_y(ax, get_motorid(annot))

def sim_w_y(ax, motorid):
	dc = 0.05
	T_max = 387
	Jy = 0.834
	T_friction = 4
	T_g = T_max*0.1
	theta_0 = 0
	w_0 = 0
	t_final_s = 2.
	eq_phase = 0.08
	def motor_ramp(t):
		ss = 0.4
		if t < ss:
			return 0.1 + 0.9*(t/ss)**2
		return 1.

	def ddt_state(state, t):
		theta, w = (state[0], state[1])
		ddt = np.zeros(2)
		ddt[0] = w
		ddt[1] = (1./Jy)*( motor_sign(motorid)*motor_ramp(t)*dc*T_max + T_g*s(theta+eq_phase) - np.sign(w)*np.abs(w)*T_friction)
		return ddt

	t_sim = np.linspace(0, t_final_s, 1000)
	state_0 = np.array([theta_0, w_0])
	state = odeint(ddt_state, state_0, t=t_sim)

	w = np.array([x[1] for x in state])
	theta = np.array([x[0] for x in state])

	# plt.plot(t_sim, theta)
	# plt.plot(t_sim, np.pi*np.ones(len(t_sim)), 'r--')
	# plt.plot(t_sim, -np.pi*np.ones(len(t_sim)), 'r--')
	# plt.show()

	ax.plot(t_sim, motor_sign(motorid) * w, color(motorid), label="%ds" % motorid)

def visualize_sim(ax):
	for annot in traces:
		sim_w_y(ax, get_motorid(annot))

def pole_sim_w_y(ax, motorid):
	dc = 0.05
	T_max = 387
	Jy = 0.834
	T_friction = 4
	T_g = T_max*0.1
	eq_phase = 0.08
	T_pole = 3
	t_final_s = 2.
	theta_0 = 0
	w_0 = 0
	T_0 = 0

	def ddt_state(state, t):
		theta, w, motor_torque = (state[0], state[1], state[2])
		ddt = np.zeros(3)
		ddt[0] = w
		ddt[1] = (1./Jy)*( motor_sign(motorid)*motor_torque + T_g*s(theta+eq_phase) - np.sign(w)*np.abs(w)*T_friction)
		ddt[2] = -T_pole*motor_torque + dc*T_max*T_pole
		return ddt

	t_sim = np.linspace(0, t_final_s, 1000)
	state_0 = np.array([theta_0, w_0, T_0])
	state = odeint(ddt_state, state_0, t=t_sim)

	theta = np.array([x[0] for x in state])
	w = np.array([x[1] for x in state])
	motor_torque = np.array([x[2] for x in state])

	# plt.plot(t_sim, theta)
	# plt.plot(t_sim, np.pi*np.ones(len(t_sim)), 'r--')
	# plt.plot(t_sim, -np.pi*np.ones(len(t_sim)), 'r--')
	# plt.show()

	ax.plot(t_sim, motor_sign(motorid) * w, color(motorid), label="%ds" % motorid)

def visualize_polesim(ax):
	for annot in traces:
		pole_sim_w_y(ax, get_motorid(annot))

if __name__ == "__main__":
	fig, ax = plt.subplots(3,1, sharex=True)
	visualize_measurements(ax[0])
	visualize_sim(ax[1])
	visualize_polesim(ax[2])
	plt.legend()
	plt.show()